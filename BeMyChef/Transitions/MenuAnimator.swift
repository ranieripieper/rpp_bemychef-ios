//
//  MenuAnimator.swift
//  BeMyChef
//
//  Created by Gilson Gil on 3/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning {
  @IBOutlet weak var mainNavigationController: MainNavigationController!
  @IBOutlet weak var menuOpenPanGesture: UIPanGestureRecognizer!
  
  var presenting = false
  var interactive = false
  let menuWidthPercentage: CGFloat = 0.8
  var dimmingView: UIView?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    mainNavigationController.view.addGestureRecognizer(menuOpenPanGesture)
  }
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
    let containerView = transitionContext.containerView()
    if presenting {
      dimmingView = UIView(frame: containerView.bounds)
      dimmingView?.backgroundColor = UIColor.blackColor()
      dimmingView?.alpha = 0.0
      containerView.addSubview(dimmingView!)
      containerView.addSubview(toViewController.view)
      toViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -menuWidthPercentage * containerView.bounds.width, 0)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        toViewController.view.transform = CGAffineTransformIdentity
//        self.nonDimmingWindow?.transform = CGAffineTransformIdentity
        self.dimmingView?.alpha = 0.5
        }) { finished in
          if transitionContext.transitionWasCancelled() {
//            self.mainNavigationController.menuViewController = nil
            self.dimmingView?.alpha = 0.0
          } else {
            let menuClosePanGesture = UIPanGestureRecognizer(target: self, action: "menuClosePanGestureRecognizer:")
            toViewController.view.addGestureRecognizer(menuClosePanGesture)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    } else {
      containerView.addSubview(fromViewController.view)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.menuWidthPercentage * containerView.bounds.width, 0)
        self.dimmingView?.alpha = 0.0
        }) { finished in
          if transitionContext.transitionWasCancelled() {
            self.dimmingView?.alpha = 0.5
//            self.mainNavigationController.menuViewController = nil
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
  
  @IBAction func menuOpenPanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    let view = self.mainNavigationController.view
    if menuPanGestureRecognizer.state == .Began {
      let location = menuPanGestureRecognizer.locationInView(view)
      if location.x < view.bounds.width * 0.2 {
        interactive = true
        if view.frame.origin.x == 0 {
          if mainNavigationController.viewControllers.count == 1 {
            mainNavigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
          }
        } else {
          mainNavigationController.dismissViewControllerAnimated(true, completion: nil)
        }
      }
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      self.updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      if (presenting && menuPanGestureRecognizer.velocityInView(view).x > 0) || (!presenting && menuPanGestureRecognizer.velocityInView(view).x < 0) {
        self.finishInteractiveTransition()
      } else {
        self.cancelInteractiveTransition()
      }
      interactive = false
    }
  }
  
  func menuClosePanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    let view = self.mainNavigationController.view
    if menuPanGestureRecognizer.state == .Began {
      interactive = true
      mainNavigationController.dismissViewControllerAnimated(true, completion: nil)
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      self.updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      if (presenting && menuPanGestureRecognizer.velocityInView(view).x > 0) || (!presenting && menuPanGestureRecognizer.velocityInView(view).x < 0) {
        self.finishInteractiveTransition()
      } else {
        self.cancelInteractiveTransition()
      }
      interactive = false
    }
  }
}

extension MenuAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
  
  func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = true
    return interactive ? self : nil
  }
  
  func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = false
    return interactive ? self : nil
  }
}
