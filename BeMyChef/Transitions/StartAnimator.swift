//
//  StartAnimator.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class StartAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  var presenting = false
  lazy var backgroundView: UIView = {
    let backgroundView = UIView()
    backgroundView.backgroundColor = UIColor(red: 250.0/255.0, green: 248.0/255.0, blue: 236.0/255.0, alpha: 1)
    backgroundView.alpha = 0.8
    return backgroundView
    }()
  var formerSuperview: UIView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
    if let uiNavigationController = toViewController as? UINavigationController where uiNavigationController.viewControllers.first is NewFoodViewController  {
      if presenting {
        formerSuperview = fromViewController.view.superview
        containerView.addSubview(fromViewController.view)
        containerView.addSubview(toViewController.view)
        toViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, containerView.bounds.width, 0)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
          fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -containerView.bounds.width / 3, 0)
          toViewController.view.transform = CGAffineTransformIdentity
        }) { finished in
          if transitionContext.transitionWasCancelled() {
            self.formerSuperview?.addSubview(fromViewController.view)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
      } else {
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
          fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, containerView.bounds.width, 0)
          toViewController.view.transform = CGAffineTransformIdentity
        }) { finished in
          if !transitionContext.transitionWasCancelled() {
            self.formerSuperview?.addSubview(toViewController.view)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
      }
    } else {
      if presenting {
        if toViewController is LoginViewController {
          let height: CGFloat = 400
          backgroundView.frame = CGRect(x: 8, y: (containerView.bounds.height - height) / 2, width: containerView.bounds.width - 8 * 2, height: height)
        } else {
          backgroundView.frame = CGRect(x: 8, y: 8, width: containerView.bounds.width - 8 * 2, height: containerView.bounds.height - 8 * 2)
        }
        containerView.addSubview(backgroundView)
        toViewController.view.frame = backgroundView.frame
        containerView.addSubview(toViewController.view)
        backgroundView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, containerView.bounds.height)
        toViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, containerView.bounds.height)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
          self.backgroundView.transform = CGAffineTransformIdentity
          toViewController.view.transform = CGAffineTransformIdentity
          }) { (finished) -> Void in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
      } else {
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
          
          }) { (finished) -> Void in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
      }
    }
  }
}

extension StartAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
}
