//
//  ZipcodeAnimator.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ZipcodeAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  var presenting = false
  var dimmingView: UIView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
    if presenting {
      dimmingView = UIView(frame: containerView.bounds)
      dimmingView!.backgroundColor = UIColor.blackColor()
      dimmingView!.alpha = 0.0
      
      containerView.addSubview(dimmingView!)
      containerView.addSubview(toViewController!.view)
      
      toViewController!.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0.0, containerView.bounds.height)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        toViewController!.view.transform = CGAffineTransformIdentity
        self.dimmingView!.alpha = 0.5
        }, completion: { (finished) -> Void in
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      })
    } else {
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        fromViewController!.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0.0, containerView.bounds.height)
        self.dimmingView!.alpha = 0.0
        }, completion: { (finished) -> Void in
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
          self.dimmingView!.removeFromSuperview()
          self.dimmingView = nil
      })
    }
  }
}

extension ZipcodeAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
}
