//
//  LoginTransition.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LoginTransition: NSObject, UIViewControllerAnimatedTransitioning {
  var presenting = false
  
  var formerSuperview: UIView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.5
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
    
    formerSuperview = fromViewController.view.superview
    if presenting {
      containerView.addSubview(fromViewController.view)
      containerView.addSubview(toViewController.view)
      toViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, containerView.bounds.height)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .CurveEaseOut, animations: {
        fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -containerView.bounds.height)
        toViewController.view.transform = CGAffineTransformIdentity
        }) { finished in
          if !transitionContext.transitionWasCancelled() {
            self.formerSuperview?.addSubview(fromViewController.view)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    } else {
      toViewController.view.transform = CGAffineTransformIdentity
      toViewController.view.frame = containerView.bounds
      containerView.addSubview(toViewController.view)
      toViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -containerView.bounds.height)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .CurveEaseOut, animations: {
        fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, containerView.bounds.height)
        toViewController.view.transform = CGAffineTransformIdentity
        }) { finished in
          if !transitionContext.transitionWasCancelled() {
            self.formerSuperview?.addSubview(fromViewController.view)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
}