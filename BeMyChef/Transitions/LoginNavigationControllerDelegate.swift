//
//  LoginNavigationControllerDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LoginNavigationControllerDelegate: NSObject, UINavigationControllerDelegate {
  @IBOutlet weak var navigationController: UINavigationController?
  
  let loginTransition = LoginTransition()
  var interactionController: UIPercentDrivenInteractiveTransition?
  var panGesture: UIPanGestureRecognizer!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    panGesture = UIPanGestureRecognizer(target: self, action: Selector("panned:"))
    navigationController!.view.addGestureRecognizer(panGesture)
  }
  
  func panned(gestureRecognizer: UIPanGestureRecognizer) {
    switch gestureRecognizer.state {
    case .Began:
      interactionController = UIPercentDrivenInteractiveTransition()
      if navigationController?.viewControllers.count > 1 {
        if let viewController = navigationController?.viewControllers.last as? RegisterKitchenAddressViewController where viewController.dimmingView != nil {
          
        } else {
          navigationController?.popViewControllerAnimated(true)
        }
      }
    case .Changed:
      let translation = gestureRecognizer.translationInView(navigationController!.view)
      let completionProgress = translation.y / navigationController!.view.bounds.height / 4
      interactionController?.updateInteractiveTransition(completionProgress)
    case .Ended:
      let translation = gestureRecognizer.translationInView(navigationController!.view)
      let completionProgress = translation.y / navigationController!.view.bounds.height
      if (gestureRecognizer.velocityInView(navigationController!.view).y > 0 && completionProgress > 0.6) {
        interactionController?.finishInteractiveTransition()
      } else {
        interactionController?.cancelInteractiveTransition()
      }
      interactionController = nil
      
    default:
      interactionController?.cancelInteractiveTransition()
      interactionController = nil
    }
  }
  
  func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    loginTransition.presenting = operation == .Push
    return loginTransition
  }
  
  func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return interactionController
  }
}
