//
//  DeliveryConfirmationAnimator.swift
//  BeMyChef
//
//  Created by Gilson Gil on 1/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class DeliveryConfirmationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  var presenting = false
  var dimmingView: UIView?
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      let containerView = transitionContext.containerView()
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
      
      dimmingView = UIView(frame: fromViewController!.view.frame)
      dimmingView!.backgroundColor = UIColor.blackColor()
      dimmingView!.alpha = 0.0
      
      containerView.addSubview(dimmingView!)
      containerView.addSubview(toViewController!.view)
      
      toViewController!.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0.0, containerView.bounds.height)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        toViewController!.view.transform = CGAffineTransformIdentity
        self.dimmingView!.alpha = 0.7
      }, completion: { (finished) -> Void in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      })
    } else {
      let containerView = transitionContext.containerView()
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        fromViewController!.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0.0, containerView.bounds.height)
        self.dimmingView!.alpha = 0.0
      }, completion: { (finished) -> Void in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        self.dimmingView!.removeFromSuperview()
        self.dimmingView = nil
      })
    }
  }
}

extension DeliveryConfirmationAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
}
