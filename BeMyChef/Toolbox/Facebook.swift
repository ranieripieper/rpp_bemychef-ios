//
//  Facebook.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Facebook {
  var accountStore = ACAccountStore()
  
  func loginWithFacebookWithClosure(closure: (Result<[String: String]> -> ())?) {
    let facebookAccountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
    let options: [String: AnyObject] = [ACFacebookAppIdKey: "721843591258451", ACFacebookAudienceKey: ACFacebookAudienceEveryone, ACFacebookPermissionsKey: ["email"]]
    accountStore.requestAccessToAccountsWithType(facebookAccountType, options: options) { (granted, error) in
      if granted {
        self.getUserData(closure)
      } else if error.code == 6 {
        let login = FBSDKLoginManager()
        login.loginBehavior = .Native
        login.logInWithReadPermissions([]) { result, error in
          if error != nil {
            println("error: \(error)")
            closure?(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
          } else if result.isCancelled {
            println("user cancelled")
            closure?(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": "user cancelled"])))
          } else {
            println("permissions: \(result.grantedPermissions)")
            self.getUserData(closure)
          }
        }
      } else {
        closure?(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
      }
    }
  }
  
  func getUserData(completion: (Result<[String: String]> -> ())?) {
    if let currentAccessToken = FBSDKAccessToken.currentAccessToken().tokenString {
      FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "name,picture.type(large){url}"]).startWithCompletionHandler() { _, result, error in
        if error != nil {
          completion?(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
        } else {
          let name = result["name"] as? String ?? ""
          let avatar = result.valueForKeyPath("picture.data.url") as? String ?? ""
          completion?(Result(["name": name, "avatar": avatar, "accessToken": currentAccessToken]))
        }
      }
//      FBSDKGraphRequest(graphPath: "me/picture", parameters: nil).startWithCompletionHandler({ _, result, error in
//        if error != nil {
//          completion?(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
//        } else {
//          completion?(Result(["name": result["name"] as! String, "accessToken": currentAccessToken]))
//        }
//      })
    }
  }
}