//
//  CEP.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/1/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

//http://cep.correiocontrol.com.br/xxxxxxxx.json
struct CEP {
  static func addressForZipcode(zipcode: String, completion: ((street: String?, city: String?, state: String?) -> ())) {
    let manager = AFHTTPRequestOperationManager()
    manager.GET("http://cep.correiocontrol.com.br/\(zipcode).json", parameters: nil, success: { requestOperation, response in
      if let responseDict = response as? NSDictionary {
        let street = responseDict.valueForKeyPath("logradouro") as? String
        let city = responseDict.valueForKeyPath("localidade") as? String
        let state = responseDict.valueForKeyPath("uf") as? String
        completion(street: street, city: city, state: state)
      } else {
        completion(street: nil, city: nil, state: nil)
      }
    }) { requestOperation, error in
      println(error)
      completion(street: nil, city: nil, state: nil)
    }
  }
}
