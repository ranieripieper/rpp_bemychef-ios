//
//  MainNavigationController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import CoreImage

extension UIImage {
  class func applyBlurToView(view: UIView) -> UIImage {
    var snapshotView = view.snapshotViewAfterScreenUpdates(true)
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0.0)
    snapshotView.layer.renderInContext(UIGraphicsGetCurrentContext())
    var image = UIGraphicsGetImageFromCurrentImageContext();
    let filter = CIFilter(name: "CIGaussianBlur")
    filter.setDefaults()
    filter.setValue(image.CIImage, forKey: kCIInputImageKey)
    filter.setValue(10, forKey: kCIInputRadiusKey)
    let outputImage = filter.outputImage
    let context = CIContext(options: nil)
    var rect = outputImage.extent()
    rect.origin.x += (rect.width - image.size.width) / 2
    rect.origin.y += (rect.height - image.size.height) / 2
    rect.size = image.size
    let cgimage = context.createCGImage(outputImage, fromRect: rect)
    return UIImage(CGImage: cgimage)!
  }
  
  class func blurredView(view: UIView) -> UIImageView {
    let image = self.applyBlurToView(view)
    let imageView = UIImageView(image: image)
    return imageView
  }
}
