//
//  Notification.swift
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

struct Notification<A> {
  let name: String
}

func postNotification<A>(note: Notification<A>, value: A) {
  let userInfo = ["value": Box(value)]
  NSNotificationCenter.defaultCenter().postNotificationName(note.name, object: nil, userInfo: userInfo)
}

class NotificationObserver {
  let observer: NSObjectProtocol
  
  init<A>(notification: Notification<A>, block aBlock: A -> ()) {
    observer = NSNotificationCenter.defaultCenter().addObserverForName(notification.name, object: nil, queue: nil) { note in
      if let value = (note.userInfo?["value"] as? Box<A>)?.unbox {
        aBlock(value)
      } else if let userInfo = note.userInfo as? A {
        aBlock(userInfo)
      } else {
        assert(false, "Couldn't understand user info")
      }
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(observer)
  }
}

class KeyboardNotificationObserver {
  let scrollView: UIScrollView
  var keyboardWillShowObserver: NSObjectProtocol!
  var keyboardWillHideObserver: NSObjectProtocol!
  var customCallback: ((CGFloat, CGFloat) -> ())?
  
  init(scrollView: UIScrollView) {
    self.scrollView = scrollView
    installNotifications()
  }
  
  func installNotifications() {
    keyboardWillShowObserver = NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillShowNotification, object: nil, queue: nil, usingBlock: handleKeyboardUp)
    keyboardWillHideObserver = NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillHideNotification, object: nil, queue: nil, usingBlock: handleKeyboardDown)
  }
  
  func handleKeyboardUp(notification: NSNotification!) {
    if let frameValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      scrollView.contentInset = UIEdgeInsets(top: scrollView.contentInset.top, left: scrollView.contentInset.left, bottom: frameValue.CGRectValue().height, right: scrollView.contentInset.right)
      scrollView.scrollIndicatorInsets = scrollView.contentInset
      if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? CGFloat {
        customCallback?(frameValue.CGRectValue().height, duration)
      }
    }
  }
  
  func handleKeyboardDown(notification: NSNotification!) {
    scrollView.contentInset = UIEdgeInsets(top: scrollView.contentInset.top, left: scrollView.contentInset.left, bottom: 0, right: scrollView.contentInset.right)
    scrollView.scrollIndicatorInsets = scrollView.contentInset
    if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? CGFloat {
      customCallback?(0, duration)
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(keyboardWillShowObserver)
    NSNotificationCenter.defaultCenter().removeObserver(keyboardWillHideObserver)
  }
}
