//
//  ImagePicker.swift
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ImagePicker: NSObject {
  var viewController: UIViewController
  var callback: UIImage? -> ()
  var alertView: AlertController?
  
  init(viewController: UIViewController, callback: UIImage? -> ()) {
    self.viewController = viewController
    self.callback = callback
  }
  
  func presentImagePicker() {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    dispatch_async(dispatch_get_main_queue()) {
      if UIImagePickerController.isCameraDeviceAvailable(.Rear) || UIImagePickerController.isCameraDeviceAvailable(.Front) {
        self.alertView = AlertController(title: nil, message: nil, buttons: [("Camera", {
          dispatch_async(dispatch_get_main_queue()) {
            imagePicker.sourceType = .Camera
            self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
            self.alertView = nil
          }
        }), ("Biblioteca", {
          dispatch_async(dispatch_get_main_queue()) {
            imagePicker.sourceType = .PhotoLibrary
            self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
            self.alertView = nil
          }
        })], cancelButton: ("Cancel", {
          self.alertView = nil
          self.callback(nil)
        }), style: .Alert)
        self.alertView?.alertInViewController(self.viewController)
      } else {
        imagePicker.sourceType = .PhotoLibrary
        self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
      }
    }
  }
}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      callback(image)
      viewController.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    callback(nil)
    viewController.dismissViewControllerAnimated(true, completion: nil)
  }
}