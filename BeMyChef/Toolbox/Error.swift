//
//  Error.swift
//  Networking
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Error {
  typealias ErrorInfoDictionary = Dictionary<String, Any>
  
  let code: Int
  let domain: String
  let userInfo: ErrorInfoDictionary
  
  var localizedDescription: String {
    return userInfo["description"] as? String ?? "unknown error"
  }
  
  init(code: Int, domain: String, userInfo: ErrorInfoDictionary?) {
    self.code = code
    self.domain = domain
    if let info = userInfo {
      self.userInfo = info
    }
    else {
      self.userInfo = [String:Any]()
    }
  }
}