//
//  Result.swift
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

enum Result<T> {
  case Success(Box<T>)
  case Failure(Error)
  
  init(_ value: T) {
    self = .Success(Box(value))
  }
  
  init(_ error: Error) {
    self = .Failure(error)
  }
  
  var failed: Bool {
    switch self {
    case .Failure(let error):
      return true
      
    default:
      return false
    }
  }
  
  var error: Error? {
    switch self {
    case .Failure(let error):
      return error
      
    default:
      return nil
    }
  }
}
