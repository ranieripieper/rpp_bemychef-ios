//
//  LocationManager.swift
//
//  Created by Gilson Gil on 10/30/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

@objc protocol LocationManagerDelegate {
  optional func didUpdateLocation(userLocation: CLLocation)
  optional func didRespondToRequestWithStatus(status: CLAuthorizationStatus)
}

class LocationManager: CLLocationManager {
  weak var locationManagerDelegate: LocationManagerDelegate?
  var userLocation: CLLocation!
  class var latitude: Float {
    if let location = LocationManager.sharedInstance.location {
      return Float(location.coordinate.latitude)
    } else {
      return 0
    }
  }
  class var longitude: Float {
    if let location = LocationManager.sharedInstance.location {
      return Float(location.coordinate.longitude)
    } else {
      return 0
    }
  }
  
  class var sharedInstance: LocationManager {
    struct Static {
      static var instance: LocationManager?
      static var token: dispatch_once_t = 0
    }
    dispatch_once(&Static.token) {
      Static.instance = LocationManager()
    }
    return Static.instance!
  }
  
  override init() {
    super.init()
    delegate = self
    desiredAccuracy = kCLLocationAccuracyBest
    if IOS_8() {
      requestWhenInUseAuthorization()
    }
  }
  
  class func isAuthorized() -> Bool {
    return authorizationStatus() == .AuthorizedWhenInUse
  }
  
  
}

extension LocationManager: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse {
      startUpdatingLocation()
    }
  }
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    if let location = locations.last as? CLLocation {
      if userLocation == nil || location.distanceFromLocation(userLocation) > 100 {
        userLocation = location
        locationManagerDelegate?.didUpdateLocation?(userLocation)
      }
    }
  }
  
  func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
    println("error \(error)")
  }
}
