//
//  PushNotificationsManager.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct PushNotificationsManager {
  static let DidRegisterForRemoteNotificationsWithDeviceToken = "DidRegisterForRemoteNotificationsWithDeviceToken"
  static let DeviceToken = "DeviceToken"
  
  static func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    println("did receive notification: \(userInfo)")
  }
  
  static func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    println("did register for remote notification")    
    let token = deviceToken.description.stringByReplacingOccurrencesOfString(" ", withString: "").stringByReplacingOccurrencesOfString("<", withString: "").stringByReplacingOccurrencesOfString(">", withString: "")
    if Defaults[DeviceToken].string == nil || Defaults.stringForKey(DeviceToken) != token {
      Defaults[DeviceToken] = token
      Defaults.synchronize()
      NSNotificationCenter.defaultCenter().postNotificationName(DidRegisterForRemoteNotificationsWithDeviceToken, object: nil, userInfo: [DeviceToken: token])
    }
  }
  
  static func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    println("did fail to register for remote notification: \(error)")
  }
}
