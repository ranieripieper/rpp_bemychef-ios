//
//  Box.swift
//
//  Created by Gilson Gil on 2/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Box<T> {
  let unbox: T
  init(_ value: T) { self.unbox = value }
}
