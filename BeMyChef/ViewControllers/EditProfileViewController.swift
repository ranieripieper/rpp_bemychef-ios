//
//  EditProfileViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 6/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarButton: UIButton!
  @IBOutlet weak var nameTextField: TextField!
  @IBOutlet weak var emailTextField: TextField!
  @IBOutlet weak var passwordTextField: TextField!
  @IBOutlet weak var passwordConfirmationTextField: TextField!
  @IBOutlet weak var saveButtonWidthConstraint: NSLayoutConstraint!
  
  var imagePicker: ImagePicker?
  var user: User!
  var newAvatar: UIImage?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    avatarButton.layer.borderColor = UIColor.whiteColor().CGColor
    saveButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 30 * 2
    prefillInfo()
    let tap = UITapGestureRecognizer(target: self, action: "tapped")
    view.addGestureRecognizer(tap)
  }
  
  func prefillInfo() {
    avatarButton.imageView?.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: user.avatarURL)!), placeholderImage: nil, success: { _, _, image in
      self.avatarButton.setImage(image, forState: .Normal)
    }, failure: { _, _, error in
      println(error)
    })
    nameTextField.text = user.name
    emailTextField.text = user.email
  }
  
  func tapped() {
    view.endEditing(true)
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    imagePicker = ImagePicker(viewController: self) { image in
      if image != nil {
        self.avatarButton.setImage(image!, forState: .Normal)
        self.newAvatar = image
      }
      self.imagePicker = nil;
    }
    imagePicker?.presentImagePicker()
  }
  
  // MARK: Save
  @IBAction func saveTapped(sender: UIButton) {
    let newName: String?
    if user.name != nameTextField.text {
      newName = nameTextField.text
    } else {
      newName = nil
    }
    let newEmail: String?
    if user.email != emailTextField.text {
      newEmail = emailTextField.text
    } else {
      newEmail = nil
    }
    let newPassword: String?
    if passwordTextField.text == passwordConfirmationTextField.text && count(passwordTextField.text) > 0 {
      newPassword = passwordTextField.text
    } else {
      newPassword = nil
    }
    user.update(newEmail, password: newPassword, name: newName, zipcode: nil, avatar: newAvatar, chef: nil, chefAbout: nil, address: nil) {
      switch $0 {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}
