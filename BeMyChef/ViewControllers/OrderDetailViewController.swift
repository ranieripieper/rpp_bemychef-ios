//
//  OrderDetailViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var orderCollectionDelegate: OrderCollectionDelegate!
  
  var order: Order!
  var currentRating = 0
  var currentComment = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
    configureWithOrder(order)
    refreshOrder()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    if let navigationController = self.navigationController, let index = find(navigationController.viewControllers as! [UIViewController], self), let paymentViewController = navigationController.viewControllers[Int(index - 1)] as? PaymentViewController {
      navigationController.viewControllers = navigationController.viewControllers.filter() {
        $0 is HomeViewController || $0 is OrderDetailViewController
      }
    }
  }
  
  func configure() {
    tableView.registerNib(UINib(nibName: "OrderStatusCell", bundle: nil), forCellReuseIdentifier: "OrderStatusCell")
    tableView.registerNib(UINib(nibName: "OrderInfoCell", bundle: nil), forCellReuseIdentifier: "OrderInfoCell")
    tableView.registerNib(UINib(nibName: "OrderMapCell", bundle: nil), forCellReuseIdentifier: "OrderMapCell")
    tableView.registerNib(UINib(nibName: "OrderMapFullCell", bundle: nil), forCellReuseIdentifier: "OrderMapFullCell")
    tableView.registerNib(UINib(nibName: "OrderUserCell", bundle: nil), forCellReuseIdentifier: "OrderUserCell")
    tableView.registerNib(UINib(nibName: "OrderActionCell", bundle: nil), forCellReuseIdentifier: "OrderActionCell")
  }
  
  func configureWithOrder(order: Order) {
    self.order = order
    if order.deliveryType! == .Delivery {
      if order.status == .Ready {
        orderCollectionDelegate.datasource = [.Status, .Info, .User, .Action]
      } else if order.status == .Production || order.status == .Confirmation {
        orderCollectionDelegate.datasource = [.Status, .Info, .Map]
      } else if order.status == .Delivered {
        orderCollectionDelegate.datasource = [.Status, .Info, .Rate, .RateComment]
      } else {
        orderCollectionDelegate.datasource = [.Status, .Info, .User]
      }
    } else {
      if order.status == .Ready {
        orderCollectionDelegate.datasource = [.Status, .Info, .MapFull]
      } else if order.status == .Delivered {
        orderCollectionDelegate.datasource = [.Status, .Info, .Rate, .RateComment]
      } else {
        orderCollectionDelegate.datasource = [.Status, .Info, .User]
      }
    }
    orderCollectionDelegate.order = order
    tableView.reloadData()
  }
  
  func refreshOrder() {
    order.info() {
      switch $0 {
      case .Success(let boxed):
        self.configureWithOrder(boxed.unbox)
      case .Failure(let error):
        println(error)
      }
    }
  }
  
  // MARK: Order
  func notDelivered() {
    let rateViewController = storyboard?.instantiateViewControllerWithIdentifier("RateViewController") as! RateViewController
    rateViewController.order = order
    navigationController?.pushViewController(rateViewController, animated: true)
  }
}
