//
//  LoginNavigationController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class LoginNavigationController: UINavigationController {
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }

  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return UIStatusBarStyle.LightContent
  }
}
