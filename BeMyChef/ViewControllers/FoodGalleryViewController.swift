//
//  FoodGalleryViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class FoodGalleryViewController: UIViewController {
  @IBOutlet weak var pictureImageView: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  @IBAction func closeTapped(sender: UIBarButtonItem) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func deleteTapped(sender: UIBarButtonItem) {
    
  }
}
