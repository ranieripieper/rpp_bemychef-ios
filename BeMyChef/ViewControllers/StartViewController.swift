//
//  StartViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import AVFoundation
import Accounts

class StartViewController: UIViewController {
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var startAnimator: StartAnimator!
  @IBOutlet weak var startViewLayerLeftSpaceConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var signupButton: UIButton!
  @IBOutlet weak var facebookButton: UIButton!
  @IBOutlet weak var goHomeButton: UIButton!
  
  var ornamentImageView: UIImageView?
  var containerView: UIView?
  var child: UIViewController?
  
  var movieLayer: AVPlayerLayer?
  var timer: NSTimer?
  lazy var facebook = Facebook()
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(tap)
    setupButtons()
    timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "tapped:", userInfo: nil, repeats: false)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
//    let contentURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("open", ofType: "mp4")!)
//    let moviePlayer = AVPlayer(URL: contentURL!)
//    movieLayer = AVPlayerLayer(player: moviePlayer)
//    moviePlayer.muted = true
//    let width = 1280 / 720 * UIScreen.mainScreen().bounds.height
//    let height = UIScreen.mainScreen().bounds.height
//    movieLayer!.frame = CGRect(x: (UIScreen.mainScreen().bounds.width - width) / 2, y: 0, width: width, height: height)
//    view.layer.insertSublayer(movieLayer!, atIndex: 0)
//    movieLayer!.opacity = 0.8
//    movieLayer!.player.play()
//    NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerItemDidReachEnd:", name: AVPlayerItemDidPlayToEndTimeNotification, object: movieLayer!.player.currentItem)
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
//    NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: movieLayer!.player)
//    movieLayer?.player.pause()
//    movieLayer?.removeFromSuperlayer()
//    movieLayer = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let uiNavigationController = segue.destinationViewController as? UINavigationController {
      uiNavigationController.transitioningDelegate = startAnimator
      if let registerViewController = uiNavigationController.viewControllers.first as? RegisterViewController {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "presentEmailSentAlert:", name: "FinishRegistrationProcess", object: nil)
        registerViewController.startViewController = self
      }
    } else if let loginViewController = segue.destinationViewController as? LoginViewController {
      loginViewController.transitioningDelegate = startAnimator
      loginViewController.startViewController = self
    } else if let locationViewController = segue.destinationViewController as? LocationViewController {
      locationViewController.startViewController = self
      locationViewController.user = sender as! User
      locationViewController.transitioningDelegate = startAnimator
    }
  }
  
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    if let user = User.persistedUser() {
      bounceLogo(false)
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(CGFloat(NSEC_PER_SEC) * 0.2)), dispatch_get_main_queue()) {
        self.performSegueWithIdentifier("SegueLocation", sender: user)
      }
    } else {
      bounceLogo(true)
    }
    view.removeGestureRecognizer(tap)
    timer?.invalidate()
    timer = nil
  }
  
  func playerItemDidReachEnd(notification: NSNotification) {
    let player = notification.object as! AVPlayerItem
    player.seekToTime(kCMTimeZero)
    movieLayer!.player.play()
  }
  
  func bounceLogo(showsLogin: Bool) {
    startViewLayerLeftSpaceConstraint.constant = 50
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
      self.view.layoutIfNeeded()
      }) { finished in
        
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(CGFloat(NSEC_PER_SEC) * 0.25)), dispatch_get_main_queue()) {
      if showsLogin {
        self.showLoginButtons() {
          if $0 {
            LocationManager.sharedInstance.requestWhenInUseAuthorization()
          }
        }
      } else {
        self.showEmpty()
      }
    }
  }
  
  func setupButtons() {
    loginButton.layer.borderColor = UIColor.whiteColor().CGColor
    signupButton.layer.borderColor = UIColor.whiteColor().CGColor
    facebookButton.layer.borderColor = UIColor.whiteColor().CGColor
    goHomeButton.layer.borderColor = UIColor.whiteColor().CGColor
  }
  
  func showLoginButtons(completion: ((Bool) -> ())?) {
    startViewLayerLeftSpaceConstraint.constant = -view.bounds.width
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: .CurveEaseOut, animations: {
      self.view.layoutIfNeeded()
    }, completion: completion)
  }
  
  func showEmpty() {
    startViewLayerLeftSpaceConstraint.constant = -view.bounds.width * 2
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: .CurveEaseOut, animations: {
      self.view.layoutIfNeeded()
      }, completion: nil)
  }
  
  func showEmailSent() {
    startViewLayerLeftSpaceConstraint.constant = -view.bounds.width * 3
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: .CurveEaseOut, animations: {
      self.view.layoutIfNeeded()
      }, completion: nil)
  }
  
  func showChefOptions() {
    startViewLayerLeftSpaceConstraint.constant = -view.bounds.width * 4
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: .CurveEaseOut, animations: {
      self.view.layoutIfNeeded()
      }, completion: nil)
  }
  
  func presentEmailSentAlert(notification: NSNotification) {
    if let userInfo = notification.userInfo, let chef = userInfo["chef"] as? Bool where chef {
      showChefOptions()
    } else {
      showEmailSent()
    }
  }
  
  @IBAction func goHome(sender: UIButton) {
    (UIApplication.sharedApplication().delegate as! AppDelegate).resumeMainApplicationFlow()
  }
  
  @IBAction func signup(sender: AnyObject) {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "presentEmailSentAlert:", name: "FinishRegistrationProcess", object: nil)
    let registerNavigationController = storyboard?.instantiateViewControllerWithIdentifier("RegisterNavigationController") as! UINavigationController
    if let registerViewController = registerNavigationController.viewControllers.first as? RegisterViewController {
      registerViewController.startViewController = self
      if let signupInfo = sender as? [String: String] {
        registerViewController.signupInfo = signupInfo
      }
    }
    presentVC(registerNavigationController)
  }
  
  @IBAction func login(sender: UIButton) {
    let loginViewController = storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
    loginViewController.startViewController = self
    presentVC(loginViewController)
  }
  
  @IBAction func connectFacebook(sender: UIButton) {
    facebook.loginWithFacebookWithClosure { result in
      switch result {
      case .Success(let boxed):
        self.signup(boxed.unbox)
      case .Failure(let error):
        println(error)
      }
    }
  }
  
  func presentVC(viewController: UIViewController) {
    showEmpty()
    containerView = UIView(frame: CGRect(x: 8 , y: 8, width: UIScreen.mainScreen().bounds.width - 8 * 2, height: UIScreen.mainScreen().bounds.height - 8 * 2))
    containerView!.backgroundColor = UIColor(red: 250.0 / 255.0, green: 248.0 / 255.0, blue: 236.0 / 255.0, alpha: 0.8)
    viewController.willMoveToParentViewController(self)
    viewController.view.frame = containerView!.bounds
    containerView!.addSubview(viewController.view)
    viewController.didMoveToParentViewController(self)
    child = viewController
    view.addSubview(containerView!)
    containerView!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, view.bounds.height)
    UIView.animateWithDuration(0.3) {
      self.containerView!.transform = CGAffineTransformIdentity
    }
  }
  
  func dismissViewController(completion: (() -> ())?) {
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.containerView!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, self.view.bounds.height)
    }) { finished in
      self.child?.view.removeFromSuperview()
      self.child = nil
      self.containerView?.removeFromSuperview()
      self.containerView = nil
      self.showLoginButtons(nil)
      completion?()
    }
  }
  
  func signupSuccessful(chef: Bool) {
    dismissViewController() {
      if chef {
        self.showChefOptions()
      } else {
        self.showEmailSent()
      }
    }
  }
  
  func loginSuccessful() {
    dismissViewController() {
      (UIApplication.sharedApplication().delegate as! AppDelegate).resumeMainApplicationFlow()
    }
  }
}
