//
//  PaymentInfoViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PaymentInfoViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var nameTextField: TextField!
  @IBOutlet weak var cpfTextField: TextField!
  @IBOutlet weak var agencyTextField: TextField!
  @IBOutlet weak var accountTextField: TextField!
  @IBOutlet weak var bankButton: UIButton!
  
  @IBOutlet weak var continueButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var alertView: AlertController?
  var startViewController: StartViewController?
  var bank: Bank?
  
  var signupInfo: [String: AnyObject]?
  var edit = false {
    didSet {
      if edit {
        self.view.backgroundColor = UIColor(red: 250 / 255, green: 248 / 255, blue: 236 / 255, alpha: 1)
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    continueButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - (8 + 20) * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    view.addGestureRecognizer(tap)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = self
    }
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = nil
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let termsViewController = segue.destinationViewController as? TermsViewController {
      termsViewController.signupInfo = signupInfo
      termsViewController.startViewController = startViewController
    } else if let bankViewController = segue.destinationViewController as? BankViewController {
      bankViewController.edit = edit
      bankViewController.callback = {
        self.bankButton.setTitle($0.name, forState: .Normal)
        self.bank = $0
        self.bankButton.setTitleColor(UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1), forState: .Normal)
        self.navigationController?.popViewControllerAnimated(false)
      }
    }
  }
  
  // MARK: Tap
  @IBAction func tapped(sender: UITapGestureRecognizer!) {
    nameTextField.resignFirstResponder() || cpfTextField.resignFirstResponder() || agencyTextField.resignFirstResponder() || accountTextField.resignFirstResponder()
  }
  
  // MARK: Continue
  @IBAction func continueTapped(sender: UIButton) {
    if validateFields() {
      if edit {
        if let mainNavigationController = navigationController as? MainNavigationController, let user = mainNavigationController.user, let bank = bank {
          user.updatePaymentInfo(cpfTextField.text, bankName: nameTextField.text, bankNumber: bank.code, bankAgency: agencyTextField.text, bankAccount: accountTextField.text) {
            switch $0 {
            case .Success:
              println("success")
            case .Failure(let error):
              println(error.localizedDescription)
            }
          }
        }
      } else {
        signupInfo!["paymentName"] = nameTextField.text
        signupInfo!["paymentCpf"] = cpfTextField.text
        signupInfo!["paymentBank"] = bank!.code
        signupInfo!["paymentAgency"] = agencyTextField.text
        signupInfo!["paymentAccount"] = accountTextField.text
        performSegueWithIdentifier("SegueTerms", sender: nil)
      }
    }
  }
  
  func validateFields() -> Bool {
    return count(nameTextField.text) > 0 && count(cpfTextField.text) > 0 && count(bankButton.titleLabel!.text!) > 0 && count(agencyTextField.text) > 0 && count(accountTextField.text) > 0
  }
  
  // MARK: How it works
  @IBAction func howItWorksTapped(sender: UIButton) {
    
  }
}

extension PaymentInfoViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return scrollView.contentOffset.y <= 0
  }
}

extension PaymentInfoViewController: TextFieldViewController {
  func lastTextFieldReturned(textField: UITextField) {
    
  }
}
