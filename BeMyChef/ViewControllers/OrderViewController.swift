//
//  OrderViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 1/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var deliveryImageView: UIImageView!
  @IBOutlet weak var foodNameLabel: UILabel!
  @IBOutlet weak var foodAmountAvailableLabel: UILabel!
  @IBOutlet weak var foodAmountLabel: UILabel!
  @IBOutlet weak var userAddressLabel: UILabel!
  @IBOutlet weak var foodServesCountLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var confirmOrderButton: UIButton!
  @IBOutlet weak var foodPriceLabel: UILabel!
  @IBOutlet weak var bmcPriceLabel: UILabel!
  @IBOutlet weak var deliveryPriceLabel: UILabel!
  @IBOutlet weak var foodFrameImageViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var infoFrameHeightConstraint: NSLayoutConstraint!
  
  var food: Food?
  var deliveryType: DeliveryType?
  var foodAmount: Int = 1
  
  override func viewDidLoad() {
    super.viewDidLoad()
    foodImageView.layer.borderColor = UIColor.whiteColor().CGColor
    foodFrameImageViewWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 100
    if deliveryType! == .Delivery {
      infoFrameHeightConstraint.constant = 450
    } else {
      infoFrameHeightConstraint.constant = 442
    }
    confirmOrderButton.setBackgroundImage(UIImage(named: "btn_detail_ask")?.resizableImageWithCapInsets(UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)), forState: .Normal)
    configureWithFood(food!)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let paymentViewController = segue.destinationViewController as? PaymentViewController, let reserve = sender as? Reserve {
      paymentViewController.food = food
      paymentViewController.reserve = reserve
    }
  }
  
  @IBAction func decreaseAmount(sender: UIButton) {
    if foodAmount > 1 {
      foodAmount--
      updateFoodAmountLabelWithValue(foodAmount)
      updatePriceLabel()
    }
  }
  
  @IBAction func increaseAmount(sender: UIButton) {
    if foodAmount < food!.amountAvailable {
      foodAmount++
      updateFoodAmountLabelWithValue(foodAmount)
      updatePriceLabel()
    }
  }
  
  func configureWithFood(food: Food) {
    if food.pictures.count > 0 {
      foodImageView.setImageWithURL(NSURL(string: food.pictures.first as! String))
    }
    deliveryImageView.image = deliveryType?.confirmImage() ?? food.kitchen.deliveryType.confirmImage()
    foodNameLabel.text = food.name
    foodAmountAvailableLabel.text = "máximo \(food.amountAvailable)"
    updateFoodAmountLabelWithValue(foodAmount)
    if food.kitchen.deliveryType == .Delivery || (deliveryType != nil && deliveryType! == .Delivery) {
      if let mainNavigationController = self.navigationController as? MainNavigationController, let user = mainNavigationController.user {
        userAddressLabel.text = Address.persistedAddress()?.toString()
      }
    } else if deliveryType! == .Takeaway {
      deliveryPriceLabel.text = "Não há taxa de entrega!"
      userAddressLabel.text = nil
    }
    foodServesCountLabel.text = String(food.servesCount)
    updatePriceLabel()
  }
  
  func updatePriceLabel() {
    priceLabel.text = String(format: "R$%.2f", food!.price * Float(foodAmount) + Float((deliveryType ?? food!.kitchen.deliveryType) == .Delivery ? 5 : 0))
    foodPriceLabel.text = String(format: "Valor do prato: R$%.2f", food!.price * Float(foodAmount) * 0.8)
    bmcPriceLabel.text = String(format: "Taxa do BeMyChef: R$%.2f", food!.price * Float(foodAmount) * 0.2)
    if (deliveryType ?? food!.kitchen.deliveryType) != .Delivery {
      deliveryPriceLabel.text = nil
      deliveryPriceLabel.hidden = true
    } else {
      deliveryPriceLabel.text = String(format: "Taxa de entrega: R$%.2f", Float((deliveryType ?? food!.kitchen.deliveryType) == .Delivery ? 5 : 0))
      deliveryPriceLabel.hidden = false
    }
  }
  
  func updateFoodAmountLabelWithValue(value: Int) {
    if value < 10 {
      foodAmountLabel.text = "0\(value)"
    } else {
      foodAmountLabel.text = "\(value)"
    }
  }
  
  // MARK: Confirm
  @IBAction func confirmTapped(sender: UIButton) {
    food!.reserve(foodAmount, deliveryType: deliveryType!) { result in
      switch result {
      case .Success(let boxed):
        self.performSegueWithIdentifier("SeguePayment", sender: boxed.unbox)
      case .Failure(let error):
        println(error)
      }
    }
  }
}
