//
//  TermsViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var declineButton: UIButton!
  @IBOutlet weak var termsLabelWidthConstraint: NSLayoutConstraint!
  
  var signupInfo: [String: AnyObject]!
  var startViewController: StartViewController?
  var alertController: AlertController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    termsLabelWidthConstraint.constant = UIScreen.mainScreen().bounds.width - (8 + 20) * 2
    declineButton.layer.borderColor = UIColor(red: 236.0/255.0, green: 111.0/255.0, blue: 102.0/255.0, alpha: 1).CGColor
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = self
    }
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = nil
    }
  }
  
  // MARK: Declie
  @IBAction func declineTapped(sender: UIButton) {
    navigationController?.popViewControllerAnimated(true)
  }
  
  // MARK: Accept
  @IBAction func acceptTapped(sender: UIButton) {
    signup()
  }
  
  // MARK: Signup
  func signup() {
    if let chef = signupInfo["chef"] as? Bool where chef {
      User.registerChef(signupInfo["email"] as! String, password: signupInfo["password"] as! String, name: signupInfo["name"] as! String, avatar: signupInfo["avatar"] as? UIImage, chefAbout: signupInfo["kitchenDescription"] as! String, kitchenPictures: signupInfo["kitchenPictures"] as! [UIImage], phone: signupInfo["kitchenPhone"] as! String, cpf: signupInfo["paymentCpf"] as! String, zipcode: signupInfo["kitchenZipcode"] as! String, street: signupInfo["kitchenStreet"] as! String, number: signupInfo["kitchenNumber"] as! String, complement: signupInfo["kitchenComplement"] as! String, bankName: signupInfo["paymentName"] as! String, bankNumber: signupInfo["paymentBank"] as! String, bankAgency: signupInfo["paymentAgency"] as! String, bankAccount: signupInfo["paymentAccount"] as! String) {
        switch $0 {
        case .Success(let boxed):
          self.startViewController?.signupSuccessful(true)
        case .Failure(let error):
          println(error)
          self.alertController = AlertController(title: nil, message: error.localizedDescription, buttons: nil, cancelButton: ("Ok", {
            self.alertController = nil
          }), style: .Alert)
          self.alertController?.alertInViewController(self)
        }
      }
    } else {
      User.registerUser(signupInfo["email"] as! String, password: signupInfo["password"] as! String, name: signupInfo["name"] as! String, avatar: signupInfo["avatar"] as? UIImage) {
        switch $0 {
        case .Success(let boxed):
          self.startViewController?.signupSuccessful(false)
        case .Failure(let error):
          println(error)
        }
      }
    }
  }
}

extension TermsViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return scrollView.contentOffset.y <= 0
  }
}
