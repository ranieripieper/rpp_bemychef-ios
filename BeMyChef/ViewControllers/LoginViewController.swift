//
//  LoginViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/1/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var forgetPasswordButton: UIButton!
  @IBOutlet weak var welcomeLabel: UILabel!
  @IBOutlet weak var welcomeDescriptionLabel: UILabel!
  @IBOutlet weak var newPasswordLabel: UILabel!
  
  var type: Type = .Login
  var startViewController: StartViewController?
  var alertController: AlertController?
  
  enum Type {
    case Login, NewPassword
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    newPasswordLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
  }
  
  // MARK: Cancel
  @IBAction func cancel(sender: UIButton) {
    startViewController?.dismissViewController(nil)
  }
  
  // MARK: Forgot Password
  @IBAction func forgotPasswordTapped(sender: UIButton) {
    if validateEmail() {
      NetworkManager.sharedInstance.forgotPassword(usernameTextField.text) { result in
        switch result {
        case .Success(let boxedBool):
          self.type = .NewPassword
          self.sentEmail()
        case .Failure(let error):
          self.presentBannerWithText(error.userInfo["description"] as! String)
        }
      }
    } else {
      missingEmail()
    }
  }
  
  func validateEmail() -> Bool {
    return count(usernameTextField.text) > 0
  }
  
  func sentEmail() {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: UIViewAnimationOptions.AllowUserInteraction, animations: {
      self.welcomeLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.view.bounds.width, 0)
      self.welcomeDescriptionLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.view.bounds.width, 0)
      self.usernameTextField.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.view.bounds.width, 0)
      self.passwordTextField.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.view.bounds.width, 0)
      self.forgetPasswordButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.view.bounds.width, 0)
      self.newPasswordLabel.transform = CGAffineTransformIdentity
    }) { finished in
      
    }
  }
  
  // MARK: Login
  @IBAction func loginTapped(sender: UIButton?) {
    switch type {
    case .Login:
      if validateEmail() {
        if validatePassword() {
          User.login(usernameTextField.text, password: passwordTextField.text) { result in
            switch result {
            case .Success(let boxedDict):
              self.startViewController?.loginSuccessful()
            case .Failure(let error):
              self.presentBannerWithText(error.localizedDescription)
            }
          }
        } else {
          missingPassword()
        }
      } else {
        missingEmail()
      }
    case .NewPassword:
      startViewController?.showLoginButtons(nil)
      dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  func validatePassword() -> Bool {
    return count(passwordTextField.text) > 0
  }
  
  func missingEmail() {
    presentBannerWithText("Por favor, preencha o email.")
  }
  
  func missingPassword() {
    presentBannerWithText("Por favor, preencha a senha.")
  }
  
  func invalidInputs() {
    presentBannerWithText("Nome ou senha inválidos.")
  }
  
  // MARK: General
  func presentBannerWithText(text: String) {
    alertController = AlertController(title: "", message: text, buttons: nil, cancelButton: ("Ok", {
      self.alertController = nil
    }), style: .Alert)
    alertController?.alertInViewController(self)
  }
}

extension LoginViewController: TextFieldViewController {
  func lastTextFieldReturned(textField: UITextField) {
    if textField == passwordTextField {
      loginTapped(nil)
    }
  }
}
