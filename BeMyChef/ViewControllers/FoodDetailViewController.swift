//
//  FoodDetailViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class FoodDetailViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var foodDetailTableViewDelegate: FoodDetailTableViewDelegate!
  @IBOutlet weak var orderNowButton: UIButton!
  @IBOutlet weak var distanceButton: UIButton!

  var food: Food?
  var deliveryConfirmationAnimator: DeliveryConfirmationAnimator?
  
  deinit {
    tableView.dataSource = nil
    tableView.delegate = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    orderNowButton.layer.borderColor = UIColor.whiteColor().CGColor
    foodDetailTableViewDelegate.food = food
    let userLocation = CLLocation(latitude: CLLocationDegrees(LocationManager.latitude), longitude: CLLocationDegrees(LocationManager.longitude))
    let distance = userLocation.distanceFromLocation(food!.kitchen.location())
    let km = distance / 1000.0
    distanceButton.setTitle(String(format: "%.1fkm", km), forState: .Normal)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    deliveryConfirmationAnimator = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let deliveryConfirmationViewController = segue.destinationViewController as? DeliveryConfirmationViewController {
      deliveryConfirmationViewController.foodDetailViewController = self
      deliveryConfirmationViewController.distance = distanceButton.titleLabel?.text ?? ""
      deliveryConfirmationAnimator = DeliveryConfirmationAnimator()
      deliveryConfirmationViewController.transitioningDelegate = deliveryConfirmationAnimator!
    } else if let orderViewController = segue.destinationViewController as? OrderViewController {
      orderViewController.food = food!
      if let deliveryType = sender as? String {
        orderViewController.deliveryType = DeliveryType(string: deliveryType)
      } else {
        orderViewController.deliveryType = food!.kitchen.deliveryType
      }
    }
  }
  
  // MARK: Location
  @IBAction func locationTapped(sender: UIBarButtonItem) {
    println("go to location")
  }
  
  // MARK: Order
  @IBAction func orderTapped(sender: UIButton) {
    if food!.kitchen.deliveryType == .Both {
      performSegueWithIdentifier("SegueDeliveryConfirmation", sender: nil)
    } else {
      performSegueWithIdentifier("SegueOrder", sender: nil)
    }
  }
  
  func confirmOrderWithDeliveryType(deliveryType: DeliveryType) {
    performSegueWithIdentifier("SegueOrder", sender: deliveryType.string())
  }
}

extension FoodDetailViewController: FoodDetailChefCellDelegate {
  func goToChef(cell: FoodDetailChefCell) {
//    performSegueWithIdentifier("SegueChefDetail", sender: food!.kitchen.chef)
  }
}
