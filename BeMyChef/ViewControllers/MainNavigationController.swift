//
//  MainNavigationController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum CurrentViewController {
  case UserHome, Kitchen, Orders, History, Notifications, Settings
}

class MainNavigationController: UINavigationController {
  @IBOutlet weak var menuAnimator: MenuAnimator!
  
  var currentViewController: CurrentViewController!
  var user = User.persistedUser()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    switch User.currentMode() {
    case .Chef:
      viewControllers = [UIStoryboard(name: "Chef", bundle: nil).instantiateInitialViewController()]
      currentViewController = .Kitchen
    case .User:
      viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateInitialViewController()]
      currentViewController = .UserHome
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let menuViewController = segue.destinationViewController as? MenuViewController {
      menuViewController.transitioningDelegate = menuAnimator
      menuViewController.mainNavigationController = self
    }
  }
  
  // MARK: Menu
  func goHome() {
    if currentViewController != .UserHome {
      viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateInitialViewController()]
      currentViewController = .UserHome
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func goHome(animated: Bool) {
    if !animated {
      goHome()
    } else {
      let homeViewController = UIStoryboard(name: "User", bundle: nil).instantiateInitialViewController() as! HomeViewController
      animateToViewController(homeViewController)
      currentViewController = .UserHome
    }
  }
  
  func goKitchen() {
    if currentViewController != .Kitchen {
      viewControllers = [UIStoryboard(name: "Chef", bundle: nil).instantiateInitialViewController()]
      currentViewController = .Kitchen
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func goKitchen(animated: Bool) {
    if !animated {
      goKitchen()
    } else {
      let chefHomeViewController = UIStoryboard(name: "Chef", bundle: nil).instantiateInitialViewController() as! ChefHomeViewController
      animateToViewController(chefHomeViewController)
      currentViewController = .Kitchen
    }
  }
  
  func goUserOrders() {
    if currentViewController != .Orders {
      viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateViewControllerWithIdentifier("UserOrdersViewController")]
      currentViewController = .Orders
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func goHistory() {
    if currentViewController != .History {
      viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateViewControllerWithIdentifier("HistoryViewController")]
      currentViewController = .History
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func goNotifications() {
//    if user!.isChef() && User.currentMode() == .Chef {
//      
//    } else {
      if currentViewController != .Notifications {
        viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateViewControllerWithIdentifier("NotificationsViewController")]
        currentViewController = .Notifications
      }
      dismissViewControllerAnimated(true, completion: nil)
//    }
  }
  
  func goSettings() {
    if currentViewController != .Settings {
      viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateViewControllerWithIdentifier("SettingsViewController")]
      currentViewController = .Settings
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func goCreateChefProfile() {
//    if currentViewController != .UserHome {
//      viewControllers = [UIStoryboard(name: "User", bundle: nil).instantiateInitialViewController()]
//    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func animateToViewController(viewController: UIViewController) {
    dismissViewControllerAnimated(true) {
      if let lastViewController = self.viewControllers.last as? UIViewController {
        let snapshot = lastViewController.view.snapshotViewAfterScreenUpdates(true)
//        let blurredSnapshotView = UIImage.blurredView(snapshot)
        self.viewControllers = [viewController]
        self.view.insertSubview(snapshot, belowSubview: viewController.view)
//        self.view.insertSubview(blurredSnapshotView, belowSubview: snapshot)
        println(viewController)
        viewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, self.view.bounds.width, 0)
        UIView.animateWithDuration(0.5) {
          snapshot.layer.transform = CATransform3DScale(CATransform3DIdentity, 0.7, 0.7, 1)
          snapshot.layer.transform = CATransform3DTranslate(snapshot.layer.transform, 0, 0, -1000)
        }
        UIView.animateWithDuration(0.5, delay: 0.5, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: .BeginFromCurrentState | .CurveEaseInOut, animations: {
          viewController.view.transform = CGAffineTransformIdentity
          }, completion: { finished in
            snapshot.removeFromSuperview()
//            blurredSnapshotView.removeFromSuperview()
        })
      }
    }
  }
}
