//
//  ChefOrderViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ChefOrderViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var orderCollectionDelegate: OrderCollectionDelegate!
  @IBOutlet weak var actionButton: UIButton!
  
  var order: Order!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
    configureWithOrder(order)
    refreshOrder()
  }
  
  func configure() {
    tableView.registerNib(UINib(nibName: "OrderStatusCell", bundle: nil), forCellReuseIdentifier: "OrderStatusCell")
    tableView.registerNib(UINib(nibName: "OrderInfoCell", bundle: nil), forCellReuseIdentifier: "OrderInfoCell")
    tableView.registerNib(UINib(nibName: "OrderMapCell", bundle: nil), forCellReuseIdentifier: "OrderMapCell")
    tableView.registerNib(UINib(nibName: "OrderMapFullCell", bundle: nil), forCellReuseIdentifier: "OrderMapFullCell")
    tableView.registerNib(UINib(nibName: "OrderUserCell", bundle: nil), forCellReuseIdentifier: "OrderUserCell")
    tableView.registerNib(UINib(nibName: "OrderActionCell", bundle: nil), forCellReuseIdentifier: "OrderActionCell")
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
  }
  
  func configureWithOrder(order: Order) {
    self.order = order
    if order.deliveryType! == .Delivery {
      if order.status == .Ready {
        orderCollectionDelegate.datasource = [.Status, .Info, .MapFull]
      } else {
        orderCollectionDelegate.datasource = [.Status, .Info, .Map, .Action]
      }
    } else {
      if order.status == .Ready {
        orderCollectionDelegate.datasource = [.Status, .Info, .User]
      } else {
        orderCollectionDelegate.datasource = [.Status, .Info, .User, .Action]
      }
    }
    orderCollectionDelegate.order = order
    
    if order.status == .Ready {
      actionButton?.removeFromSuperview()
      tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    } else if order.status != .Confirmation {
      actionButton?.setTitle("prato pronto!", forState: .Normal)
    }
    tableView.reloadData()
  }
  
  func refreshOrder() {
    order.info() {
      switch $0 {
      case .Success(let boxed):
        self.configureWithOrder(boxed.unbox)
      case .Failure(let error):
        println(error)
      }
    }
  }
  
  // MARK: Order
  @IBAction func acceptTapped(sender: UIButton) {
    if order.status == .Confirmation {
      order.accept(true) {
        switch $0 {
        case .Success:
          let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
          if self.order.deliveryType! == .Delivery {
            customAlert.configureForAcceptOrderDelivery()
          } else {
            customAlert.configureForAcceptOrderTakeaway()
          }
          customAlert.present()
          customAlert.callback = {
            self.navigationController?.popViewControllerAnimated(true)
          }
        case .Failure(let error):
          println(error.localizedDescription)
        }
      }
    } else {
      order.foodReady() {
        switch $0 {
        case .Success(let boxed):
          let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
          customAlert.configureForOrderReady()
          customAlert.present()
          customAlert.callback = {
            self.navigationController?.popViewControllerAnimated(true)
          }
        case .Failure(let error):
          println(error)
        }
      }
    }
  }
  
  func declineOrder() {
    order.accept(false) {
      switch $0 {
      case .Success:
        let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
        customAlert.configureForCancelOrder()
        customAlert.present()
        customAlert.callback = {
          self.navigationController?.popViewControllerAnimated(true)
        }
      case .Failure(let error):
        println(error)
      }
    }
  }
  
  func cancelOrder() {
    order.cancel() {
      switch $0 {
      case .Success(let boxed):
        let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
        customAlert.configureForCancelOrder()
        customAlert.present()
        customAlert.callback = {
          self.navigationController?.popViewControllerAnimated(true)
        }
      case .Failure(let error):
        println(error)
      }
    }
  }
}
