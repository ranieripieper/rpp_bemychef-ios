//
//  OpenKitchenDeliveryViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OpenKitchenDeliveryViewController: UIViewController {
  @IBOutlet weak var takeawayButton: UIButton!
  @IBOutlet weak var bothButton: UIButton!
  @IBOutlet weak var deliveryButton: UIButton!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  var foods: [Food]!
  
  // MARK: Buttons
  @IBAction func buttonTapped(sender: UIButton) {
    takeawayButton.selected = sender == takeawayButton
    bothButton.selected = sender == bothButton
    deliveryButton.selected = sender == deliveryButton
    if takeawayButton.selected {
      descriptionLabel.text = "Nessa opção, quando o pedido estiver pronto, o comprador vai retirá-lo na sua casa."
    } else if bothButton.selected {
      descriptionLabel.text = "Nessa opção, ou você pode ir entregar ou o comprador pode vir buscar o pedido pronto. Ele escolhe na hora da compra!"
    } else {
      descriptionLabel.text = "Nessa opção, você leva o pedido até a casa do comprador. A distância máxima é de 2km e você recebe a taxa de R$5,00 pela entrega."
    }
  }
  
  // MARK: Open
  @IBAction func openTapped(sender: UIButton) {
    if validate() {
      if let mainNavigationController = self.navigationController as? MainNavigationController, let user = mainNavigationController.user, let chef = user.chef, let kitchen = chef.kitchens.first {
        kitchen.open(foods, deliveryType: deliveryType()) {
          switch $0 {
          case .Success:
            if let chefHomeViewController = self.navigationController?.viewControllers.first as? ChefHomeViewController {
              chefHomeViewController.didOpen()
            }
          case .Failure:
            println($0)
          }
        }
      }

    }
  }
  
  func validate() -> Bool {
    return takeawayButton.selected || bothButton.selected || deliveryButton.selected
  }
  
  func deliveryType() -> DeliveryType {
    if takeawayButton.selected {
      return .Takeaway
    } else if bothButton.selected {
      return .Both
    } else {
      return .Delivery
    }
  }
}
