//
//  OpenKitchenMenuViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OpenKitchenMenuViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var openKitchenMenuCollectionDelegate: OpenKitchenMenuCollectionDelegate!
  @IBOutlet weak var animator: ZipcodeAnimator!
  
  var foods: [Food]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "ChefFoodCell", bundle: nil), forCellReuseIdentifier: "ChefFoodCell")
    tableView.registerNib(UINib(nibName: "ChefHomeNewFoodCell", bundle: nil), forCellReuseIdentifier: "ChefHomeNewFoodCell")
    tableView.tableFooterView = UIView(frame: CGRectZero)
    openKitchenMenuCollectionDelegate.datasource = foods
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let openKitchenMenuOptionsViewController = segue.destinationViewController as? OpenKitchenMenuOptionsViewController, let index = sender as? Int {
      openKitchenMenuOptionsViewController.transitioningDelegate = animator
      openKitchenMenuOptionsViewController.food = foods[index]
      openKitchenMenuOptionsViewController.callback = {
        if $0 {
          self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "Abrir Cozinha", style: .Bordered, target: self, action: "openKitchen")]
        } else {
          self.tableView.deselectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: true)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
      }
    } else if let openKitchenDeliveryViewController = segue.destinationViewController as? OpenKitchenDeliveryViewController {
      if let indexPaths = tableView.indexPathsForSelectedRows() {
        openKitchenDeliveryViewController.foods = indexPaths.map() {
          self.foods[$0.row]
        }
      }
    }
  }
  
  // MARK: New Food
  func addNewFood() {
    performSegueWithIdentifier("SegueNewFood", sender: nil)
  }
  
  // MARK: Food
  func presentOptionsForIndex(index: Int) {
    performSegueWithIdentifier("SegueFoodOptions", sender: index)
  }
  
  func didDeselectFood() {
    if let indexPaths = tableView.indexPathsForSelectedRows() where indexPaths.count > 0 {
      
    } else {
      navigationItem.rightBarButtonItems = []
    }
  }
  
  // MARK: Open
  func openKitchen() {
    performSegueWithIdentifier("SegueDelivery", sender: nil)
  }
}
