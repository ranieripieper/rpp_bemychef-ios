//
//  IngredientsViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class IngredientsViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var addIngredientsButton: UIButton!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var doneButton: UIButton!
  @IBOutlet weak var addIngredientsButtonTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var doneButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
  
  var ingredients = [Ingredient]() {
    didSet {
      tableViewHeightConstraint.constant = CGFloat(ingredients.count * 44)
      tableView.reloadData()
      if ingredients.count == 0 {
        configureViewNoIngredient()
      } else {
        configureViewIngredients()
      }
    }
  }
  var food: Food?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    doneButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 20 * 2
    tableViewHeightConstraint.constant = 0
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let ingredientViewController = segue.destinationViewController as? IngredientViewController {
      ingredientViewController.callback = {
        self.ingredients.append($0)
        self.navigationController?.popViewControllerAnimated(true)
      }
    }
  }
  
  func configureViewNoIngredient() {
    doneButton.hidden = true
    addIngredientsButton.backgroundColor = UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1)
    addIngredientsButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    addIngredientsButtonTopSpaceConstraint.constant = 80
  }
  
  func configureViewIngredients() {
    doneButton.hidden = false
    addIngredientsButton.backgroundColor = UIColor.whiteColor()
    addIngredientsButton.setTitleColor(UIColor(white: 0.3, alpha: 1.0), forState: .Normal)
    addIngredientsButtonTopSpaceConstraint.constant = 20
  }
  
  // MARK: Ingredients
  @IBAction func send(sender: UIButton) {
    if let mainNavigationController = self.navigationController as? MainNavigationController, let user = mainNavigationController.user, let chef = user.chef, let kitchen = chef.kitchens.first {
      Food.insertFood(food!.name, description: food!.subtitle, amount: food!.amountAvailable, price: food!.price, type: FoodType.Regular.hashValue, prepTime: Float(food!.preparingTime), kitchenId: kitchen.id, ingredientsId: ingredients.map() { $0.id }, pictures: food!.pictures as! [UIImage]) {
        switch $0 {
        case .Success(let boxed):
          self.navigationController?.popToRootViewControllerAnimated(true)
        case .Failure(let error):
          println(error)
        }
      }
    }
  }
}
