//
//  ChefHomeViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ChefHomeViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var chefFirstNameLabel: UILabel!
  @IBOutlet weak var chefLastNameLabel: UILabel!
  @IBOutlet weak var chefAvatarImageView: UIImageView! {
    didSet {
      chefAvatarImageView.layer.borderColor = UIColor.whiteColor().CGColor
    }
  }
  @IBOutlet weak var chefRatingView: RatingView!
  @IBOutlet weak var chefRatesLabel: UILabel!
  @IBOutlet weak var chefHomeCollectionDelegate: ChefHomeCollectionDelegate!
  @IBOutlet weak var avatarImageViewTopSpaceConstraint: NSLayoutConstraint!
  
  enum Mode {
    case None, Foods, History, Orders
  }
  
  lazy var user: User = {
    if let mainNavigationController = self.navigationController as? MainNavigationController {
      return mainNavigationController.user!
    } else {
      return User.persistedUser()!
    }
  }()
  var mode: Mode = .None

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "ChefFoodCell", bundle: nil), forCellReuseIdentifier: "ChefFoodCell")
    tableView.registerNib(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
    tableView.registerNib(UINib(nibName: "ChefHomeEmptyCell", bundle: nil), forCellReuseIdentifier: "ChefHomeEmptyCell")
    tableView.registerNib(UINib(nibName: "ChefHomeNewFoodCell", bundle: nil), forCellReuseIdentifier: "ChefHomeNewFoodCell")
    tableView.registerNib(UINib(nibName: "ChefHomeSelectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChefHomeSelectionHeader")
    configure()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    refresh()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let chefOrderViewController = segue.destinationViewController as? ChefOrderViewController, let order = sender as? Order {
      chefOrderViewController.order = order
    } else if let openKitchenMenuViewController = segue.destinationViewController as? OpenKitchenMenuViewController {
      openKitchenMenuViewController.foods = user.chef!.kitchens.first!.foods
    }
  }
  
  // MARK: Chef
  func configure() {
    chefFirstNameLabel.text = user.name.componentsSeparatedByString(" ").first
    chefLastNameLabel.text = user.name.componentsSeparatedByString(" ").last
    chefAvatarImageView.setImageWithURL(NSURL(string: user.avatarURL))
    chefRatingView.rating = user.chef!.rating
    chefRatesLabel.text = "(\(user.chef!.rates) avaliações)"
    chefRatingView.dark = true
    configureOpenCloseButton()
  }
  
  func configureOpenCloseButton() {
    if let chef = user.chef, let kitchen = chef.kitchens.first {
      let attributes = [NSFontAttributeName: UIFont(name: "ProximaNovaS-Regular", size: 18)!, NSForegroundColorAttributeName: UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1)]
      if kitchen.open {
        if chefHomeCollectionDelegate.headerSideSelected == .Left {
          mode = .Orders
        } else {
          mode = .Foods
        }
        let closeButton = UIBarButtonItem(title: "fechar cozinha", style: .Bordered, target: self, action: "closeKitchenTapped")
        closeButton.tintColor = UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1)
        closeButton.setTitleTextAttributes(attributes, forState: .Normal)
        navigationItem.rightBarButtonItems = [closeButton]
      } else {
        if chefHomeCollectionDelegate.headerSideSelected == .Left {
          mode = .Foods
        } else {
          mode = .History
        }
        let openButton = UIBarButtonItem(title: "abrir cozinha", style: .Bordered, target: self, action: "openKitchen")
        openButton.tintColor = UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1)
        openButton.setTitleTextAttributes(attributes, forState: .Normal)
        navigationItem.rightBarButtonItems = [openButton]
      }
    } else {
      navigationItem.rightBarButtonItems = []
    }
  }
  
  func refresh() {
    user.chef!.myKitchens() {
      switch $0 {
      case .Success(let boxed):
        self.configureOpenCloseButton()
        if self.mode == .Foods {
          self.chefHomeCollectionDelegate.datasource = [self.user.chef!.kitchens.first!.foods]
        }
        self.tableView.scrollEnabled = self.user.chef!.kitchens.first!.foods.count > 0
        self.tableView.reloadData()
      case .Failure(let error):
        println(error)
      }
    }
    user.chef!.myOrders(1) {
      switch $0 {
      case .Success(let boxed):
        self.user.chef!.openOrders = boxed.unbox.filter() {
          $0.status == Status.Confirmation || $0.status == Status.Ready || $0.status == Status.Production
        }
        self.user.chef!.orderHistory = boxed.unbox.filter() {
          $0.status == .Delivered
        }
        if self.mode == .Orders {
          self.chefHomeCollectionDelegate.datasource = self.user.chef!.openOrders
        } else if self.mode == .History {
          self.chefHomeCollectionDelegate.datasource = self.user.chef!.orderHistory
        }
        self.tableView.scrollEnabled = self.user.chef!.kitchens.first!.foods.count > 0
        self.tableView.reloadData()
      case .Failure(let error):
        println(error)
      }
    }
  }
  
  func openKitchen() {
    performSegueWithIdentifier("SegueOpenKitchen", sender: nil)
  }
  
  func didOpen() {
    navigationController?.popToViewController(self, animated: true)
    let noActionAlert = NSBundle.mainBundle().loadNibNamed("NoActionAlert", owner: self, options: nil).first as! NoActionAlert
    noActionAlert.setKitchenOpen()
    noActionAlert.present()
  }
  
  func closeKitchenTapped() {
    if user.chef!.openOrders.count > 0 {
      let singleButtonAlert = NSBundle.mainBundle().loadNibNamed("SingleButtonAlert", owner: self, options: nil).first as! SingleButtonAlert
      singleButtonAlert.present()
      singleButtonAlert.callback = {
        self.closeKitchen()
      }
    } else {
      closeKitchen()
    }
  }
  
  func closeKitchen() {
    if let chef = user.chef, let kitchen = chef.kitchens.first {
      kitchen.close() {
        switch $0 {
        case .Success:
          let noActionAlert = NSBundle.mainBundle().loadNibNamed("NoActionAlert", owner: self, options: nil).first as! NoActionAlert
          noActionAlert.setKitchenClosed()
          noActionAlert.present()
          self.configureOpenCloseButton()
          self.tableView.reloadData()
        case .Failure:
          println($0)
        }
      }
    }
  }
  
  // MARK: Order
  func goToOrder(order: Order) {
    performSegueWithIdentifier("SegueOrder", sender: order)
  }
  
  // MARK: New Food
  func addNewFood() {
    performSegueWithIdentifier("SegueNewFood", sender: nil)
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    if let mainNavigationController = navigationController as? MainNavigationController {
      mainNavigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
}
