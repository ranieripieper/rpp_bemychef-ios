//
//  OpenKitchenMenuOptionsViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OpenKitchenMenuOptionsViewController: UIViewController {
  @IBOutlet weak var foodNameLabel: UILabel!
  @IBOutlet weak var amountTextField: TextField!
  @IBOutlet weak var priceTextField: TextField!
  @IBOutlet weak var fullPriceLabel: UILabel!
  @IBOutlet weak var servesCountTextField: TextField!
  @IBOutlet weak var preparingTimeTextField: TextField!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var currencyTextFieldDelegate: CurrencyTextFieldDelegate!
  @IBOutlet weak var hourTextFieldDelegate: HourTextFieldDelegate!
  
  var food: Food!
  var callback: ((Bool) -> ())?
  var currentPrice: Float = 0
  var currentMinutes = 0
  lazy var numberFormatter: NSNumberFormatter = {
    let numberFormatter = NSNumberFormatter()
    numberFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    numberFormatter.numberStyle = .CurrencyStyle
    numberFormatter.minimumIntegerDigits = 1
    numberFormatter.minimumFractionDigits = 2
    numberFormatter.maximumFractionDigits = 2
    return numberFormatter
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(tap)
    currencyTextFieldDelegate.delegate = self
    hourTextFieldDelegate.delegate = self
  }
  
  // MARK: Tap
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    let didResign = amountTextField.resignFirstResponder() || priceTextField.resignFirstResponder() || preparingTimeTextField.resignFirstResponder()
    if !didResign && !contentView.frame.contains(sender.locationInView(sender.view)) {
      callback?(false)
    }
  }
  
  // MARK: OK
  @IBAction func okTapped(sender: UIButton) {
    if validateFields() {
      food.amountAvailable = amountTextField.text.toInt() ?? 0
      food.price = currentPrice
      food.preparingTime = currentMinutes
      food.servesCount = servesCountTextField.text.toInt() ?? 0
      callback?(true)
    }
  }
  
  func validateFields() -> Bool {
    return count(amountTextField.text) > 0 && currentPrice > 0 && count(servesCountTextField.text) > 0 && currentMinutes > 0
  }
}

extension OpenKitchenMenuOptionsViewController: CurrencyTextFieldDelegateDelegate {
  func currencyNewValue(value: Float) {
    currentPrice = value
    let fullValue = value * Float(1.2)
    fullPriceLabel.text = numberFormatter.stringFromNumber(NSNumber(float: fullValue))
  }
}

extension OpenKitchenMenuOptionsViewController: HourTextFieldDelegateDelegate {
  func hourNewValue(value: Int) {
    currentMinutes = value
  }
}
