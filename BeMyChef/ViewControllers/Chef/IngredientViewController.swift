//
//  IngredientViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class IngredientViewController: UIViewController {
  @IBOutlet weak var textField: TextField!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var toolbar: UIToolbar!
  @IBOutlet weak var autocompleteCollectionDelegate: AutocompleteCollectionDelegate!
  
  var ingredients = Ingredient.all()
  var callback: ((Ingredient) -> ())?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
    textField.inputAccessoryView = toolbar
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: tableView)
    keyboardNotificationObserver.installNotifications()
  }
  
  func configure() {
    tableView.registerNib(UINib(nibName: "AutocompleteCell", bundle: nil), forCellReuseIdentifier: "AutocompleteCell")
    autocompleteCollectionDelegate.originalDatasource = ingredients.map() {
      $0.name
    }
    autocompleteCollectionDelegate.callback = { index in
      let ingredient = self.ingredients.filter() {
        $0.name == self.autocompleteCollectionDelegate.datasource[index]
        }.first
      if ingredient != nil {
        self.callback?(ingredient!)
      }
    }
    textField.becomeFirstResponder()
  }
  
  // MARK: InputView
  @IBAction func cancel(sender: UIButton) {
    navigationController?.popViewControllerAnimated(true)
  }
}

extension IngredientViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    autocompleteCollectionDelegate.datasource = autocompleteCollectionDelegate.originalDatasource.filter() {
      let text = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
      return count(text) == 0 || $0.rangeOfString(text, options: .DiacriticInsensitiveSearch | .CaseInsensitiveSearch, range: nil, locale: nil) != nil
    }
    tableView.reloadData()
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    return false
  }
}
