//
//  KitchenDescriptionViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class KitchenDescriptionViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var picture1Button: UIButton!
  @IBOutlet weak var picture2Button: UIButton!
  @IBOutlet weak var picture3Button: UIButton!
  @IBOutlet weak var takePictureButton: UIButton!
  @IBOutlet weak var takePictureLabel: UILabel!
  @IBOutlet weak var descriptionTextView: UITextView!
  @IBOutlet weak var nextButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var takePictureLeftSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var signupInfo: [String: AnyObject]?
  var startViewController: StartViewController?
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var pictures = [UIImage]()
  
  let descriptionPlaceholder = "Tudo começou num belo dia de verão*..."
  
  var edit = false {
    didSet {
      if edit {
        self.view.backgroundColor = UIColor(red: 250 / 255, green: 248 / 255, blue: 236 / 255, alpha: 1)
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    nextButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - (8 + 20) * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    picture1Button.layer.borderColor = UIColor.whiteColor().CGColor
    picture2Button.layer.borderColor = UIColor.whiteColor().CGColor
    picture3Button.layer.borderColor = UIColor.whiteColor().CGColor
    takePictureButton.layer.borderColor = UIColor.whiteColor().CGColor
    picture1Button.imageView?.contentMode = .ScaleAspectFill
    picture2Button.imageView?.contentMode = .ScaleAspectFill
    picture3Button.imageView?.contentMode = .ScaleAspectFill
    takePictureButton.imageView?.contentMode = .ScaleAspectFit
    view.addGestureRecognizer(tap)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = self
    }
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = nil
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let paymentInfoViewController = segue.destinationViewController as? PaymentInfoViewController {
      paymentInfoViewController.signupInfo = signupInfo
      paymentInfoViewController.startViewController = startViewController
    }
  }
  
  // MARK: Tap
  @IBAction func tapped(sender: UITapGestureRecognizer!) {
    descriptionTextView.resignFirstResponder()
  }
  
  // MARK: Pictures
  @IBAction func addPictureTapped(sender: UIButton) {
    if imagePicker != nil || pictures.count >= 3 {
      return
    }
    let viewController = startViewController ?? self
    imagePicker = ImagePicker(viewController: viewController) { image in
      if image != nil {
        self.handleAddPicture(image!)
      }
      self.imagePicker = nil
    }
    imagePicker?.presentImagePicker()
  }
  
  func handleAddPicture(picture: UIImage) {
    pictures.append(picture)
    takePictureButton.selected = true
    takePictureLabel.hidden = true
    switch pictures.count {
    case 1:
      picture1Button.setImage(pictures.last!, forState: .Normal)
      picture1Button.hidden = false
      takePictureLeftSpaceConstraint.constant = picture2Button.frame.origin.x
    case 2:
      picture2Button.setImage(pictures.last!, forState: .Normal)
      picture2Button.hidden = false
      takePictureLeftSpaceConstraint.constant = picture3Button.frame.origin.x
    case 3:
      picture3Button.setImage(pictures.last!, forState: .Normal)
      picture3Button.hidden = false
      takePictureButton.hidden = true
    default:
      break
    }
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  @IBAction func deletePicture(sender: UIButton) {
    deletePictureAtIndex(sender.tag)
  }
  
  func deletePictureAtIndex(index: Int) {
    takePictureButton.hidden = false
    pictures.removeAtIndex(index)
    if pictures.count > 1 {
      picture1Button.setImage(pictures[0], forState: .Normal)
      picture2Button.setImage(pictures[1], forState: .Normal)
      picture3Button.hidden = true
      takePictureLeftSpaceConstraint.constant = picture3Button.frame.origin.x
    } else if pictures.count > 0 {
      picture1Button.setImage(pictures[0], forState: .Normal)
      picture2Button.setImage(nil, forState: .Normal)
      picture1Button.hidden = false
      picture2Button.hidden = true
      takePictureLeftSpaceConstraint.constant = picture2Button.frame.origin.x
    } else {
      picture1Button.setImage(nil, forState: .Normal)
      picture2Button.setImage(nil, forState: .Normal)
      picture1Button.hidden = true
      picture2Button.hidden = true
      takePictureLeftSpaceConstraint.constant = picture1Button.frame.origin.x
      takePictureLabel.hidden = false
    }
    picture3Button.setImage(nil, forState: .Normal)
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  // MARK: Send
  @IBAction func nextTapped(sender: UIButton) {
    if validateFields() {
      signupInfo!["kitchenDescription"] = descriptionTextView.text
      signupInfo!["kitchenPictures"] = pictures
      performSegueWithIdentifier("SeguePaymentInfo", sender: nil)
    }
  }
  
  func validateFields() -> Bool {
    return descriptionTextView.text != descriptionPlaceholder && count(descriptionTextView.text) > 0 && pictures.count > 0
  }
}

extension KitchenDescriptionViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return scrollView.contentOffset.y <= 0
  }
}

extension KitchenDescriptionViewController: UITextViewDelegate {
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    if textView.text == descriptionPlaceholder {
      textView.text = ""
    }
    return true
  }
  
  func textViewShouldEndEditing(textView: UITextView) -> Bool {
    if textView.text == "" {
      textView.text = descriptionPlaceholder
    }
    return true
  }
}
