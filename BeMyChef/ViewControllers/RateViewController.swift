//
//  RateViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RateViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  var order: Order!
  var currentRating = 0
  var currentComment = ""
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: tableView)
    keyboardNotificationObserver.customCallback = { (height: CGFloat, duration: CGFloat) in
      UIView.animateWithDuration(NSTimeInterval(duration)) {
        self.tableView.contentOffset = CGPoint(x: 0, y: self.tableView.contentSize.height + height - self.tableView.bounds.height)
      }
    }
    keyboardNotificationObserver.installNotifications()
    tableView.registerNib(UINib(nibName: "OrderStatusCell", bundle: nil), forCellReuseIdentifier: "OrderStatusCell")
    tableView.registerNib(UINib(nibName: "OrderInfoCell", bundle: nil), forCellReuseIdentifier: "OrderInfoCell")
    tableView.registerNib(UINib(nibName: "OrderActionCell", bundle: nil), forCellReuseIdentifier: "OrderActionCell")
  }
  
  // MARK: Send
  func sendTapped() {
    if validateFields() {
      order.food.comment(nil, text: currentComment, rating: Float(currentRating)) {
        switch $0 {
        case .Success(let boxed):
          println(boxed.unbox)
        case .Failure(let error):
          println(error)
        }
      }
    } else {
      
    }
  }
  
  func validateFields() -> Bool {
    return currentRating > 0 && count(currentComment) > 0
  }
}
