//
//  HomeViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var homeTableViewDelegate: HomeTableViewDelegate!
  
  var currentPage = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    refreshFoodsAtPage(currentPage + 1)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let foodDetailViewController = segue.destinationViewController as? FoodDetailViewController {
      if let food = sender as? Food {
        foodDetailViewController.food = food
      }
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    if let mainNavigationController = navigationController as? MainNavigationController {
      mainNavigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
  
  // MARK: Foods
  func refreshFoodsAtPage(page: Int) {
    Food.findNearFood(page, latitude: LocationManager.latitude, longitude: LocationManager.longitude) {
      switch $0 {
      case .Success(let boxed):
        if page == 1 {
          self.homeTableViewDelegate.foods.removeAll(keepCapacity: false)
        }
        self.homeTableViewDelegate.foods += boxed.unbox
        self.tableView.reloadData()
      case .Failure(let error):
        println(error)
      }
    }
  }
  
  // MARK: Deinit
  deinit {
    tableView?.dataSource = nil
    tableView?.delegate = nil
  }
}
