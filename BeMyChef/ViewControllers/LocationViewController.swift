//
//  LocationViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/24/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LocationViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var zipcodeTextField: TextField!
  @IBOutlet weak var streetTextField: TextField!
  @IBOutlet weak var numberTextField: TextField!
  @IBOutlet weak var complementTextField: TextField!
  @IBOutlet weak var doneButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var startViewController: StartViewController?
  var mainNavigationController: MainNavigationController?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var dimmingView: UIView?
  var dimmingView2: UIView?
  var zipcodeView: ZipcodeView?
  var user: User!
  var userAddresses = [(Address, String)]() {
    didSet {
      if let zipcodeView = zipcodeView {
        zipcodeView.results[0] = userAddresses
        zipcodeView.tableView.reloadData()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    view.addGestureRecognizer(tap)
    doneButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - (20 + 8) * 2
    user.deliveredAddresses() {
      switch $0 {
      case .Success(let boxed):
        self.userAddresses = boxed.unbox.map() {
          ($0, $0.toString())
        }
      case .Failure(let error):
        println(error)
      }
    }
    if startViewController == nil {
      let bgView = UIView(frame: CGRect(x: 8, y: 8, width: view.bounds.width - 8 * 2, height: view.bounds.height - 8 * 2))
      bgView.backgroundColor = UIColor(red: 250 / 255, green: 248 / 255, blue: 236 / 255, alpha: 1)
      view.insertSubview(bgView, atIndex: 0)
    }
  }
  
  // MARK: Tapped
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    zipcodeTextField.resignFirstResponder() || streetTextField.resignFirstResponder() || numberTextField.resignFirstResponder() || complementTextField.resignFirstResponder()
  }
  
  // MARK: Zipcode
  @IBAction func getZipcodeTapped(sender: UIButton) {
    if let location = LocationManager.sharedInstance.location {
      GooglePlacesAPI.zipcode(Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude)) {
        switch $0 {
        case .Success(let boxed):
          self.presentZipcodeViewController(boxed.unbox)
        case .Failure(let error):
          break
        }
      }
    }
  }
  
  func presentZipcodeViewController(results: [(Address, String)]) {
    dimmingView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.superview!.bounds.height))
    dimmingView!.alpha = 0
    dimmingView!.backgroundColor = UIColor.blackColor()
    view.addSubview(dimmingView!)
//    dimmingView2 = UIView(frame: CGRect(x: 0, y: 0, width: startViewController!.view.bounds.width, height: startViewController!.view.superview!.bounds.height))
    if let startViewController = startViewController {
      dimmingView2 = UIView(frame: CGRect(x: 0, y: 0, width: startViewController.view.bounds.width, height: startViewController.view.superview!.bounds.height))
      dimmingView2!.alpha = 0
      dimmingView2!.backgroundColor = UIColor.blackColor()
      startViewController.view.insertSubview(dimmingView2!, atIndex: startViewController.view.subviews.count - 2)
    }
    
    zipcodeView = NSBundle.mainBundle().loadNibNamed("ZipcodeView", owner: self, options: nil).first as? ZipcodeView
    zipcodeView!.frame = CGRect(x: 16, y: (view.bounds.height - zipcodeView!.bounds.height) / 2, width: view.bounds.width - 16 * 2, height: zipcodeView!.bounds.height)
    view.addSubview(zipcodeView!)
    zipcodeView!.results = [userAddresses, results]
    zipcodeView!.callback = {
      self.zipcodeTextField.text = $0.zipcode
      self.streetTextField.text = $0.street
      self.numberTextField.text = $0.number != "0" ? $0.number : ""
      self.complementTextField.text = $0.complement != "0" ? $0.complement : ""
      self.dismissZipcodeViewController()
    }
    
    zipcodeView!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, view.bounds.height)
    
    UIView.animateWithDuration(0.3) {
      self.dimmingView!.alpha = 0.5
      self.dimmingView2?.alpha = 0.5
      self.zipcodeView!.transform = CGAffineTransformIdentity
    }
  }
  
  func dismissZipcodeViewController() {
    UIView.animateWithDuration(0.3, animations: {
      self.dimmingView?.alpha = 0
      self.dimmingView2?.alpha = 0
      self.zipcodeView?.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, self.view.bounds.height)
      }) { finished in
        self.dimmingView?.removeFromSuperview()
        self.dimmingView2?.removeFromSuperview()
        self.zipcodeView?.removeFromSuperview()
        self.dimmingView = nil
        self.dimmingView2 = nil
        self.zipcodeView = nil
    }
  }
  
  // MARK: Done
  @IBAction func doneTapped(sender: UIButton) {
    if validateFields() {
      let address = Address(zipcode: zipcodeTextField.text, street: streetTextField.text, number: numberTextField.text, complement: complementTextField.text)
      address.persist()
      dismissViewControllerAnimated(true) {
        if let mainNavigationController = self.mainNavigationController {
          mainNavigationController.goHome(true)
        } else {
          (UIApplication.sharedApplication().delegate as! AppDelegate).resumeMainApplicationFlow()
        }
      }
    }
  }
  
  func validateFields() -> Bool {
    return count(zipcodeTextField.text) > 0 && count(streetTextField.text) > 0
  }
}
