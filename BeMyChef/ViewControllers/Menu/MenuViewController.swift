//
//  MenuViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  @IBOutlet weak var homeButton: UIButton!
  @IBOutlet weak var ordersButton: UIButton!
  @IBOutlet weak var kitchenButton: UIButton!
  @IBOutlet weak var notificationsBadgeLabel: UILabel!
  @IBOutlet weak var profileButton: UIButton!
  
  var mainNavigationController: MainNavigationController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    switch User.currentMode() {
    case .User:
      configureForUser()
    case .Chef:
      configureForChef()
    }
  }
  
  // MARK: Home
  @IBAction func homeTapped(sender: UIButton!) {
    mainNavigationController.goHome()
  }
  
  // MARK: Orders
  @IBAction func ordersTapped(sender: UIButton!) {
    mainNavigationController.goUserOrders()
  }
  
  // MARK: Kitchen
  @IBAction func kitchenTapped(sender: UIButton!) {
    mainNavigationController.goKitchen()
  }
  
  // MARK: History
  @IBAction func historyTapped(sender: UIButton!) {
    mainNavigationController.goHistory()
  }
  
  // MARK: Notifications
  @IBAction func notificationsTapped(sender: UIButton!) {
    mainNavigationController.goNotifications()
  }
  
  // MARK: Settings
  @IBAction func settingsTapped(sender: UIButton!) {
    mainNavigationController.goSettings()
  }
  
  // MARK: Logout
  @IBAction func logoutTapped(sender: UIButton!) {
    User.logout()
    NetworkManager.sharedInstance.uninstallHeaderToken()
    (UIApplication.sharedApplication().delegate as! AppDelegate).resumeLoginApplicationFlow()
  }
  
  // MARK: Profile
  @IBAction func profileTapped(sender: UIButton!) {
    if let user = mainNavigationController.user {
      if user.isChef() {
        if User.currentMode() == .Chef {
          let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
          let locationViewController = loginStoryboard.instantiateViewControllerWithIdentifier("LocationViewController") as! LocationViewController
          locationViewController.user = user
          locationViewController.mainNavigationController = mainNavigationController
          presentViewController(locationViewController, animated: true, completion: nil)
          User.setMode(.User)
        } else {
          mainNavigationController.goKitchen(true)
          User.setMode(.Chef)
        }
      } else {
        mainNavigationController.goCreateChefProfile()
      }
    }
  }
  
  // MARK: Close
  @IBAction func closeTapped(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func configureForUser() {
    homeButton.hidden = false
    ordersButton.hidden = false
    kitchenButton.hidden = true
    if let user = mainNavigationController.user where user.isChef() {
      profileButton.setTitle("Trocar perfil para Chef", forState: .Normal)
      profileButton.setImage(UIImage(named: "img_menu_chef"), forState: .Normal)
    } else {
      profileButton.setTitle("Criar perfil de Chef", forState: .Normal)
      profileButton.setImage(nil, forState: .Normal)
    }
  }
  
  func configureForChef() {
    homeButton.hidden = true
    ordersButton.hidden = true
    kitchenButton.hidden = false
    profileButton.setTitle("Trocar perfil para usuário", forState: .Normal)
    profileButton.setImage(UIImage(named: "img_menu_user"), forState: .Normal)
  }
}
