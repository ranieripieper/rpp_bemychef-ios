//
//  NotificationsViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  var currentPage = 0
  var notifications: [String]?
  var user: User? {
    if let mainNavigationController = self.navigationController as? MainNavigationController {
      return mainNavigationController.user
    } else {
      return User.persistedUser()
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getNotifications()
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    if let mainNavigationController = navigationController as? MainNavigationController {
      mainNavigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
  
  // MARK: API
  func getNotifications() {
    user?.userNotifications(currentPage + 1) {
      switch $0 {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error)
      }
    }
  }
}
