//
//  DeliveryConfirmationViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 1/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class DeliveryConfirmationViewController: UIViewController {
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var deliveryPriceLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  
  var foodDetailViewController: FoodDetailViewController?
  var distance: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(tap)
    distanceLabel.text = (distance ?? "próximo") + " de você"
  }
  
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    if sender.state == .Ended {
      dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  @IBAction func delivery(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      if self.foodDetailViewController != nil {
        self.foodDetailViewController!.confirmOrderWithDeliveryType(.Delivery)
      }
    })
  }
  
  @IBAction func takeout(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      if self.foodDetailViewController != nil {
        self.foodDetailViewController!.confirmOrderWithDeliveryType(.Takeaway)
      }
    })
  }
}
