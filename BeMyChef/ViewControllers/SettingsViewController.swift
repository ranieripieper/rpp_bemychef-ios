//
//  SettingsViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
  @IBOutlet weak var emailNotificationsSelectionImageView: UIImageView!
  @IBOutlet weak var deleteAccountButton: UIButton! {
    didSet {
      deleteAccountButton.layer.borderColor = UIColor(red: 236 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1).CGColor
    }
  }
  @IBOutlet weak var editKitchenButton: UIButton?
  @IBOutlet weak var editPaymentButton: UIButton?
  @IBOutlet weak var termsTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var emailNotificationsWidthConstraint: NSLayoutConstraint!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let mainNavigationController = navigationController as? MainNavigationController, let user = mainNavigationController.user where !user.isChef() {
      termsTopConstraint.constant = 0
      editKitchenButton?.removeFromSuperview()
      editPaymentButton?.removeFromSuperview()
    }
    emailNotificationsWidthConstraint.constant = UIScreen.mainScreen().bounds.width
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let editProfileViewController = segue.destinationViewController as? EditProfileViewController {
      editProfileViewController.user = mainNavigationController()?.user
    }
  }
  
  func mainNavigationController() -> MainNavigationController? {
    if let mainNavigationController = navigationController as? MainNavigationController {
      return mainNavigationController
    } else {
      return nil
    }
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    mainNavigationController()?.performSegueWithIdentifier("SegueMenu", sender: nil)
  }
  
  // MARK: Email
  @IBAction func toggleEmailNotifications(sender: UIButton) {
    sender.selected = !sender.selected
    emailNotificationsSelectionImageView.image = UIImage(named: sender.selected ? "icn_settings_selected" : "icn_settings_deselected")
  }
  
  // MARK: Edit Kitchen
  @IBAction func editKitchen(sender: UIButton) {
    let storyboard = UIStoryboard(name: "Login", bundle: nil)
    if let registerKitchenAddressViewController = storyboard.instantiateViewControllerWithIdentifier("RegisterKitchenAddressViewController") as? RegisterKitchenAddressViewController {
      registerKitchenAddressViewController.edit = true
      navigationController?.pushViewController(registerKitchenAddressViewController, animated: true)
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  // MARK: Payment Info
  @IBAction func editPaymentInfo(sender: UIButton) {
    let storyboard = UIStoryboard(name: "Login", bundle: nil)
    if let paymentInfoViewController = storyboard.instantiateViewControllerWithIdentifier("PaymentInfoViewController") as? PaymentInfoViewController {
      paymentInfoViewController.edit = true
      navigationController?.pushViewController(paymentInfoViewController, animated: true)
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  // MARK: Logout
  @IBAction func logoutTapped(sender: UIButton) {
    User.logout()
    if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
      appDelegate.resumeLoginApplicationFlow()
    }
  }
  
  // MARK: Delete Account
  @IBAction func deleteAccountTapped(sender: UIButton) {
    
  }
}
