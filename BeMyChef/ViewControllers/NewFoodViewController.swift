//
//  NewFoodViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NewFoodViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var picture1Button: UIButton!
  @IBOutlet weak var picture2Button: UIButton!
  @IBOutlet weak var picture3Button: UIButton!
  @IBOutlet weak var takePictureButton: UIButton!
  @IBOutlet weak var takePictureLabel: UILabel!
  @IBOutlet weak var foodNameTextField: TextField!
  @IBOutlet weak var foodDescriptionTextView: UITextView!
  @IBOutlet weak var foodAmountAvailableTextField: TextField!
  @IBOutlet weak var foodPriceTextField: TextField!
  @IBOutlet weak var fullPriceLabel: UILabel!
  @IBOutlet weak var foodServesCountTextField: TextField!
  @IBOutlet weak var foodPrepTimeTextField: TextField!
  @IBOutlet weak var nextButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var takePictureLeftSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var currencyTextFieldDelegate: CurrencyTextFieldDelegate!
  @IBOutlet weak var hourTextFieldDelegate: HourTextFieldDelegate!
  
  let descriptionPlaceholder = "Descrição*"
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var pictures = [UIImage]()
  var currentPrice: Float = 0
  var currentMinutes = 0
  lazy var numberFormatter: NSNumberFormatter = {
    let numberFormatter = NSNumberFormatter()
    numberFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    numberFormatter.numberStyle = .CurrencyStyle
    numberFormatter.minimumIntegerDigits = 1
    numberFormatter.minimumFractionDigits = 2
    numberFormatter.maximumFractionDigits = 2
    return numberFormatter
    }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    nextButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 20 * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    picture1Button.layer.borderColor = UIColor.whiteColor().CGColor
    picture2Button.layer.borderColor = UIColor.whiteColor().CGColor
    picture3Button.layer.borderColor = UIColor.whiteColor().CGColor
    takePictureButton.layer.borderColor = UIColor.whiteColor().CGColor
    picture1Button.imageView?.contentMode = .ScaleAspectFill
    picture2Button.imageView?.contentMode = .ScaleAspectFill
    picture3Button.imageView?.contentMode = .ScaleAspectFill
    takePictureButton.imageView?.contentMode = .ScaleAspectFit
    view.addGestureRecognizer(tap)
    Ingredient.allIngredients()
    currencyTextFieldDelegate.delegate = self
    hourTextFieldDelegate.delegate = self
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let ingredientsViewController = segue.destinationViewController as? IngredientsViewController, let food = sender as? Food {
      ingredientsViewController.food = food
    }
  }
  
  // MARK: Tapped
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    foodNameTextField.resignFirstResponder() || foodDescriptionTextView.resignFirstResponder() || foodAmountAvailableTextField.resignFirstResponder() || foodPrepTimeTextField.resignFirstResponder() || foodPriceTextField.resignFirstResponder()
  }
  
  // MARK: Pictures
  @IBAction func addPictureTapped(sender: UIButton) {
    if imagePicker != nil || pictures.count >= 3 {
      return
    }
    imagePicker = ImagePicker(viewController: self) { image in
      if image != nil {
        self.handleAddPicture(image!)
      }
      self.imagePicker = nil
    }
    imagePicker?.presentImagePicker()
  }
  
  func handleAddPicture(picture: UIImage) {
    pictures.append(picture)
    takePictureButton.selected = true
    takePictureLabel.hidden = true
    switch pictures.count {
    case 1:
      picture1Button.setImage(pictures.last!, forState: .Normal)
      picture1Button.hidden = false
      takePictureLeftSpaceConstraint.constant = picture2Button.frame.origin.x
    case 2:
      picture2Button.setImage(pictures.last!, forState: .Normal)
      picture2Button.hidden = false
      takePictureLeftSpaceConstraint.constant = picture3Button.frame.origin.x
    case 3:
      picture3Button.setImage(pictures.last!, forState: .Normal)
      picture3Button.hidden = false
      takePictureButton.hidden = true
    default:
      break
    }
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  @IBAction func deletePicture(sender: UIButton) {
    deletePictureAtIndex(sender.tag)
  }
  
  func deletePictureAtIndex(index: Int) {
    takePictureButton.hidden = false
    pictures.removeAtIndex(index)
    if pictures.count > 1 {
      picture1Button.setImage(pictures[0], forState: .Normal)
      picture2Button.setImage(pictures[1], forState: .Normal)
      picture3Button.hidden = true
      takePictureLeftSpaceConstraint.constant = picture3Button.frame.origin.x
    } else if pictures.count > 0 {
      picture1Button.setImage(pictures[0], forState: .Normal)
      picture2Button.setImage(nil, forState: .Normal)
      picture1Button.hidden = false
      picture2Button.hidden = true
      takePictureLeftSpaceConstraint.constant = picture2Button.frame.origin.x
    } else {
      picture1Button.setImage(nil, forState: .Normal)
      picture2Button.setImage(nil, forState: .Normal)
      picture1Button.hidden = true
      picture2Button.hidden = true
      takePictureLeftSpaceConstraint.constant = picture1Button.frame.origin.x
      takePictureLabel.hidden = false
    }
    picture3Button.setImage(nil, forState: .Normal)
    UIView.animateWithDuration(0.3) {
      self.view.layoutIfNeeded()
    }
  }
  
  // MARK: Send
  @IBAction func nextTapped(sender: UIButton) {
    if validateFields() {
      let food = Food(id: 0, name: foodNameTextField.text, subtitle: foodDescriptionTextView.text, pictures: pictures, price: foodPriceTextField.text, type: FoodType.Regular, preparingTime: foodPrepTimeTextField.text, amountAvailable: foodAmountAvailableTextField.text, servesCount: foodServesCountTextField.text, reviews: nil, ingredients: [], kitchen: nil)
//      init(id: Int, name: String, subtitle: String, pictures: [UIImage]?, price: String, type: FoodType, preparingTime: String, amountAvailable: String, reviews: [Review]?, ingredients: [Ingredient], kitchen: Kitchen?)
      performSegueWithIdentifier("SegueIngredients", sender: food)
    }
  }
  
  func validateFields() -> Bool {
    return count(foodNameTextField.text) > 0 && count(foodDescriptionTextView.text) > 0 && count(foodAmountAvailableTextField.text) > 0 && count(foodServesCountTextField.text) > 0 && count(foodPriceTextField.text) > 0 && count(foodPrepTimeTextField.text) > 0
  }
}

extension NewFoodViewController: UITextViewDelegate {
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    if textView.text == descriptionPlaceholder {
      textView.text = nil
    }
    return true
  }
  
  func textViewShouldEndEditing(textView: UITextView) -> Bool {
    if textView.text == "" {
      textView.text = descriptionPlaceholder
    }
    return true
  }
}

extension NewFoodViewController: CurrencyTextFieldDelegateDelegate {
  func currencyNewValue(value: Float) {
    currentPrice = value
    let fullValue = value * Float(1.2)
    fullPriceLabel.text = numberFormatter.stringFromNumber(NSNumber(float: fullValue))
  }
}

extension NewFoodViewController: HourTextFieldDelegateDelegate {
  func hourNewValue(value: Int) {
    currentMinutes = value
  }
}

extension NewFoodViewController: TextFieldViewController {
  func lastTextFieldReturned(textField: UITextField) {
    
  }
}
