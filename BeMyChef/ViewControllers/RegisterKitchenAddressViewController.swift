//
//  RegisterKitchenAddressViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/7/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RegisterKitchenAddressViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var zipcodeTextField: TextField!
  @IBOutlet weak var streetTextField: TextField!
  @IBOutlet weak var numberTextField: TextField!
  @IBOutlet weak var complementTextField: TextField!
  @IBOutlet weak var phoneTextField: TextField!
  @IBOutlet weak var addressLabelWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!

  var signupInfo: [String: AnyObject]?
  var startViewController: StartViewController?
  var zipcodeView: ZipcodeView?
  var dimmingView: UIView?
  var dimmingView2: UIView?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  var edit = false {
    didSet {
      if edit {
        self.view.backgroundColor = UIColor(red: 250 / 255, green: 248 / 255, blue: 236 / 255, alpha: 1)
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addressLabelWidthConstraint.constant = UIScreen.mainScreen().bounds.width - (8 + 20) * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    view.addGestureRecognizer(tap)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = self
    }
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    if let loginNavigationControllerDelegate = navigationController?.delegate as? LoginNavigationControllerDelegate {
      loginNavigationControllerDelegate.panGesture.delegate = nil
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let kitchenDescriptionViewController = segue.destinationViewController as? KitchenDescriptionViewController {
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      kitchenDescriptionViewController.edit = edit
      kitchenDescriptionViewController.signupInfo = signupInfo
      kitchenDescriptionViewController.startViewController = startViewController
    }
  }
  
  // MARK: Tap
  @IBAction func tapped(sender: UITapGestureRecognizer!) {
    zipcodeTextField.resignFirstResponder() || streetTextField.resignFirstResponder() || numberTextField.resignFirstResponder() || complementTextField.resignFirstResponder() || phoneTextField.resignFirstResponder()
  }
  
  // MARK: Zipcode
  @IBAction func getZipcodeTapped(sender: UIButton) {
    if let location = LocationManager.sharedInstance.location {
      GooglePlacesAPI.zipcode(Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude)) {
        switch $0 {
        case .Success(let boxed):
          self.presentZipcodeViewController(boxed.unbox)
        case .Failure(let error):
          break
        }
      }
    }
  }
  
  func presentZipcodeViewController(results: [(Address, String)]) {
    dimmingView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.superview!.bounds.height))
    dimmingView!.alpha = 0
    dimmingView!.backgroundColor = UIColor.blackColor()
    view.addSubview(dimmingView!)
    if let startViewController = startViewController {
      dimmingView2 = UIView(frame: CGRect(x: 0, y: 0, width: startViewController.view.bounds.width, height: startViewController.view.superview!.bounds.height))
      dimmingView2!.alpha = 0
      dimmingView2!.backgroundColor = UIColor.blackColor()
      startViewController.view.insertSubview(dimmingView2!, atIndex: startViewController.view.subviews.count - 2)
    } else if let navigationController = navigationController {
      dimmingView2 = UIView(frame: CGRect(x: 0, y: 0, width: navigationController.view.bounds.width, height: 64))
      dimmingView2!.alpha = 0
      dimmingView2!.backgroundColor = UIColor.blackColor()
      navigationController.view.addSubview(dimmingView2!)
    }
    
    zipcodeView = NSBundle.mainBundle().loadNibNamed("ZipcodeView", owner: self, options: nil).first as? ZipcodeView
    zipcodeView!.frame = CGRect(x: 16, y: (view.bounds.height - zipcodeView!.bounds.height) / 2, width: view.bounds.width - 16 * 2, height: zipcodeView!.bounds.height)
    view.addSubview(zipcodeView!)
    zipcodeView!.results = [[], results]
    zipcodeView!.callback = {
      self.zipcodeTextField.text = $0.zipcode
      self.streetTextField.text = $0.street
      self.numberTextField.text = $0.number
      self.complementTextField.text = $0.complement
      self.dismissZipcodeViewController()
    }
    
    zipcodeView!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, view.bounds.height)
    
    UIView.animateWithDuration(0.3) {
      self.dimmingView!.alpha = 0.5
      self.dimmingView2!.alpha = 0.5
      self.zipcodeView!.transform = CGAffineTransformIdentity
    }
  }
  
  func dismissZipcodeViewController() {
    UIView.animateWithDuration(0.3, animations: {
      self.dimmingView?.alpha = 0
      self.dimmingView2?.alpha = 0
      self.zipcodeView?.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, self.view.bounds.height)
      }) { finished in
        self.dimmingView?.removeFromSuperview()
        self.dimmingView2?.removeFromSuperview()
        self.zipcodeView?.removeFromSuperview()
        self.dimmingView = nil
        self.dimmingView2 = nil
        self.zipcodeView = nil
    }
  }
  
  // MARK: Continue
  @IBAction func continueTapped(sender: UIButton) {
    signupInfo?["kitchenZipcode"] = zipcodeTextField.text
    signupInfo?["kitchenStreet"] = streetTextField.text
    signupInfo?["kitchenNumber"] = numberTextField.text
    signupInfo?["kitchenComplement"] = complementTextField.text
    signupInfo?["kitchenPhone"] = phoneTextField.text
    performSegueWithIdentifier("SegueKitchenDescription", sender: nil)
  }
}

extension RegisterKitchenAddressViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return scrollView.contentOffset.y <= 0 && dimmingView == nil
  }
}
