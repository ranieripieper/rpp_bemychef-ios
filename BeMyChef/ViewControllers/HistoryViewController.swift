//
//  HistoryViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/14/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var userOrdersCollectionDelegate: UserOrdersCollectionDelegate!
  
  var isLoading = false
  var currentPage = 0
  var endReached = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
    tableView.contentInset = UIEdgeInsets(top: 64 + 50, left: 0, bottom: 0, right: 0)
    getOrders(currentPage + 1)
    userOrdersCollectionDelegate.selectionCallback = goToOrder
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let orderDetailViewController = segue.destinationViewController as? OrderDetailViewController, let order = sender as? Order {
      orderDetailViewController.order = order
    }
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    if let mainNavigationController = navigationController as? MainNavigationController {
      mainNavigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
    }
  }
  
  // MARK: Order
  func getOrders(page: Int) {
    if isLoading || endReached {
      return
    }
    isLoading = true
    if let mainNavigationController = self.navigationController as? MainNavigationController, let user = mainNavigationController.user {
      user.orders(page) {
        switch $0 {
        case .Success(let boxed):
          if boxed.unbox.count == 0 {
            self.endReached = true
          } else {
            var orders: [Order]
            if page == 1 {
              orders = []
            } else {
              orders = self.userOrdersCollectionDelegate.orders ?? []
            }
            orders += boxed.unbox
            self.userOrdersCollectionDelegate.orders = orders
            self.tableView.reloadData()
            self.currentPage++
          }
          self.tableView.hidden = !(self.userOrdersCollectionDelegate.orders?.count > 0)
          self.isLoading = false
        case .Failure(let error):
          self.tableView.hidden = true
          self.isLoading = false
          println(error.localizedDescription)
        }
      }
    }
  }
  
  func goToOrder(order: Order) {
    performSegueWithIdentifier("SegueOrder", sender: order)
  }
}
