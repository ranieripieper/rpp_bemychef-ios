//
//  PaymentViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/22/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var nameTextField: TextField!
  @IBOutlet weak var numberTextField: CardTextField!
  @IBOutlet weak var expireTextField: CardTextField!
  @IBOutlet weak var cvvTextField: TextField!
  @IBOutlet weak var confirmButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  var food: Food?
  var reserve: Reserve?
  var amount: Int?
  var deliveryType: DeliveryType?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if food?.pictures.count > 0 {
      foodImageView.setImageWithURL(NSURL(string: food!.pictures.first as! String))
    }
    confirmButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 20 * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    view.addGestureRecognizer(tap)
    numberTextField.mask = "#### #### #### ####"
    expireTextField.mask = "##/####"
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let orderDetailViewController = segue.destinationViewController as? OrderDetailViewController, let order = sender as? Order {
      orderDetailViewController.order = order
    }
  }
  
  // MARK: Tap
  @IBAction func tapped(sender: UITapGestureRecognizer?) {
    nameTextField.resignFirstResponder() || numberTextField.resignFirstResponder() || expireTextField.resignFirstResponder() || cvvTextField.resignFirstResponder()
  }
  
  // MARK: Order
  @IBAction func confirmTapped(sender: UIButton!) {
    tapped(nil)
    if validateFields() {
      let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
      customAlert.configureForPayment()
      customAlert.present()
      customAlert.status = .Processing
      let paymentMethod = PaymentMethod(cardNameHolder: nameTextField.text, cardNumber: numberTextField.text.stringByReplacingOccurrencesOfString(" ", withString: ""), cardExpirationMonth: expireTextField.text.substringToIndex(advance(expireTextField.text.startIndex, 2)).toInt()!, cardExpirationYear: expireTextField.text.substringFromIndex(advance(expireTextField.text.startIndex, 3)).toInt()!, cardFlag: flagForCard())
      paymentMethod.cardCVV = cvvTextField.text.toInt()
      reserve!.confirmWithPaymentMethod(paymentMethod) {
        switch $0 {
        case .Success(let boxed):
          customAlert.status = .Approved
          customAlert.callback = {
            self.goToOrder(boxed.unbox)
          }
        case .Failure(let error):
          println(error.localizedDescription)
          customAlert.status = .Failed
        }
      }
    }
  }
  
  func validateFields() -> Bool {
    return count(nameTextField.text) > 0 && count(numberTextField.text) > 0/* && Luhn.validateString(numberTextField.text)*/ && count(expireTextField.text) == 7 && count(cvvTextField.text) > 0
  }
  
  func flagForCard() -> String {
    let flag = Luhn.typeFromString(numberTextField.text)
    switch flag {
    case .Visa:
      return "visa"
    case .Mastercard:
      return "master"
    case .Amex:
      return "amex"
    case .Discover:
      return "discover"
    case .DinersClub:
      return "diners"
    case .JCB:
      return "jcb"
    default:
      return "other"
    }
  }
  
  func goToOrder(order: Order) {
    performSegueWithIdentifier("SegueOrder", sender: order)
  }
}

extension PaymentViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if let vMaskTextField = textField as? VMaskTextField {
      return vMaskTextField.shouldChangeCharactersInRange(range, replacementString: string)
    }
    return true
  }
}
