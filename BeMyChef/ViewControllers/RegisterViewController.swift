//
//  RegisterViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/1/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarButton: UIButton!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField?
  @IBOutlet weak var passwordConfirmationTextField: UITextField?
  @IBOutlet weak var profileTypeButton: UIButton!
  @IBOutlet weak var userButton: UIButton!
  @IBOutlet weak var chefButton: UIButton!
  @IBOutlet weak var userLabel: UILabel!
  @IBOutlet weak var chefLabel: UILabel!
  
  @IBOutlet weak var registerButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var profileTypeButtonTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var chefButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var imagePicker: ImagePicker?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var alertView: AlertController?
  var startViewController: StartViewController?
  var zipcodeView: ZipcodeView?
  var dimmingView: UIView?
  var dimmingView2: UIView?
  
  var signupInfo: [String: AnyObject] = [:]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    registerButtonWidthConstraint.constant = UIScreen.mainScreen().bounds.width - (8 + 20) * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    avatarButton.layer.borderColor = UIColor.whiteColor().CGColor
    view.addGestureRecognizer(tap)
    if !UIApplication.sharedApplication().isRegisteredForRemoteNotifications() {
      UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    tap.cancelsTouchesInView = false
    prefill()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let termsViewController = segue.destinationViewController as? TermsViewController {
      termsViewController.signupInfo = signupInfo
      termsViewController.startViewController = startViewController
    } else if let registerKitchenAddressViewController = segue.destinationViewController as? RegisterKitchenAddressViewController {
      registerKitchenAddressViewController.signupInfo = signupInfo
      registerKitchenAddressViewController.startViewController = startViewController
    }
  }
  
  // MARK: View
  func removePasswordFields() {
    passwordTextField?.removeFromSuperview()
    passwordConfirmationTextField?.removeFromSuperview()
    profileTypeButtonTopSpaceConstraint.constant = 12
  }
  
  // MARK: Cancel
  @IBAction func cancel(sender: UIButton) {
    startViewController?.dismissViewController(nil)
  }
  
  // MARK: Tap
  @IBAction func tapped(sender: UITapGestureRecognizer!) {
    nameTextField.resignFirstResponder()
    emailTextField.resignFirstResponder()
    passwordTextField?.resignFirstResponder()
    passwordConfirmationTextField?.resignFirstResponder()
  }
  
  // MARK: Prefil
  func prefill() {
    if let name = signupInfo["name"] as? String {
      nameTextField.text = name
      removePasswordFields()
    }
    if let avatar = signupInfo["avatar"] as? String {
      avatarButton.imageView?.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: avatar)!), placeholderImage: nil, success: { _, _, image in
        if self.avatarButton.imageView?.image == nil {
          self.avatarButton.setBackgroundImage(image, forState: .Normal)
          self.avatarButton.setTitle(nil, forState: .Normal)
        }
      }, failure: { _, _, error in
        println(error)
      })
    }
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped(sender: UIButton) {
    if imagePicker != nil {
      return
    }
    imagePicker = ImagePicker(viewController: startViewController!) { image in
      if image != nil {
        self.avatarButton.setImage(image!, forState: .Normal)
        self.signupInfo["avatar"] = image!
      }
      self.imagePicker = nil;
    }
    imagePicker?.presentImagePicker()
  }
  
  // MARK: Register
  @IBAction func registerTapped(sender: UIButton!) {
    if validatePasswords() {
      if validateFields() {
        if let image = avatarButton.imageView?.image {
          signupInfo["avatar"] = image
        }
        signupInfo["name"] = nameTextField.text
        signupInfo["email"] = emailTextField.text
        if let password = passwordTextField?.text {
          signupInfo["password"] = passwordTextField!.text
        }
        signupInfo["chef"] = chefButton.selected
        if userButton.selected {
          performSegueWithIdentifier("SegueTerms", sender: nil)
        } else {
          performSegueWithIdentifier("SegueKitchenAddress", sender: nil)
        }
      } else {
        invalidFields()
      }
    } else {
      invalidPasswords()
    }
  }
  
  func validatePasswords() -> Bool {
    return (passwordTextField == nil || passwordConfirmationTextField == nil) || (count(passwordTextField!.text) > 0 && passwordTextField!.text == passwordConfirmationTextField!.text)
  }
  
  func validateFields() -> Bool {
    return count(nameTextField.text) > 0 && count(emailTextField.text) > 0 && (userButton.selected || chefButton.selected)
  }
  
  func invalidPasswords() {
    presentBannerWithText("Por favor, verifique se as senhas conferem")
  }
  
  func invalidFields() {
    presentBannerWithText("Por favor, preencha todos os campos abaixo")
  }
  
  func presentBannerWithText(text: String) {
//    let bannerView = BannerView(text: text)
//    bannerView.presentInViewController(self)
  }
  
  // MARK: User Type
  @IBAction func toggleProfileTypeButtons(sender: UIButton) {
    sender.selected = !sender.selected
    if sender.selected {
      chefButtonHeightConstraint.constant = 100
    } else {
      chefButtonHeightConstraint.constant = 0
    }
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: .CurveEaseOut, animations: {
      self.userLabel.hidden = !sender.selected
      self.chefLabel.hidden = !sender.selected
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
  
  @IBAction func userTapped(sender: UIButton) {
    sender.selected = true
    self.chefButton.selected = false
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(Float(NSEC_PER_SEC) * 0.1)), dispatch_get_main_queue()) {
      self.toggleProfileTypeButtons(self.profileTypeButton)
      self.profileTypeButton.setTitleColor(UIColor(red: 236.0/255.0, green: 111.0/255.0, blue: 102.0/255.0, alpha: 1), forState: .Normal)
      self.profileTypeButton.setTitle("Só quero comer", forState: .Normal)
      self.profileTypeButton.titleLabel?.font = UIFont(name: "ProximaNovaA-Regular", size: 16)
    }
  }
  
  @IBAction func chefTapped(sender: UIButton) {
    sender.selected = true
    self.userButton.selected = false
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(Float(NSEC_PER_SEC) * 0.1)), dispatch_get_main_queue()) {
      self.toggleProfileTypeButtons(self.profileTypeButton)
      self.profileTypeButton.setTitleColor(UIColor(red: 236.0/255.0, green: 111.0/255.0, blue: 102.0/255.0, alpha: 1), forState: .Normal)
      self.profileTypeButton.setTitle("Vou cozinhar também", forState: .Normal)
      self.profileTypeButton.titleLabel?.font = UIFont(name: "ProximaNovaA-Regular", size: 16)
    }
  }
}

extension RegisterViewController: TextFieldViewController {
  func lastTextFieldReturned(textField: UITextField) {
    tapped(nil)
  }
}
