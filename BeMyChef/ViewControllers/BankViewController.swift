//
//  BankViewController.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BankViewController: UIViewController {
  @IBOutlet weak var textField: TextField!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var toolbar: UIToolbar!
  @IBOutlet weak var autocompleteCollectionDelegate: AutocompleteCollectionDelegate!
  
  var banks = Bank.allBanks()
  var callback: ((Bank) -> ())?
  var edit = false {
    didSet {
      if edit {
        self.view.backgroundColor = UIColor(red: 250 / 255, green: 248 / 255, blue: 236 / 255, alpha: 1)
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
    textField.inputAccessoryView = toolbar
  }
  
  func configure() {
    tableView.registerNib(UINib(nibName: "AutocompleteCell", bundle: nil), forCellReuseIdentifier: "AutocompleteCell")
    autocompleteCollectionDelegate.originalDatasource = banks.map() {
      $0.code + " - " + $0.name
    }
    autocompleteCollectionDelegate.callback = { index in
      let bank = self.banks.filter() {
        $0.code + " - " + $0.name == self.autocompleteCollectionDelegate.datasource[index]
        }.first
      if bank != nil {
        self.callback?(bank!)
      }
    }
    textField.becomeFirstResponder()
  }
  
  // MARK: InputView
  @IBAction func cancel(sender: UIButton) {
    navigationController?.popViewControllerAnimated(true)
  }
}

extension BankViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    autocompleteCollectionDelegate.datasource = autocompleteCollectionDelegate.originalDatasource.filter() {
      let text = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
      return count(text) == 0 || $0.rangeOfString(text, options: .DiacriticInsensitiveSearch | .CaseInsensitiveSearch, range: nil, locale: nil) != nil
    }
    tableView.reloadData()
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    return false
  }
}
