//
//  ChefHomeNewFoodCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum CellType {
  case Short, Long
}

class ChefHomeNewFoodCell: UITableViewCell {
  @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
  
  var callback: (() -> ())?
  
  func configureWithType(type: CellType) {
    topSpaceConstraint.constant = type == .Short ? 16 : 35
    bottomSpaceConstraint.constant = type == .Short ? 16 : 35
  }
  
  @IBAction func newFoodTapped(sender: UIButton) {
    callback?()
  }
}
