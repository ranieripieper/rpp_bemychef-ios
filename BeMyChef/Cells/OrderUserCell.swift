//
//  OrderUserCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderUserCell: UITableViewCell {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  
  func configureWithUser(user: User?) {
    avatarImageView.setImageWithURL(NSURL(string: user?.avatarURL ?? ""))
    nameLabel.text = user?.name
  }
}
