//
//  ChefHomeEmptyCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ChefHomeEmptyCell: UITableViewCell {
  @IBOutlet weak var label: UILabel!
  
  func configureWithText(text: String) {
    label.text = text
  }
}
