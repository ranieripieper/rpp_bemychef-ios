//
//  FoodDetailIngredientsCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FoodDetailIngredientsCell: UITableViewCell {
  @IBOutlet weak var ingredientsLabel: UILabel!
  @IBOutlet weak var ingredientsLabelWidthConstraint: NSLayoutConstraint!
  
  func configureWithIngredients(ingredients: [Ingredient]) {
    var string = ""
    ingredients.reduce(0) { index, ingredient -> Int in
      if index == ingredients.count - 1 {
        string += " e " + ingredient.name
      } else {
        string += ", " + ingredient.name
      }
      return index + 1
    }
    ingredientsLabel.text = string
    ingredientsLabelWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 12 - 12
  }
}
