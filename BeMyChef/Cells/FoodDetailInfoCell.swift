//
//  FoodDetailInfoCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FoodDetailInfoCell: UITableViewCell {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var foodTypeImageView: UIImageView!
  @IBOutlet weak var foodNameLabel: UILabel!
  @IBOutlet weak var foodPriceLabel: UILabel!
  @IBOutlet weak var foodSubtitleLabel: UILabel!
  @IBOutlet weak var scrollViewTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var foodImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var foodImageViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var foodSubtitleLabelWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var foodTypeImageViewHeightConstraint: NSLayoutConstraint!
  
  func configureWithFood(food: Food) {
    if food.pictures.count > 0 {
      foodImageView.setImageWithURL(NSURL(string: food.pictures.first as! String))
    }
    foodImageViewWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    if food.type == .Regular {
      foodTypeImageView.image = nil
      foodTypeImageViewHeightConstraint.constant = 0
    } else {
      foodTypeImageView.image = UIImage(named: FoodType.imageNameForType(food.type))
    }
    foodNameLabel.text = food.name
    foodPriceLabel.text = String(format: "R$%.2f", food.price)
    foodSubtitleLabel.text = food.subtitle
    foodSubtitleLabelWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 20 - 20
  }
}
