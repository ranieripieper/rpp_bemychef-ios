//
//  FoodCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class FoodCell: UITableViewCell {
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var foodTypeImageView: UIImageView!
  @IBOutlet weak var chefImageView: UIImageView!
  @IBOutlet weak var chefOpenImageView: UIImageView!
  @IBOutlet weak var ratingView: RatingView!
  @IBOutlet weak var foodNameLabel: UILabel!
  @IBOutlet weak var foodPriceLabel: UILabel!
  @IBOutlet weak var chefNameLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    chefImageView.layer.borderColor = UIColor.whiteColor().CGColor
  }
  
  func configureWithFood(food: Food) {
    foodImageView.image = nil
    if food.pictures.count > 0 {
      foodImageView.setImageWithURL(NSURL(string: food.pictures.first as! String))
    }
    if food.type == .Regular {
      foodTypeImageView.image = nil
    } else {
      foodTypeImageView.image = UIImage(named: FoodType.imageNameForType(food.type))
    }
    chefImageView.setImageWithURL(NSURL(string: food.kitchen.chef!.user.avatarURL))
    chefOpenImageView.hidden = !food.kitchen.open
    ratingView.rating = food.kitchen.chef!.rating
    foodNameLabel.text = food.name
    foodPriceLabel.text = String(format: "R$%.2f", food.price)
    chefNameLabel.text = food.kitchen.chef!.user.name
    if let location = LocationManager.sharedInstance.location {
      distanceLabel.text = NSString(format: "%.1fkm", food.kitchen.distanceToLocation(location)) as String
    }
  }
}
