//
//  ReviewCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/22/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class ReviewCell: UICollectionViewCell {
  @IBOutlet weak var personImageView: UIImageView!
  @IBOutlet weak var personNameLabel: UILabel!
  @IBOutlet weak var reviewLabel: UILabel!
  @IBOutlet weak var reviewDateLabel: UILabel!
  
  func configureWithReview(review: Review) {
    personImageView.image = UIImage(named: review.user.avatarURL)
    personNameLabel.text = review.user.name
    reviewLabel.text = review.text
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd.MM.yy"
    reviewDateLabel.text = dateFormatter.stringFromDate(review.date)
  }
}
