//
//  FoodDetailMapCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class FoodDetailMapCell: UITableViewCell {
  @IBOutlet weak var mapView: MKMapView!
  
  func configureWithFood(food: Food) {
    var negative: Float
    if arc4random() % 2 == 0 {
      negative = -1
    } else {
      negative = 1
    }
    let latitude = Float(food.kitchen.location().coordinate.latitude) + negative * 0.001 * Float(arc4random() % 5)
    if arc4random() % 2 == 0 {
      negative = -1
    } else {
      negative = 1
    }
    let longitude = Float(food.kitchen.location().coordinate.longitude) + negative * 0.001 * Float(arc4random() % 5)
    let address = Address(latitude: latitude, longitude: longitude)
    
    mapView.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
    
    if let annotation = mapView.annotations.first as? Address {
      mapView.removeAnnotation(annotation)
    }
    mapView.addAnnotation(address)
  }
}
