//
//  OrderInfoCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderInfoCell: UITableViewCell {
  @IBOutlet weak var amountLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var deliveryTypeLabel: UILabel!
  @IBOutlet weak var deliveryTypeImageView: UIImageView!
  
  func configureWithOrder(order: Order) {
    amountLabel.text = String(order.amount)
    valueLabel.text = NSString(format: "R$%.2f", (Float(order.amount) * order.food.price)) as String
    deliveryTypeLabel.text = (order.deliveryType == .Delivery ? "chef entrega" : "ele busca")
    deliveryTypeImageView.image = UIImage(named: (order.deliveryType == .Delivery ? "img_confirm_entrega" : "img_confirm_retiro"))
  }
}
