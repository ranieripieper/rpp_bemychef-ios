//
//  ChefFoodCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ChefFoodCell: UITableViewCell {
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var selection: UIView!
  @IBOutlet weak var quantityLabel: UILabel!
  @IBOutlet weak var quantityValueLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var priceValueLabel: UILabel!
  
  var deleteCallback: (ChefFoodCell -> ())?
  
  func configureWithFood(food: Food, showsEditingControl: Bool, showsDetails: Bool) {
    foodImageView.image = nil
    foodImageView.setImageWithURL(NSURL(string: food.pictures.first as? String ?? ""))
    nameLabel.text = food.name
    deleteButton.hidden = !showsEditingControl
    if !showsDetails {
      quantityLabel.removeFromSuperview()
      quantityValueLabel.removeFromSuperview()
      priceLabel.removeFromSuperview()
      priceValueLabel.removeFromSuperview()
    } else {
      quantityValueLabel.text = String(food.amountAvailable)
      priceValueLabel.text = String(format: "R$%.2f", food.price)
    }
  }
  
  @IBAction func deleteFood(sender: UIButton) {
    deleteCallback?(self)
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    selection.hidden = !selected
  }
}
