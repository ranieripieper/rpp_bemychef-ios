//
//  FoodDetailReviewCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class FoodDetailReviewCell: UITableViewCell {
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var pageControl: UIPageControl!
  
  var reviews: [Review]?
  
  func configureWithFood(food: Food) {
    if food.pictures.count > 0 {
      foodImageView.setImageWithURL(NSURL(string: food.pictures.first as! String))
    }
    self.reviews = food.reviews
    pageControl.numberOfPages = food.reviews!.count
    collectionView.reloadData()
  }
}

extension FoodDetailReviewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return reviews?.count ?? 0
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return collectionView.bounds.size
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let review = reviews![indexPath.item]
    let reviewCell = collectionView.dequeueReusableCellWithReuseIdentifier("ReviewCell", forIndexPath: indexPath) as! ReviewCell
    reviewCell.configureWithReview(review)
    return reviewCell
  }
}

extension FoodDetailReviewCell: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let page = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
    pageControl.currentPage = page
  }
}
