//
//  SelectedIngredientsCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SelectedIngredientsCell: UITableViewCell {
  @IBOutlet weak var label: UILabel!
  
  var callback: (SelectedIngredientsCell -> ())?
  
  func configureWithName(name: String, callback: SelectedIngredientsCell -> ()) {
    label.text = name
    self.callback = callback
  }
  
  @IBAction func deleteTapped(sender: UIButton) {
    callback?(self)
  }
}
