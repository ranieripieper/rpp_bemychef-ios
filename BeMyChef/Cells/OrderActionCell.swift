//
//  OrderActionCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderActionCell: UITableViewCell {
  @IBOutlet weak var button: UIButton!
  
  var callback: (() -> ())?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    button.layer.borderColor = UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1).CGColor
  }
  
  func configureWithButtonType(buttonType: ButtonType) {
    button.setTitle(buttonType.0, forState: .Normal)
    callback = buttonType.1
    if buttonType.0 == "não recebi" || buttonType.0 == "enviar avaliação" {
      button.backgroundColor = UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1)
      button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }
  }
  
  @IBAction func tapped(sender: UIButton) {
    callback?()
  }
}
