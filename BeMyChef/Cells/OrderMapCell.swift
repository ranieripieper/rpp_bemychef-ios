//
//  OrderMapCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class OrderMapCell: UITableViewCell {
  @IBOutlet weak var mapView: MKMapView!
  
  func configureWithUser(user: User?) {
    if let user = user {
      let address = Address(latitude: user.address?.latitude ?? 0, longitude: user.address?.longitude ?? 0)
      mapView.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(address.latitude), longitude: CLLocationDegrees(address.longitude)), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
      
      if let annotation = mapView.annotations.first as? MKAnnotation {
        mapView.removeAnnotation(annotation)
      }
      mapView.addAnnotation(address)
    }
  }
}
