//
//  FoodDetailChefCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol FoodDetailChefCellDelegate {
  func goToChef(cell: FoodDetailChefCell)
}

class FoodDetailChefCell: UITableViewCell {
  @IBOutlet weak var chefButton: UIButton!
  @IBOutlet weak var chefOpenImageView: UIImageView!
  @IBOutlet weak var foodDeliveryImageView: UIImageView!
  @IBOutlet weak var foodDeliveryLabel: UILabel!
  @IBOutlet weak var chefFirstNameLabel: UILabel!
  @IBOutlet weak var chefLastNameLabel: UILabel!
  @IBOutlet weak var chefRatesLabel: UILabel!
  @IBOutlet weak var foodPreparingTimeLabel: UILabel!
  @IBOutlet weak var foodAvailableLabel: UILabel!
  @IBOutlet weak var ratingView: RatingView!
  
  weak var delegate: FoodDetailChefCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    ratingView.dark = true
    chefButton.layer.borderColor = UIColor.whiteColor().CGColor
  }
  
  func configureWithFood(food: Food) {
    chefOpenImageView.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: food.kitchen.chef!.user.avatarURL)!), placeholderImage: nil, success: { _, _, image in
      self.chefButton.setBackgroundImage(image, forState: .Normal)
    }, failure: { _, _, error in
      println(error)
    })
    chefButton.setImage(UIImage(named: food.kitchen.chef!.user.avatarURL), forState: .Normal)
    chefOpenImageView.hidden = !food.kitchen.open
    ratingView.rating = food.kitchen.chef!.rating
    switch food.kitchen.deliveryType {
    case .Both:
      foodDeliveryImageView.image = UIImage(named: "img_deliver_entrego_retira")
      foodDeliveryLabel.text = "você escolhe"
    case .Delivery:
      foodDeliveryImageView.image = UIImage(named: "img_deliver_entrega_aqui")
      foodDeliveryLabel.text = "o chef entrega"
    case .Takeaway:
      foodDeliveryImageView.image = UIImage(named: "img_deliver_eu_retiro")
      foodDeliveryLabel.text = "você busca"
    default:
      foodDeliveryImageView.image = UIImage(named: "")
      foodDeliveryLabel.text = ""
    }
    chefFirstNameLabel.text = food.kitchen.chef!.user.name.componentsSeparatedByString(" ").first
    chefLastNameLabel.text = food.kitchen.chef!.user.name.componentsSeparatedByString(" ").last
    chefRatesLabel.text = "(\(food.kitchen.chef!.rates) avaliações)"
    foodPreparingTimeLabel.text = "\(food.preparingTime)"
    foodAvailableLabel.text = "\(food.amountAvailable)"
  }
  
  @IBAction func tappedChef(sender: UIButton) {
    delegate?.goToChef(self)
  }
}
