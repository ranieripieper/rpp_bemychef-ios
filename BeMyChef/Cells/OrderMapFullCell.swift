//
//  OrderMapFullCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderMapFullCell: OrderMapCell {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  
  override func configureWithUser(user: User?) {
    super.configureWithUser(user)
    avatarImageView.setImageWithURL(NSURL(string: user?.avatarURL ?? ""))
    nameLabel.text = user?.name
    addressLabel.text = user?.address?.toString()
    emailLabel.text = user?.email
  }
}
