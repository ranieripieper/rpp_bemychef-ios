//
//  OrderStatusCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderStatusCell: UITableViewCell {
  @IBOutlet weak var pictureImageView: UIImageView!
  @IBOutlet weak var foodNameLabel: UILabel!
  @IBOutlet weak var statusLabel: UILabel!
  
  func configureWithOrder(order: Order) {
    pictureImageView.setImageWithURL(NSURL(string: order.food.pictures.first as? String ?? ""))
    foodNameLabel.text = order.food.name
    statusLabel.text = order.status.toString()
  }
}
