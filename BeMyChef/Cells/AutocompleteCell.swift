//
//  AutocompleteCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AutocompleteCell: UITableViewCell {
  @IBOutlet weak var label: UILabel!
  
  func configureWithText(text: String) {
    label.text = text
  }
}
