//
//  RateCommentCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RateCommentCell: UITableViewCell {
  @IBOutlet weak var commentTextView: UITextView!
  
  var callback: ((String) -> ())?
  let commentTextViewPlaceholder = "Comentários"
}

extension RateCommentCell: UITextViewDelegate {
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    callback?((textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text))
    return true
  }
  
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    if textView.text == commentTextViewPlaceholder {
      textView.text = ""
    }
    return true
  }
  
  func textViewShouldEndEditing(textView: UITextView) -> Bool {
    if textView.text == "" {
      textView.text = commentTextViewPlaceholder
    }
    return true
  }
}
