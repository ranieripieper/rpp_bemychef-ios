//
//  ZipcodeCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/20/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ZipcodeCell: UITableViewCell {
  @IBOutlet weak var addressLabel: UILabel!
  
  func configureWithAddress(address: String, highlighted: Bool) {
    addressLabel.text = address
    if highlighted {
      addressLabel.textColor = UIColor(red: 236 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1)
    } else {
      addressLabel.textColor = UIColor(white: 125 / 255, alpha: 1)
    }
  }
}
