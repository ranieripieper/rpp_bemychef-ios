//
//  ChefFoodDetailsCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var deliveryTypeImageView: UIImageView!
  @IBOutlet weak var amountLabel: UILabel!
  @IBOutlet weak var statusLabel: UILabel!
  
  func configureWithOrder(order: Order) {
    foodImageView.setImageWithURL(NSURL(string: order.food.pictures.first as? String ?? ""))
    nameLabel.text = order.food.name
    if let deliveryType = order.deliveryType {
      switch deliveryType {
      case .Takeaway:
        deliveryTypeImageView.image = UIImage(named: "img_chef_eu_retiro")
      case .Delivery:
        deliveryTypeImageView.image = UIImage(named: "img_chef_entrega_aqui")
      case .Both:
        deliveryTypeImageView.image = UIImage(named: "img_chef_entrego_retira")
      case .ProDelivery:
        deliveryTypeImageView.image = UIImage(named: "")
      }
    }
    amountLabel.text = String(order.amount)
    statusLabel.text = order.status.toString()
  }
}
