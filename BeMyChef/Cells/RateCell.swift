//
//  RateCell.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RateCell: UITableViewCell {
  @IBOutlet weak var rate1Button: UIButton!
  @IBOutlet weak var rate2Button: UIButton!
  @IBOutlet weak var rate3Button: UIButton!
  @IBOutlet weak var rate4Button: UIButton!
  @IBOutlet weak var rate5Button: UIButton!
  
  var currentRating = 0
  var callback: ((Int) -> ())?
  
  @IBAction func rateTapped(sender: UIButton) {
    currentRating = sender.tag
    configure()
    callback?(currentRating)
  }
  
  private func configure() {
    rate1Button.selected = currentRating >= 1
    rate2Button.selected = currentRating >= 2
    rate3Button.selected = currentRating >= 3
    rate4Button.selected = currentRating >= 4
    rate5Button.selected = currentRating == 5
  }
}
