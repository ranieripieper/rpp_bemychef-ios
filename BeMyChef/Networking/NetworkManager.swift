//
//  NetworkManager.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class NetworkManager: AFHTTPSessionManager {
  let domain = "co.bemychef"
  let perPage = 20
  
  class var sharedInstance: NetworkManager {
    struct Static {
      static var instance: NetworkManager?
      static var token: dispatch_once_t = 0
    }
    dispatch_once(&Static.token) {
      Static.instance = NetworkManager()
      Static.instance!.installHeaderToken()
      AFNetworkActivityIndicatorManager.sharedManager().enabled = true
    }
    return Static.instance!
  }
  
  init() {
    // Ranis machine
//    super.init(baseURL: NSURL(string: "http://192.168.0.12:3000/api/v1/"), sessionConfiguration: nil)
    // Development
    super.init(baseURL: NSURL(string: "https://bemychefstg.herokuapp.com/api/v1/"), sessionConfiguration: nil)
    // Collision
//    super.init(baseURL: NSURL(string: "https://bmcpresentation.herokuapp.com/api/v1/"), sessionConfiguration: nil)
    // Production
//    super.init(baseURL: NSURL(string: "https://bemychef.co/api/v1/"), sessionConfiguration: nil)
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  func installHeaderToken() {
//    let user = SSKeychain.passwordForService(domain, account: "PersistedUser")
    let authToken = SSKeychain.passwordForService(domain, account: "AuthToken")
    if /*user != nil && */authToken != nil {
      requestSerializer.setValue(authToken!, forHTTPHeaderField: "User-Token")
    }
  }
  
  func uninstallHeaderToken() {
    requestSerializer.setValue(nil, forHTTPHeaderField: "User-Token")
  }
}

typealias APIResponse = [String: AnyObject]

// MARK: API
// MARK: Authentication
extension NetworkManager {
  override func POST(URLString: String!, parameters: AnyObject!, constructingBodyWithBlock block: ((AFMultipartFormData!) -> Void)!, success: ((NSURLSessionDataTask!, AnyObject!) -> Void)!, failure: ((NSURLSessionDataTask!, NSError!) -> Void)!) -> NSURLSessionDataTask! {
    SVProgressHUD.showWithMaskType(.Gradient)
    return super.POST(URLString, parameters: parameters, constructingBodyWithBlock: block, success: success, failure: failure)
  }
  
  override func POST(URLString: String!, parameters: AnyObject!, success: ((NSURLSessionDataTask!, AnyObject!) -> Void)!, failure: ((NSURLSessionDataTask!, NSError!) -> Void)!) -> NSURLSessionDataTask! {
    SVProgressHUD.showWithMaskType(.Gradient)
    return super.POST(URLString, parameters: parameters, success: success, failure: failure)
  }
  
//  api :POST, "/api/v1/users.json", "Cria um novo usuário [User/Chef]"
//  description "Cria um novo usuário"
//  param_group :user
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  curl -F "user[photo_attributes][file_content_type]=image/jpg" -F "user[photo_attributes][file]=@profile.jpg" -F "user[device_id]=ios_1234" -F "user[name]=nome usuário"  -F "user[email]=user38@example.com"  -F "user[password]=secret123"  -F "user[password_confirmation]=secret123" -F "user[locale]=en" #{MainYetting.HOST}/api/v1/users.json
  func registerUser(email: String, password: String, name: String, avatar: UIImage?, completion: Result<APIResponse> -> ()) {
    uninstallHeaderToken()
    var user: [String: AnyObject] = ["email": email, "password": password, "password_confirmation": password, "name": name, "locale": NSLocale.preferredLanguages().first!]
    if let deviceToken = Defaults[PushNotificationsManager.DeviceToken].string {
      user["user_push_tokens_attributes"] = ["token": deviceToken, "plataform": "ios"]
    }
    if avatar != nil {
      user["photo_attributes"] = ["file_content_type": "image/png"]
    }
    var params = ["user": user]
    POST("users.json", parameters: params, constructingBodyWithBlock: { multipartFormData in
      let newSize = CGSize(width: 320, height: 320)
      if avatar != nil {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        avatar!.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        let newAvatar = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let newAvatarData = UIImagePNGRepresentation(newAvatar)
        let fileName = "avatar_\(email)".stringByReplacingOccurrencesOfString("@", withString: "_").stringByReplacingOccurrencesOfString(".", withString: "_")
        multipartFormData.appendPartWithFileData(newAvatarData, name: "user[photo_attributes][file]", fileName: fileName, mimeType: "image/png")
      }
      }, success: { dataTask, response in
        println("req: \(dataTask.currentRequest.HTTPBody)")
        completion(self.handleSuccess(response) { success in
          println("response: \(response)")
          if success, let responseDict = response as? [String: AnyObject], let user = responseDict["user"] as? [String: AnyObject], let authToken = user["auth_token"] as? String, let userId = user["id"] as? Int {
            SSKeychain.setPassword(authToken, forService: self.domain, account: "AuthToken")
            SSKeychain.setPassword(String(userId), forService: self.domain, account: "UserId")
            self.requestSerializer.setValue(authToken, forHTTPHeaderField: "User-Token")
          }
          })
      }) { dataTask, error in
        println(error)
        completion(self.handleError(error))
    }
  }
  
//curl
//  -F 'user[chef_attributes][bank_account_info_attributes][name]=Nome conta banco'
//  -F 'user[chef_attributes][bank_account_info_attributes][bank]=341'
//  -F 'user[chef_attributes][bank_account_info_attributes][agency]=3412'
//  -F 'user[chef_attributes][bank_account_info_attributes][current_account]=09831-1'
//  -F 'user[chef_attributes][bank_account_info_attributes][document]=03988876590'
//  -F 'user[chef_attributes][about]=teste chef about'
//  -F 'user[chef_attributes][kitchens_attributes][][phone]=1100000000'
//  -F 'user[chef_attributes][kitchens_attributes][][food_truck]=false'
//  -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file_content_type]=image/jpg'
//  -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file]=@bmc_logo_share.png'
//  -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file_content_type]=image/jpg'
//  -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file]=@dds_logo.png'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][street]=Rua Tuim'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][number]=356'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][complement]=Sala 1'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][neighborhood]=Vila Uberabinha'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][zip_code]=04514-100'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][city]=São Paulo'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][state]=São Paulo'
//  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][country]=Brasil'
//  -F 'user[photo_attributes][file_content_type]=image/jpg'
//  -F 'user[photo_attributes][file]=@logo_BeMyChef.png'
//  -F 'user[device_id]=ios_1234'
//  -F 'user[name]=nome usuário'
//  -F 'user[email]=user40@example.com'
//  -F 'user[password]=secret123'
//  -F 'user[password_confirmation]=secret123'
//  -F 'user[locale]=en' http://bemychefstg.herokuapp.com/api/v1/users.json
  func registerChef(email: String, password: String, name: String, avatar: UIImage?, chefAbout: String, kitchenPictures: [UIImage], phone: String, cpf: String, zipcode: String, street: String, number: String, complement: String, bankName: String, bankNumber: String, bankAgency: String, bankAccount: String, completion: Result<APIResponse> -> ()) {
    uninstallHeaderToken()
    var bank = ["name": bankName, "bank": bankNumber, "agency": bankAgency, "current_account": bankAccount, "document": cpf]
    var address = ["zip_code": zipcode]//, "complement": complement, "number": number, "street": street]
//    var kitchen = ["": ["address_attributes": address, "phone": phone]]
    var kitchen = ["": ["address_attributes": address, "phone": phone, "photos_attributes": ["" : ["file_content_type": "image/png"]]]]
    var chef = ["about": chefAbout, "kitchens_attributes": kitchen, "bank_account_info_attributes": bank]
    var user = ["email": email, "password": password, "password_confirmation": password, "name": name, "locale": NSLocale.preferredLanguages().first!, "chef_attributes": chef]
    if let deviceToken = Defaults[PushNotificationsManager.DeviceToken].string {
      user["user_push_tokens_attributes"] = ["token": deviceToken, "plataform": "ios"]
    }
    if avatar != nil {
      user["photo_attributes"] = ["file_content_type": "image/png"]
    }
    POST("users.json", parameters: ["user": user], constructingBodyWithBlock: { multipartFormData in
      let newSize = CGSize(width: 320, height: 320)
      if avatar != nil {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        avatar!.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        let newAvatar = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let newAvatarData = UIImagePNGRepresentation(newAvatar)
        let fileName = "avatar_\(email)".stringByReplacingOccurrencesOfString("@", withString: "_").stringByReplacingOccurrencesOfString(".", withString: "_")
        multipartFormData.appendPartWithFileData(newAvatarData, name: "user[photo_attributes][file]", fileName: fileName, mimeType: "image/png")
      }
      for kitchenPicture in kitchenPictures {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        kitchenPicture.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        let newPicture = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let newPictureData = UIImagePNGRepresentation(newPicture)
        multipartFormData.appendPartWithFileData(newPictureData, name: "user[chef_attributes][kitchens_attributes][][photos_attributes][][file]", fileName: "kitchen_picture\(find(kitchenPictures, kitchenPicture)!)", mimeType: "image/png")
      }
      }, success: { dataTask, response in
        completion(self.handleSuccess(response) { success in
          println("response: \(response)")
          if success, let responseDict = response as? [String: AnyObject], let user = responseDict["user"] as? [String: AnyObject], let authToken = user["auth_token"] as? String, let userId = user["id"] as? Int {
            SSKeychain.setPassword(authToken, forService: self.domain, account: "AuthToken")
            self.requestSerializer.setValue(authToken, forHTTPHeaderField: "User-Token")
          }
          })
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func updatePaymentInfo(cpf: String, bankName: String, bankNumber: String, bankAgency: String, bankAccount: String, completion: Result<APIResponse> -> ()) {
    PUT("users.json", parameters: ["user": ["chef_attributes": ["bank_account_info_attributes": ["document": cpf, "name": bankName, "banck": bankNumber, "agency": bankAgency, "current_account": bankAccount]]]], success: { dataTask, data in
      completion(self.handleSuccess(data, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :POST, "/api/v1/users/reconfirm.json", "Reenvia email para o usuário com as instruções para ativação da conta [User/Chef]"
//  param :email, String, :required => true, :desc => "Email do usuário"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example 'curl -F "email=ranieripieper@gmail.com" http://bmc.dev.com:3000/api/v1/users/reconfirm.json'
  func sendConfirmationToEmail(email: String, completion: Result<APIResponse> -> ()) {
    uninstallHeaderToken()
    POST("users/reconfirm.json", parameters: ["email": email], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
//  curl -H 'User-Token: 12345' -F 'user_push_token[token]=xxx' -F 'user_push_token[plataform]=ios' http://bemychefstg.herokuapp.com/api/v1/users/update_push_token.json
  func updatePushToken() {
    if let deviceToken = Defaults[PushNotificationsManager.DeviceToken].string {
      POST("users/update_push_token.json", parameters: ["user_push_token": ["token": deviceToken, "plataform": "ios"]], success: { dataTask, response in
        println(response)
        }) { dataTask, error in
          println(error)
      }
    }
    
  }
  
//  api :PUT, "/api/v1/users.json", "Altera um usuário [User/Chef]"
//  description "Altera um  usuário"
//  example 'curl -F "user[photo_attributes][file_content_type]=image/jpg" -F "user[photo_attributes][file]=@profile.jpg" -F "user[chef]=true" -F "user[chef_about]=teste chef about"  -F "user[device_id]=ios_1234" -F "user[name]=nome usuário"  -F "user[email]=user22@example.com"  -F "user[password]=secret123"  -F "user[password_confirmation]=secret123"  -F "user[address_attributes][street]=rua tuim" -F "user[address_attributes][number]=356" -F "user[address_attributes][neighborhood]=Vila Uberabimha" -F "user[address_attributes][zip_code]=04514-100" -F "user[address_attributes][city]=São Paulo" -F "user[address_attributes][state]=São Paulo" -F "user[address_attributes][country]=Brasil" -F "user[locale]=en" -F "user[document]=123"  -X PUT http://bmc.dev.com:3000/api/v1/users.json'
//  param_group :user
//  param "User-Token", String, :required => true, :desc => "Token do usuário. Deve ser passado no Header"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  func update(email: String?, password: String?, name: String?, zipcode: String?, avatar: UIImage?, chef: Bool?, chefAbout: String?, address: Address?, completion: Result<APIResponse> -> ()) {
    var params = [String: AnyObject]()
    if email != nil {
      params["email"] = email!
    }
    if password != nil {
      params["password"] = password!
      params["password_confirmation"] = password!
    }
    if name != nil {
      params["name"] = name!
    }
    if chefAbout != nil {
      params["chef_about"] = chefAbout!
    }
    if chef != nil {
      params["chef"] = chef!
    }
    if address != nil && address!.APIDict().count > 0 {
      params["address_attributes"] = address!.APIDict()
    }
    if let deviceToken = Defaults[PushNotificationsManager.DeviceToken].string {
      params["device_id"] = deviceToken
    }
    params["locale"] = NSLocale.preferredLanguages().first!
    let request = requestSerializer.multipartFormRequestWithMethod("PUT", URLString: "users.json", parameters: ["user": params], constructingBodyWithBlock: { formData in
      let newSize = CGSize(width: 320, height: 320)
      if let avatar = avatar {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        avatar.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        let newAvatar = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let newAvatarData = UIImagePNGRepresentation(newAvatar)
        formData.appendPartWithFileData(newAvatarData, name: "user[photo_attributes][file]", fileName: "avatar.png", mimeType: "image/png")
      }
    }, error: nil)
    let dataTask = self.session.dataTaskWithRequest(request) { data, response, error in
      if error != nil {
        completion(self.handleError(error))
      } else {
        completion(self.handleSuccess(data, nil))
      }
    }
    dataTask.resume()
  }
  
//  api :POST, "/api/v1/users/login.json", "Realiza o Login [User/Chef]"
//  param :email, String, :required => true, :desc => "Email"
//  param :password, String, :required => true, :desc => "Senha"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example 'curl -F "user[email]=ranieripieper@gmail.com" -F "user[password]=secret123" http://bmc.dev.com:3000/api/v1/users/login.json '
  func login(email: String, password: String, completion: Result<APIResponse> -> ()) {
    uninstallHeaderToken()
    let params = ["email": email, "password": password]
    POST("users/login.json", parameters: ["user": params], success: { dataTask, response in
      completion(self.handleSuccess(response) { success in
        if success {
          if let responseDict = response as? [String: AnyObject], let user = responseDict["user"] as? [String: AnyObject], let authToken = user["auth_token"] as? String, let userId = user["id"] as? Int {
            SSKeychain.setPassword(authToken, forService: self.domain, account: "AuthToken")
            self.requestSerializer.setValue(authToken, forHTTPHeaderField: "User-Token")
          }
        } else {
          SSKeychain.deletePasswordForService(self.domain, account: "AuthToken")
          SSKeychain.deletePasswordForService(self.domain, account: "PersistedUser")
        }
        })
      }) { dataTask, error in
        SSKeychain.deletePasswordForService(self.domain, account: "AuthToken")
        SSKeychain.deletePasswordForService(self.domain, account: "PersistedUser")
        completion(self.handleError(error))
    }
  }
  
//  api :POST, "/api/v1/users/recoverable.json", "Envia o email com as instruções para resetar a senha [User/Chef]"
//  param :email, String, :required => true, :desc => "Email do usuário"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example 'curl -F "email=ranieripieper@gmail.com" http://bmc.dev.com:3000/api/v1/users/recoverable.json'
  func forgotPassword(email: String, completion: Result<APIResponse> -> ()) {
    POST("users/recoverable.json", parameters: ["email": email], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        let data = error.userInfo![AFNetworkingOperationFailingURLResponseDataErrorKey] as! NSData
        if let dict: AnyObject = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: nil) {
          println(dict)
        }
        completion(self.handleError(error))
    }
  }
}

// MARK: USER
extension NetworkManager {
//  api :GET, "/api/v1/user/addresses.json", "Retorna os endereços em que o Usuário já recebeu entregas [User]"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/user/addresses.json"
  func deliveredAddresses(completion: Result<APIResponse> -> ()) {
    GET("user/addresses.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: User Food
extension NetworkManager {
//  api :GET, "/api/v1/dishes/find_near.json", "Retorna os pratos que estão disponíveis no momento na região [User]"
//  param :page, :number, :required => true, :desc => "Número da página - Deve ser maior ou igual a 1."
//  param :lat, Float, :required => true, :desc => "Latitude"
//  param :lng, Float, :required => true, :desc => "Longitude"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/dishes/find_near.json?page=1&lat=-23.6121&lng=-46.6627"
  func findNearFood(page: Int, latitude: Float, longitude: Float, completion: Result<APIResponse> -> ()) {
    GET("dishes/find_near.json", parameters: ["page": page, "lat": latitude, "lng": longitude], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/dishes/:dish_id.json", "Retorna o prato pelo ID [User/Chef]"
//  param :dish_id, Integer, :required => true, :desc => "Id do prato"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/dishes/1.json"
  func food(foodId: Int, completion: Result<APIResponse> -> ()) {
    GET("dishes/\(foodId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: User Ingredients
extension NetworkManager {
//  api :GET, "/api/v1/ingredients/get_all.json", "Retorna todos os ingredientes  [User/Chef]"
//  param :page, :number, :required => false, :desc => "Número da página - Deve ser maior ou igual a 1. <br>Se o parâmetro não for passado, irá retornar todos os ingredientes"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/ingredients/get_all.json"
//  example "http://bmc.dev.com:3000/api/v1/ingredients/get_all.json?page=1"
  func allIngredients(completion: Result<APIResponse> -> ()) {
    GET("ingredients/get_all.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func allIngredients(page: Int, completion: Result<APIResponse> -> ()) {
    GET("ingredients/get_all.json", parameters: ["page": page], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: User Chef
extension NetworkManager {
//  api :GET, "/api/v1/chef/:chef_id.json", "Retorna as informações do chef pelo ID do chef [User]"
//  param :chef_id, Integer, :required => true, :desc => "Id do Chef"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/chef/1.json"
  func chef(chefId: Int, completion: Result<APIResponse> -> ()) {
    GET("chef/\(chefId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/user/chef/:user_id.json", "Retorna as informações do chef pelo ID do usuário (chef) [User]"
//  param :chef_id, Integer, :required => true, :desc => "Id do usuário Chef"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/user/chef/1.json"
  func chefWithUserId(userId: Int, completion: Result<APIResponse> -> ()) {
    GET("user/chef/\(userId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: User Kitchen
extension NetworkManager {
//  api :GET, "/api/v1/kitchens/:kitchen_id.json", "Retorna as informações da cozinha pelo ID [User/Chef]"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
//  example "http://bmc.dev.com:3000/api/v1/kitchens/1.json"
  func kitchen(kitchenId: Int, completion: Result<APIResponse> -> ()) {
    GET("kitchens/\(kitchenId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/user/chef/:user_chef_id/kitchens.json", "Retorna as cozinhas pelo id do Usuário (Chef) [User]"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  param :only_avaiable_dish, [true, false], :required => false, :desc => "Retorna só os pratos disponíveis para venda no momento. Só existem pratos disponíveis para a venda, se a cozinha está aberta (close = false) <br> Valor Default: false"
//  example "http://bmc.dev.com:3000/api/v1/user/chef/3/kitchens.json"
//  example "http://bmc.dev.com:3000/api/v1/user/chef/3/kitchens.json?only_avaiable_dish=true"
  func kitchensForChef(chefId: Int, completion: Result<APIResponse> -> ()) {
    GET("user/\(chefId)/kitchens.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  func availableKitchensForChef(chefId: Int, completion: Result<APIResponse> -> ()) {
    GET("user/\(chefId)/kitchens.json", parameters: ["only_available_dish": true], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
////  api :GET, "/api/v1/chef/:chef_id/kitchens.json", "Retorna as cozinhas pelo id do Chef [User]"
////  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
////  param :only_avaiable_dish, [true, false], :required => false, :desc => "Retorna só os pratos disponíveis para venda no momento. Só existem pratos disponíveis para a venda, se a cozinha está aberta (close = false) <br> Valor Default: false"
////  example "http://bmc.dev.com:3000/api/v1/chef/3/kitchens.json"
////  example "http://bmc.dev.com:3000/api/v1/chef/3/kitchens.json?only_avaiable_dish=true"
//  func kitchensForChef(chefId: Int, completion: Result<APIResponse> -> ()) {
//    GET("user/\(chefId)/kitchens.json", parameters: nil, success: { dataTask, response in
//      completion(self.handleSuccess(response, nil))
//      }) { dataTask, error in
//        completion(self.handleError(error))
//    }
//  }
//  
//  func availableKitchensForChef(chefId: Int, completion: Result<APIResponse> -> ()) {
//    GET("user/\(chefId)/kitchens.json", parameters: ["only_available_dish": true], success: { dataTask, response in
//      completion(self.handleSuccess(response, nil))
//      }) { dataTask, error in
//        completion(self.handleError(error))
//    }
//  }
}

// MARK: User Review
extension NetworkManager {
//  api :POST, "/api/v1/dishes/:dish_id/comments/create.json", "Insere um novo comentário [User]"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param_group :comment
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example 'curl -F "locale=pt-BR" -F "comment[photo_attributes][file_content_type]=image/jpg" -F "comment[photo_attributes][file]=@profile.jpg"  -F "comment[comment]=comentário 123" -F "comment[rating]=2"  http://0.0.0.0:3000/api/v1/dishes/1/comments/create.json'
  func commentAtFood(foodId: Int, photo: UIImage?, text: String, rating: Float, completion: Result<APIResponse> -> ()) {
    let params = ["comment": text, "rating": rating]
    POST("dishes/\(foodId)/comments/create.json", parameters: ["comment": params], constructingBodyWithBlock: { formData in
      if photo != nil {
        if let userId = SSKeychain.passwordForService(self.domain, account: "UserId") {
          formData.appendPartWithFileData(UIImagePNGRepresentation(photo!), name: "comment[photo_attributes][file]", fileName: "comment\(foodId)\(userId)", mimeType: "image/png")
        }
      }
    }, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :POST, "/api/v1/dishes/:dish_id/comments/:comment_id/update.json", "Altera um comentário [User]"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param_group :comment
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example 'curl -F "comment[photo_attributes][file_content_type]=image/jpg" -F "comment[photo_attributes][file]=@profile.jpg"  -F "comment[comment]=comentário 123" -F "comment[rating]=2"  http://0.0.0.0:3000/api/v1/dishes/1/comments/1/update.json'
  func updateComment(commentId: Int, foodId: Int, photo: UIImage?, text: String, rating: Float, completion: Result<APIResponse> -> ()) {
    let params = ["comment": text, "rating": rating]
    POST("dishes/\(foodId)/comments/create.json", parameters: ["comment": params], constructingBodyWithBlock: { formData in
      if photo != nil {
        if let userId = SSKeychain.passwordForService(self.domain, account: "UserId") {
          formData.appendPartWithFileData(UIImagePNGRepresentation(photo!), name: "comment[photo_attributes][file]", fileName: "comment\(foodId)\(userId)", mimeType: "image/png")
        }
      }
      }, success: { dataTask, response in
        completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/dishes/:dish_id/comments.json", "Retorna os comentários do prato [User/Chef]"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  param :dish_id, Integer, :required => true, :desc => "Id do prato"
//  param :page, Integer, :required => true, :desc => "Pagina"
//  param :nr_reg, Integer, :required => false, :desc => "Número de registros por página - Default: #{MainYetting.DEFAULT_REG_PER_PAGE}"
//  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  example "http://bmc.dev.com:3000/api/v1/dishes/1/comments.json?page=1&locale=pt-BR&nr_reg=3"
  func comments(foodId: Int, page: Int, completion: Result<APIResponse> -> ()) {
    let params = ["page" : page, "nr_reg": perPage]
    GET("dishes/\(foodId)/comments.json", parameters: params, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/dishes/:dish_id/my_comment.json", "Retorna o comentário do prato [User]"
//  param :dish_id, Integer, :required => true, :desc => "Id do prato"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/dishes/1/my_comment.json?locale=pt-BR"
  func myCommentAtFood(foodId: Int, completion: Result<APIResponse> -> ()) {
    GET("dishes/\(foodId)/my_comment.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: User Notifications
extension NetworkManager {
//  api :GET, "/api/v1/user/notifications.json", "Retorna as notificações que o usuário recebeu [User]"
//  param :page, :number, :required => true, :desc => "Número da página - Deve ser maior ou igual a 1."
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/notifications/user.json"
  func userNotifications(page: Int, completion: Result<APIResponse> -> ()) {
    GET("notifications/user.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/user/notifications/:notification_id.json", "Retorna a notificação pelo ID [User]"
//  param :notification_id, :number, :required => true, :desc => "Id da notificação"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "http://bmc.dev.com:3000/api/v1/notifications/user/1.json"
  func userNotification(notificationId: Int, completion: Result<APIResponse> -> ()) {
    GET("user/notifications/\(notificationId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: User Orders
extension NetworkManager {
//  curl -H 'User-Token: 12345' -F 'reserve[quantity]=2' -F 'reserve[ordered_value]=20' -F 'reserve[delivery_price]=5' -F 'reserve[delivery_pick_up]=1'  -X POST http://bemychefstg.herokuapp.com/api/v1/ordereds/dishes/reserve/2.json
//  reserve[quantity]
//  reserve[delivery_pick_up]
//  reserve[ordered_value]
//  reserve[delivery_price]
  func reserve(foodId: Int, amount: Int, deliveryType: Int, value: Float, deliveryPrice: Float, completion: Result<APIResponse> -> ()) {
    var params: [String: AnyObject] = ["quantity": amount, "delivery_pick_up": deliveryType, "ordered_value": Float(amount) * value, "delivery_price": deliveryPrice]
    POST("ordereds/dishes/reserve/\(foodId).json", parameters: ["reserve": params], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//api :POST, "/api/v1/ordereds/dishes/:dish_id.json", "Realiza o pedido do prato [User]"
//param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//param :dish_id, Integer, :required => true, :desc => "Id do prato"
//param :qt, Integer, :required => true, :desc => "Quantidade de pratos"
//param :delivery_pick_up, [1, 2, 3], :required => true, :desc => "se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery"
//param :ps, String, :required => false, :desc => "Observação do pedido. Ex.: Carne bem passada"
//param :card_holder_name, String, :required => true, :desc => "Nome do titular do cartão"
//param :card_number, String, :required => true, :desc => "Número do cartão"
//param :card_cvv, Integer, :required => true, :desc => "Código de segurança do cartão"
//param :card_exp_year, Integer, :required => true, :desc => "Ano de expiração do cartão"
//param :card_exp_month, Integer, :required => true, :desc => "Mês de expiração do cartão"
//param :card_flag, String, :required => true, :desc => "Bandeira do cartão"
//curl -H 'User-Token: 12345' -X POST http://bemychefstg.herokuapp.com/api/v1/ordereds/reserve/1/confirm.json?card_holder_name=Ranieri&card_number=4012001037141112&card_cvv=123&card_exp_year=2018&card_exp_month=05&card_flag=visa
  func confirmReserve(reserveId: Int, cardHash: String, completion: Result<APIResponse> -> ()) {
    let persistedAddress = Address.persistedAddress()!
    let address = ["street": persistedAddress.street, "number": persistedAddress.number, "zip_code": persistedAddress.zipcode]
    let params = ["address": address, "cardHash": cardHash]
    super.POST("ordereds/reserve/\(reserveId)/confirm.json", parameters: params, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  http://bemychefstg.herokuapp.com/api/v1/ordereds.json
  func orders(page: Int, completion: Result<APIResponse> -> ()) {
    GET("user/ordereds.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
//  api :GET, "/api/v1/ordereds/:ordered_id.json", "Retorna as informações do pedido [User/Chef]"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
//  example "http://bmc.dev.com:3000/api/v1/ordereds/66.json"
  func orderInfo(orderId: Int, completion: Result<APIResponse> -> ()) {
    GET("ordereds/\(orderId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: CHEF
extension NetworkManager {
  //  api :GET, "/api/v1/chef/payment/resume.json", "Retorna quanto o chef já recebeu e quanto o chef tem a receber [Chef]"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/chef/payment/resume.json"
  func chefPaymentSummary(completion: Result<APIResponse> -> ()) {
    GET("chef/payment/resume.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: Chef Food
extension NetworkManager {
//  api :POST, "/api/v1/dishes/insert.json", "Insere um novo prato [Chef]"
//  param_group :dish, :desc => "Prato"
//  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
//  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
//  example "curl 
//  -F 'dish[dish_ingredients_attributes][][ingredient_id]=1'
//  -F 'dish[dish_ingredients_attributes][][ingredient_id]=2'
//  -F 'dish[name]=teste'
//  -F 'dish[description]=desc%20prato'
//  -F 'dish[quantity]=10'
//  -F 'dish[price]=10.90'
//  -F 'dish[organic]=false'
//  -F 'dish[dish_type]=1'
//  -F 'dish[time_to_prepare]=20'
//  -F 'dish[kitchen_id]='
//  -F 'User-Token=12345'
//  -F 'dish[photos_attributes][][file_content_type]=image/jpg'
//  -F 'dish[photos_attributes][][file]=@bmc_logo_share.png'
//  -F 'dish[photos_attributes][][file_content_type]=image/jpg'
//  -F 'dish[photos_attributes][][file]=@dds_logo.png' #{MainYetting.HOST}/api/v1/dishes/insert.json"
  func insertFood(name: String, description: String, amount: Int, price: Float, type: Int, prepTime: Float, kitchenId: Int, ingredientsId: [Int], pictures: [UIImage], completion: Result<APIResponse> -> ()) {
    var food = ["name": name, "description": description, "quantity": amount, "price": price, "dish_type": type, "time_to_prepare": prepTime, "kitchen_id": kitchenId]
    var params: [String: AnyObject] = ["dish": food]
    for ingredientId in ingredientsId {
      params["dish[dish_ingredients_attributes][][ingredient_id]"] = ingredientId
    }
    if pictures.count > 0 {
      params["dish[photos_attributes][][file_content_type]"] = "image/png"
    }
    POST("dishes/insert.json", parameters: params, constructingBodyWithBlock: { multipartFormData in
      let newSize = CGSize(width: 320, height: 320)
      for picture in pictures {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        picture.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        let newPicture = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let newPictureData = UIImagePNGRepresentation(newPicture)
        let fileName = "picture_\(find(pictures, picture)!)"
        multipartFormData.appendPartWithFileData(newPictureData, name: "dish[photos_attributes][][file]", fileName: fileName, mimeType: "image/png")
      }
    }, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
  
  //  api :POST, "/api/v1/dishes/:dish_id/update.json", "Faz update do prato. Pode alterar quantidade, tempo de preparo e preço [Chef]"
  //  param :dish_id, Integer, :required => false, :desc => "Id do prato"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param_group :dish_update
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/dishes/1/update.json?User-Token=123&dish[quantity]=101&dish[time_to_prepare]=45&dish[price]=30.00"
  func updateFood(foodId: Int, quantity: Int, timeToPrepare: Int, price: String, completion: Result<APIResponse> -> ()) {
    let params = ["quantity": quantity, "time_to_prepare": timeToPrepare, "price": price]
    POST("dished/\(foodId)/update.json", parameters: ["dish": params], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
//  curl -H 'User-Token: 12345' -F 'dish[dish_ingredients_attributes][][ingredient_id]=1' -F 'User-Token=123456' -X DELETE http://bemychefstg.herokuapp.com/api/v1/dishes/1/delete.json
  func deleteFood(foodId: Int, completion: Result<APIResponse> -> ()) {
    DELETE("dishes/\(foodId)/delete.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
    }) { dataTask, error in
      completion(self.handleError(error))
    }
  }
}

// MARK: Chef Ingredients
extension NetworkManager {
  //  api :GET, "/api/v1/ingredients/get_dish_type.json", "Retorna o tipo de prato dependendo dos ingredients [Chef]"
  //  param :ingredients, Array, :required => true, :desc => "Lista dos ingredients"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/ingredients/get_dish_type.json?ingredients[]=1&ingredients[]=2"
  func foodTypeWithIngredients(ingredientsIds: [Int], completion: Result<APIResponse> -> ()) {
    let params = ingredientsIds.reduce("") { string, id -> String in
      string + "ingredients[]=\(id)"
    }
    GET("ingredients/get_dish_type.json", parameters: params, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: Chef Kitchen
extension NetworkManager {
  //  api :GET, "/api/v1/chef/kitchen.json", "Retorna as cozinhas do chef logado [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/chef/kitchens.json"
  func myKitchen(completion: Result<APIResponse> -> ()) {
    GET("chef/kitchen.json", parameters: nil, success: { dataTask, response in
      println(response)
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :GET, "/api/v1/chef/kitchens.json", "Retorna as cozinhas do chef logado [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/chef/kitchens.json"
  func myKitchens(completion: Result<APIResponse> -> ()) {
    GET("chef/kitchens.json", parameters: nil, success: { dataTask, response in
      println(response)
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :POST, "/api/v1/chef/kitchens/:kitchen_id/close.json", "Fecha a cozinha [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
  //  example "http://bmc.dev.com:3000/api/v1/chef/kitchens/2/close.json"
  func closeKitchen(kitchenId: Int, completion: Result<APIResponse> -> ()) {
    POST("chef/kitchens/\(kitchenId)/close.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :POST, "/api/v1/chef/kitchens/:kitchen_id/open.json", "Abre a cozinha [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
  //  example "http://bmc.dev.com:3000/api/v1/chef/kitchens/2/open.json"
//  kitchen_idrequerido
//  kitchen
//  kitchen[delivery]opcional
//  kitchen[pick_up]opcional
//  kitchen[dishes_attributes]requerido
//  kitchen[dishes_attributes][id]opcional
//  kitchen[dishes_attributes][price]opcional
//  kitchen[dishes_attributes][quantity]opcional
//  kitchen[dishes_attributes][time_to_prepare] opcional
  func openKitchen(kitchenId: Int, foods: [[String: AnyObject]], delivery: Bool, takeaway: Bool, completion: Result<APIResponse> -> ()) {
    let kitchen = ["delivery": delivery, "pick_up": takeaway, "dishes_attributes": foods]
    POST("chef/kitchens/\(kitchenId)/open.json", parameters: ["kitchen": kitchen], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: Chef Notifications
extension NetworkManager {
  //  api :GET, "/api/v1/chef/notifications.json", "Retorna as notificações que o chef recebeu [Chef]"
  //  param :page, :number, :required => true, :desc => "Número da página - Deve ser maior ou igual a 1."
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/notifications/chef.json"
  func chefNotifications(page: Int, completion: Result<APIResponse> -> ()) {
    GET("notifications/chef.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :GET, "/api/v1/chef/notifications/:notification_id.json", "Retorna a notificação pelo ID [Chef]"
  //  param :notification_id, :number, :required => true, :desc => "Id da notificação"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/notifications/chef/1.json"
  func chefNotification(notificationId: Int, completion: Result<APIResponse> -> ()) {
    GET("chef/notifications/\(notificationId).json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: Chef Orders
extension NetworkManager {
  //  api :POST, "/api/v1/ordereds/:ordered_id/accept.json", "Confirma ou não o pedido do prato [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  //  param :accepted, ["true", "false"], :required => true, :desc => "true -> Chefe aceitou o pedido - False -> Chefe não aceitou o pedido"
  //  example "http://bmc.dev.com:3000/api/v1/ordereds/1/accept.json?accepted=true"
  //  example "http://bmc.dev.com:3000/api/v1/ordereds/1/accept.json?accepted=false"
  func acceptOrder(orderId: Int, accept: Bool, completion: Result<APIResponse> -> ()) {
    POST("ordereds/\(orderId)/accept.json?accepted=\(accept)", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :POST, "/api/v1/ordereds/:ordered_id/cancel_by_chef.json", "Cancela o pedido [Chef]"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  //  example "http://bmc.dev.com:3000/api/v1/ordereds/1/cancel_by_chef.json"
  func cancelOrder(orderId: Int, completion: Result<APIResponse> -> ()) {
    POST("ordereds/\(orderId)/cancel_by_chef.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :POST, "/api/v1/ordereds/:ordered_id/ordered_ready.json", "Prato está pronto para entrega/retirada [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  //  example "http://bmc.dev.com:3000/api/v1/ordereds/1/ordered_ready.json"
  func foodReady(orderId: Int, completion: Result<APIResponse> -> ()) {
    POST("ordereds/\(orderId)/ordered_ready.json", parameters: nil, success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
  
  //  api :GET, "/api/v1/ordereds.json", "Retorna todos os pedidos paginados [Chef]"
  //  param "User-Token", String, :required => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  //  param :page, :number, :required => false, :desc => "Número da página - Deve ser maior ou igual a 1."
  //  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  //  example "http://bmc.dev.com:3000/api/v1/ordereds.json"
  func myOrders(page: Int, completion: Result<APIResponse> -> ()) {
    GET("ordereds.json", parameters: ["page": page], success: { dataTask, response in
      completion(self.handleSuccess(response, nil))
      }) { dataTask, error in
        completion(self.handleError(error))
    }
  }
}

// MARK: General
extension NetworkManager {
  func handleSuccess(response: AnyObject, _ callback: ((Bool) -> ())?) -> Result<APIResponse> {
    SVProgressHUD.dismiss()
    if let responseData = response as? [String: AnyObject] {
      if let success = responseData["success"] as? Bool {
        if success {
          callback?(true)
          return Result(responseData)
        } else {
          if let description = responseData["info"] as? String {
            return Result(Error(code: 200, domain: self.domain, userInfo: ["description": description]))
          }
          return Result(Error(code: 500, domain: self.domain, userInfo: ["description": "unknown error"]))
        }
      }
    }
    callback?(false)
    return Result(Error(code: 500, domain: self.domain, userInfo: ["description": "unknown error"]))
  }
  
  func handleError(error: NSError) -> Result<APIResponse> {
    SVProgressHUD.dismiss()
    if let userInfo = error.userInfo, let errorData = userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData, let errorDict = NSJSONSerialization.JSONObjectWithData(errorData, options: .AllowFragments, error: nil) as? [String: AnyObject] {
      if let errorInfo = errorDict["info"] as? [String: AnyObject], let info: AnyObject = errorInfo[errorInfo.keys.first!] {
        if let info = info as? String {
          return Result(Error(code: -1, domain: domain, userInfo: ["description": info]))
        } else if let infos = info as? [String] {
          return Result(Error(code: -1, domain: domain, userInfo: ["description": infos.first!]))
        }
      } else if let errorInfo = errorDict["info"] as? String {
        return Result(Error(code: -1, domain: domain, userInfo: ["description": errorInfo]))
      }
      return Result(Error(code: -1, domain: domain, userInfo: ["description": errorDict["info"]]))
    }
    return Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription]))
  }
}
