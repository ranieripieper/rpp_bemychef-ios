//
//  GooglePlacesAPI.swift
//
//  Created by Gilson Gil on 3/5/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class GooglePlacesAPI: AFHTTPSessionManager {
  let types = "geocode"
  let location = "-15.733494,-47.939065"
  let radius = "50000"
  let language = "pt"
  let apiKey = "AIzaSyB-6sPlmCll_zOBy4b3UrbZ9kwZnQgJNfo"
  
  init() {
    super.init(baseURL: NSURL(string: "https://maps.googleapis.com/maps/api/geocode/"), sessionConfiguration: nil)
  }
//  https://maps.googleapis.com/maps/api/geocode/json?latlng=-23.600123,-46.668894&key=AIzaSyB-6sPlmCll_zOBy4b3UrbZ9kwZnQgJNfo
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
}

extension GooglePlacesAPI {
  final class func zipcode(latitude: Float, longitude: Float, completion: Result<[(Address, String)]> -> ()) {
    let googlePlacesAPI = GooglePlacesAPI()
    googlePlacesAPI.GET("json", parameters: ["latlng": NSString(format: "%.6f,%.6f", latitude, longitude), "key": googlePlacesAPI.apiKey], success: { dataTask, response in
      let addresses = GooglePlacesAPI.parse(response)
      if addresses.count > 0 {
        completion(Result(addresses))
      } else {
        completion(Result(Error(code: -1, domain: "co.bemychef", userInfo: ["description": "no results"])))
      }
    }) { dataTask, error in
      println(error)
      completion(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
    }
  }
  
  final private class func parse(response: AnyObject) -> [(Address, String)] {
    var addresses: [(Address, String)] = []
    if let responseDict = response as? NSDictionary, let results = responseDict["results"] as? [[String: AnyObject]] where results.count > 0 {
      for result in results {
        if let addressComponents = result["address_components"] as? [[String: AnyObject]] where addressComponents.count > 0 {
          let address = Address(geocode: addressComponents)
          if count(address.zipcode) > 0 {
            if let formattedAddress = result["formatted_address"] as? String {
              let tuple = (address, formattedAddress)
              addresses.append(tuple)
            }
          }
        }
      }
    }
    return addresses
  }
}
