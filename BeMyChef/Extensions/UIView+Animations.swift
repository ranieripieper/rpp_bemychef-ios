//
//  UIView+Animations.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

extension UIView {
  func animateLoginLogo(logoImageView: UIImageView, loginButtons: [UIView]) {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
      logoImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 50.0, 0.0)
    }) { finished in
      
    }
    UIView.animateWithDuration(0.3, delay: 0.25, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
      logoImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.frame.width, 0.0)
      }, completion: { finished in
        
    })
    UIView.animateWithDuration(0.3, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
      self.setButtonsFrames(loginButtons)
    }) { finished in
      
    }
  }
  
  func setButtonsFrames(buttons: [UIView]) {
    buttons.map() {
      $0.frame = CGRect(x: (self.frame.width - $0.bounds.width) / 2, y: $0.frame.origin.y, width: $0.bounds.width, height: $0.bounds.height)
    }
  }
  
  func animateSubviewsOut(subviews: [UIView]) {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: .CurveEaseOut, animations: {
      let transformed = subviews.map() {
        $0.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.frame.width, 0.0)
      }
      }) { finished in
        
    }
  }
  
  func animateSubviewsIn(subviews: [UIView]) {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: .CurveEaseOut, animations: {
      let transformed = subviews.map() {
        $0.transform = CGAffineTransformIdentity
      }
    }) { finished in
      
    }
  }
}
