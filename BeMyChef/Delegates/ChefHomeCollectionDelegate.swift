//
//  ChefHomeCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ChefHomeCollectionDelegate: NSObject {
  @IBOutlet weak var chefHomeViewController: ChefHomeViewController!
  @IBOutlet weak var tableView: UITableView!
  
  var datasource: [AnyObject] = []
  var headerSideSelected: Side = .Left
  
  enum Side {
    case Left, Right
  }
}

extension ChefHomeCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 {
      return 150
    } else {
      if chefHomeViewController.user.chef!.kitchens.first!.foods.count == 0 {
        return 0
      } else {
        return 50
      }
    }
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if section == 0 {
      return UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 110))
    } else {
      if chefHomeViewController.user.chef!.kitchens.first!.foods.count == 0 {
        return nil
      } else {
        let chefHomeSelectionHeader = tableView.dequeueReusableHeaderFooterViewWithIdentifier("ChefHomeSelectionHeader") as! ChefHomeSelectionHeader
        if chefHomeViewController.user.chef!.kitchens.first!.open {
          chefHomeSelectionHeader.configureWithButtonTypes(("Pedidos Abertos", {
            self.datasource = self.chefHomeViewController.user.chef!.openOrders
            self.chefHomeViewController.mode = .Orders
            self.headerSideSelected = .Left
            self.tableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
          }), right: ("Meus Pratos", {
            self.datasource = self.chefHomeViewController.user.chef!.kitchens.first!.foods
            self.chefHomeViewController.mode = .Foods
            self.headerSideSelected = .Right
            self.tableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
          }), leftSelected: self.headerSideSelected == .Left)
        } else {
          chefHomeSelectionHeader.configureWithButtonTypes(("Meus pratos", {
            self.datasource = self.chefHomeViewController.user.chef!.kitchens.first!.foods
            self.chefHomeViewController.mode = .Foods
            self.headerSideSelected = .Left
            self.tableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
          }), right: ("Pratos vendidos", {
            self.datasource = self.chefHomeViewController.user.chef!.orderHistory
            self.chefHomeViewController.mode = .History
            self.headerSideSelected = .Right
            self.tableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
          }), leftSelected: self.headerSideSelected == .Left)
        }
        return chefHomeSelectionHeader
      }
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if datasource.count == 0 {
      if indexPath.row == 0 {
        return UIScreen.mainScreen().bounds.height - tableView.frame.origin.y - 150 - 115
      } else {
        return 115
      }
    } else {
      if indexPath.row < datasource.count {
        return 80
      } else {
        return 115
      }
    }
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 0
    } else {
      return max(datasource.count + 1, 2)
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if datasource.count == 0 {
      if indexPath.row == 0 {
        let emptyCell = tableView.dequeueReusableCellWithIdentifier("ChefHomeEmptyCell", forIndexPath: indexPath) as! ChefHomeEmptyCell
        emptyCell.configureWithText(textForNoItems())
        return emptyCell
      } else {
        let chefHomeNewFoodCell = tableView.dequeueReusableCellWithIdentifier("ChefHomeNewFoodCell", forIndexPath: indexPath) as! ChefHomeNewFoodCell
        chefHomeNewFoodCell.configureWithType(.Long)
        chefHomeNewFoodCell.callback = newFoodTapped
        return chefHomeNewFoodCell
      }
    } else {
      if indexPath.row < datasource.count {
        if let food = datasource[indexPath.row] as? Food {
          let chefFoodCell = tableView.dequeueReusableCellWithIdentifier("ChefFoodCell", forIndexPath: indexPath) as! ChefFoodCell
          chefFoodCell.configureWithFood(food, showsEditingControl: true, showsDetails: true)
          chefFoodCell.deleteCallback = {
            if let indexPath = tableView.indexPathForCell($0), let food = self.datasource[indexPath.row] as? Food {
              food.delete() {
                switch $0 {
                case .Success:
                  var foods = (self.chefHomeViewController.user.chef!.kitchens.first!.foods as NSArray).mutableCopy() as! NSMutableArray
                  foods.removeObject(food)
                  self.chefHomeViewController.user.chef!.kitchens.first!.foods = foods.copy() as! [Food]
                  self.datasource.removeAtIndex(indexPath.row)
                  tableView.beginUpdates()
                  tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                  tableView.endUpdates()
                case .Failure:
                  println($0)
                }
              }
            }
          }
          return chefFoodCell
        } else if let order = datasource[indexPath.row] as? Order {
          let orderCell = tableView.dequeueReusableCellWithIdentifier("OrderCell", forIndexPath: indexPath) as! OrderCell
          orderCell.configureWithOrder(order)
          return orderCell
        } else {
          return UITableViewCell()
        }
      } else {
        let chefHomeNewFoodCell = tableView.dequeueReusableCellWithIdentifier("ChefHomeNewFoodCell", forIndexPath: indexPath) as! ChefHomeNewFoodCell
        chefHomeNewFoodCell.configureWithType(.Long)
        chefHomeNewFoodCell.callback = newFoodTapped
        return chefHomeNewFoodCell
      }
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if let order = datasource[indexPath.row] as? Order {
      chefHomeViewController.goToOrder(order)
    }
  }
  
  func newFoodTapped() {
    chefHomeViewController.addNewFood()
  }
  
  func textForNoItems() -> String {
    switch chefHomeViewController.mode {
    case .Foods:
      return "Você ainda não tem nenhum prato disponível!"
    case .Orders:
      return "Você ainda não teve nenhum pedido!"
    case .History:
      return "Você ainda não teve nenhum pedido!"
    case .None:
      return ""
    }
  }
  
  func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    if datasource.count > indexPath.row {
      return datasource[indexPath.row] is Order
    }
    return false
  }
}

extension ChefHomeCollectionDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let factor: CGFloat = 2 / 15
    chefHomeViewController.avatarImageViewTopSpaceConstraint.constant = max(164, 184 - scrollView.contentOffset.y * factor)
  }
}
