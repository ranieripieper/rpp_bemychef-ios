//
//  RateCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RateCollectionDelegate: NSObject {
  @IBOutlet weak var rateViewController: RateViewController!
}

extension RateCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    switch indexPath.row {
    case 0:
      return 210
    case 1:
      return 148
    case 2:
      return 100
    case 3:
      return 90
    case 4:
      return 91
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch indexPath.row {
    case 0:
      let statusCell = tableView.dequeueReusableCellWithIdentifier("OrderStatusCell", forIndexPath: indexPath) as! OrderStatusCell
      statusCell.configureWithOrder(rateViewController.order)
      return statusCell
    case 1:
      let infoCell = tableView.dequeueReusableCellWithIdentifier("OrderInfoCell", forIndexPath: indexPath) as! OrderInfoCell
      infoCell.configureWithOrder(rateViewController.order)
      return infoCell
    case 2:
      let rateCell = tableView.dequeueReusableCellWithIdentifier("RateCell", forIndexPath: indexPath) as! RateCell
      rateCell.callback = {
        self.rateViewController.currentRating = $0
      }
      return rateCell
    case 3:
      let rateCommentCell = tableView.dequeueReusableCellWithIdentifier("RateCommentCell", forIndexPath: indexPath) as! RateCommentCell
      return rateCommentCell
    case 4:
      let orderActionCell = tableView.dequeueReusableCellWithIdentifier("OrderActionCell", forIndexPath: indexPath) as! OrderActionCell
      orderActionCell.configureWithButtonType(("enviar avaliação", {
        self.rateViewController.sendTapped()
      }))
      return orderActionCell
    default:
      return UITableViewCell()
    }
  }
}
