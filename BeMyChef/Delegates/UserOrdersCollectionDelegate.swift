//
//  UserOrdersCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class UserOrdersCollectionDelegate: NSObject {
  var orders: [Order]?
  
  var selectionCallback: (Order -> ())?
}

extension UserOrdersCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let orders = orders {
      return orders.count
    }
    return 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("OrderCell", forIndexPath: indexPath) as! OrderCell
    cell.configureWithOrder(orders![indexPath.row])
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    selectionCallback?(orders![indexPath.row])
  }
}
