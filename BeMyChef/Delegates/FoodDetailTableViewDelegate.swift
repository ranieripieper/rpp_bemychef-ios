//
//  FoodDetailTableViewDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class FoodDetailTableViewDelegate: NSObject {
  @IBOutlet weak var foodDetailViewController: FoodDetailViewController!
  
  var food: Food?
  
  var infoCellOnceToken: dispatch_once_t = 0
  var sizingInfoCell: FoodDetailInfoCell?
  var ingredientsCellOnceToken: dispatch_once_t = 0
  var sizingIngredientsCell: FoodDetailIngredientsCell?
  lazy var mapViewDelegate: MapViewDelegate = {
    return MapViewDelegate()
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
}

extension FoodDetailTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    if food!.kitchen.verified {
//      return 6
//    } else {
      return 5
//    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    switch indexPath.row {
    case 0:
      dispatch_once(&infoCellOnceToken) {
        self.sizingInfoCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailInfoCell") as? FoodDetailInfoCell
      }
      sizingInfoCell!.configureWithFood(food!)
      let height = calculateHeightForConfiguredCell(sizingInfoCell!)
      return height
    case 1:
      return 290
    case 2:
      if food!.reviews?.count > 0 {
        return 120
      } else {
        return 0
      }
    case 3:
      dispatch_once(&ingredientsCellOnceToken) {
        self.sizingIngredientsCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailIngredientsCell") as? FoodDetailIngredientsCell
      }
      sizingIngredientsCell!.configureWithIngredients(food!.ingredients)
      let height = calculateHeightForConfiguredCell(sizingIngredientsCell!)
      return height
    case 4:
//      if food!.kitchen.verified {
//        return 185
//      } else {
        return 120
//      }
    default:
      return 120
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch indexPath.row {
    case 0:
      let infoCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailInfoCell", forIndexPath: indexPath) as! FoodDetailInfoCell
      infoCell.configureWithFood(food!)
      infoCell.setNeedsUpdateConstraints()
      infoCell.updateConstraintsIfNeeded()
      return infoCell
    case 1:
      let chefCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailChefCell", forIndexPath: indexPath) as! FoodDetailChefCell
      chefCell.configureWithFood(food!)
      chefCell.delegate = foodDetailViewController
      return chefCell
    case 2:
      let reviewCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailReviewCell", forIndexPath: indexPath) as! FoodDetailReviewCell
      reviewCell.configureWithFood(food!)
      return reviewCell
    case 3:
      let ingredientsCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailIngredientsCell", forIndexPath: indexPath) as! FoodDetailIngredientsCell
      ingredientsCell.configureWithIngredients(food!.ingredients)
      ingredientsCell.setNeedsUpdateConstraints()
      ingredientsCell.updateConstraintsIfNeeded()
      return ingredientsCell
    case 4:
//      if food!.kitchen.verified {
//        let verifiedCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailVerifiedCell", forIndexPath: indexPath) as! UITableViewCell
//        return verifiedCell
//      } else {
        let mapCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailMapCell", forIndexPath: indexPath) as! FoodDetailMapCell
        mapCell.configureWithFood(food!)
        mapCell.mapView.delegate = mapViewDelegate
        return mapCell
//      }
    default:
      let mapCell = tableView.dequeueReusableCellWithIdentifier("FoodDetailMapCell", forIndexPath: indexPath) as! FoodDetailMapCell
      mapCell.configureWithFood(food!)
      mapCell.mapView.delegate = mapViewDelegate
      return mapCell
    }
  }
  
  func calculateHeightForConfiguredCell(cell: UITableViewCell) -> CGFloat {
    cell.setNeedsUpdateConstraints()
    cell.updateConstraintsIfNeeded()
    cell.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.width, height: cell.bounds.height)
    cell.setNeedsLayout()
    cell.layoutIfNeeded()
    let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
    return size.height + 1.0
  }
}

extension FoodDetailTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if let infoCell = foodDetailViewController.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? FoodDetailInfoCell {
      let ratio: CGFloat = 0.5
      let yOffset = scrollView.contentOffset.y + 64
      infoCell.scrollView.contentOffset = CGPoint(x: 0, y: min(-yOffset * ratio, 0))
      infoCell.scrollViewTopSpaceConstraint.constant = min(yOffset, 0)
      infoCell.scrollViewHeightConstraint.constant = 150 - min(yOffset, 0)
      infoCell.foodImageViewHeightConstraint.constant = 170 - min(yOffset, 0)
    }
  }
}
