//
//  TextFieldDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/8/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol TextFieldViewController {
  var view: UIView! { get set }
  
  func lastTextFieldReturned(textField: UITextField)
}

class TextFieldDelegate: NSObject, UITextFieldDelegate {
  @IBOutlet weak var viewController: TextFieldViewController!
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if let nextTextField = viewController.view.viewWithTag(textField.tag + 1) as? UITextField {
      nextTextField.becomeFirstResponder()
    } else if let nextTextView = viewController.view.viewWithTag(textField.tag + 1) as? UITextView {
      nextTextView.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
      viewController.lastTextFieldReturned(textField)
    }
    return true
  }
}
