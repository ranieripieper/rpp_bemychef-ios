//
//  AutocompleteCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AutocompleteCollectionDelegate: NSObject {
  var originalDatasource: [String]! {
    didSet {
      datasource = originalDatasource
    }
  }
  var datasource: [String]!
  var callback: ((Int) -> ())?
}

extension AutocompleteCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let autocompleteCell = tableView.dequeueReusableCellWithIdentifier("AutocompleteCell", forIndexPath: indexPath) as! AutocompleteCell
    autocompleteCell.configureWithText(datasource[indexPath.row])
    return autocompleteCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    callback?(indexPath.row)
  }
}
