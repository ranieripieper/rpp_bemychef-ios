//
//  ZipcodeCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/20/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ZipcodeCollectionDelegate: NSObject {
  var datasource = [[(Address, String)]]()
  
  var callback: (Address -> ())?
}

extension ZipcodeCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource[section].count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("ZipcodeCell", forIndexPath: indexPath) as! ZipcodeCell
    cell.configureWithAddress(datasource[indexPath.section][indexPath.row].1, highlighted: indexPath.section == 0)
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    callback?(datasource[indexPath.section][indexPath.row].0)
  }
}
