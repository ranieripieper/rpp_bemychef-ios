//
//  HomeTableViewDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HomeTableViewDelegate: NSObject {
  @IBOutlet weak var homeViewController: HomeViewController!

  var foods: [Food] = []
}

extension HomeTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return foods.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let food = foods[indexPath.row]
    let foodCell = tableView.dequeueReusableCellWithIdentifier("FoodCell", forIndexPath: indexPath) as! FoodCell
    foodCell.configureWithFood(food)
    return foodCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let food = foods[indexPath.row]
    homeViewController.performSegueWithIdentifier("SegueFoodDetail", sender: food)
  }
}
