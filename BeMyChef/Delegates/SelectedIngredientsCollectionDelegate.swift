//
//  SelectedIngredientsCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SelectedIngredientsCollectionDelegate: NSObject {
  @IBOutlet weak var ingredientsViewController: IngredientsViewController!
}

extension SelectedIngredientsCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return ingredientsViewController.ingredients.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("SelectedIngredientsCell", forIndexPath: indexPath) as! SelectedIngredientsCell
    cell.configureWithName(ingredientsViewController.ingredients[indexPath.row].name) {
      if let indexPath = tableView.indexPathForCell($0) {
        self.ingredientsViewController.ingredients.removeAtIndex(indexPath.row)
      }
    }
    return cell
  }
}
