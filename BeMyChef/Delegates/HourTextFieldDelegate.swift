//
//  HourTextFieldDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol HourTextFieldDelegateDelegate {
  func hourNewValue(value: Int)
}

class HourTextFieldDelegate: NSObject {
  lazy var numberFormatter: NSNumberFormatter = {
    let numberFormatter = NSNumberFormatter()
    numberFormatter.numberStyle = .NoStyle
    numberFormatter.maximumFractionDigits = 2
    numberFormatter.minimumFractionDigits = 2
    numberFormatter.minimumIntegerDigits = 1
    numberFormatter.maximumIntegerDigits = 1
    numberFormatter.decimalSeparator = "H"
    return numberFormatter
    }()
  var delegate: HourTextFieldDelegateDelegate?
}

extension HourTextFieldDelegate: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let maxDigits = 3
    var stringMaybeChanged = string
    if count(stringMaybeChanged) > 1 {
      var stringPasted = stringMaybeChanged
      stringPasted = stringPasted.stringByReplacingOccurrencesOfString(numberFormatter.currencySymbol!, withString: "")
      let numberPasted = NSDecimalNumber(string: stringPasted)
      stringMaybeChanged = numberFormatter.stringFromNumber(numberPasted)!
    }
    let selectedRange = textField.selectedTextRange
    let start = textField.beginningOfDocument
    let cursorOffset = textField.offsetFromPosition(start, toPosition: selectedRange!.start)
    var textFieldTextStr = textField.text
    let textFieldTextStrLength = count(textFieldTextStr)
    textFieldTextStr = (textFieldTextStr as NSString).stringByReplacingCharactersInRange(range, withString: stringMaybeChanged)
    textFieldTextStr = textFieldTextStr.stringByReplacingOccurrencesOfString(numberFormatter.currencySymbol!, withString: "")
    textFieldTextStr = textFieldTextStr.stringByReplacingOccurrencesOfString(numberFormatter.decimalSeparator!, withString: "")
    
    while count(textFieldTextStr) > 0 && textFieldTextStr.substringToIndex(advance(textFieldTextStr.startIndex, 1)) == "0" {
      textFieldTextStr = textFieldTextStr.stringByReplacingCharactersInRange(Range<String.Index>(start: textFieldTextStr.startIndex, end: advance(textFieldTextStr.startIndex, 1)), withString: "")
    }
    
    if count(textFieldTextStr) <= maxDigits {
      let textFieldTextNewStr: String?
      if count(textFieldTextStr) > 0 {
        let textFieldTextNum = NSDecimalNumber(string: textFieldTextStr)
        let divideByNum = NSDecimalNumber(int: 10).decimalNumberByRaisingToPower(numberFormatter.maximumFractionDigits)
        let textFieldTextNewNum = textFieldTextNum.decimalNumberByDividingBy(divideByNum)
        textFieldTextNewStr = numberFormatter.stringFromNumber(textFieldTextNewNum)
        let fullHours = Int(textFieldTextNewNum)
        let extraMinutes = Int(textFieldTextNewNum.floatValue * 100) % 100
        delegate?.hourNewValue(fullHours * 60 + extraMinutes)
      } else {
        textFieldTextNewStr = "0H00"
        delegate?.hourNewValue(0)
      }
      
      textField.text = textFieldTextNewStr
      
      if cursorOffset != textFieldTextStrLength {
        let lengthDelta = count(textFieldTextStr) - textFieldTextStrLength
        let newCursorOffset = max(0, min(count(textFieldTextNewStr!), cursorOffset + lengthDelta))
        let newPosition = textField.positionFromPosition(textField.beginningOfDocument, offset: newCursorOffset)
        let newRange = textField.textRangeFromPosition(newPosition, toPosition: newPosition)
        textField.selectedTextRange = newRange
      }
    }
    return false
  }
  
//  func textFieldShouldEndEditing(textField: UITextField) -> Bool {
//    let length = count(textField.text)
//    if length > 1 {
//      var lastTwoDigits = textField.text.substringFromIndex(advance(textField.text.startIndex, length - 2))
//      if var number = numberFormatter.numberFromString(lastTwoDigits) {
//        if number.integerValue >= 60 {
//          number = NSNumber(integer: number.integerValue - 60)
//          let numberStr = numberFormatter.stringFromNumber(number)!
//          if length > 3 {
//            let firstDigit = textField.text.substringToIndex(advance(textField.text.startIndex, 1))
//            var firstDigitNumber = numberFormatter.numberFromString(firstDigit)
//            firstDigitNumber = NSNumber(integer: firstDigitNumber!.integerValue + 1)
//            textField.text = "\(firstDigitNumber!)\(numberStr)"
//          } else {
//            textField.text = "1\(numberStr)"
//          }
//        }
//      }
//    }
//    return true
//  }
}
