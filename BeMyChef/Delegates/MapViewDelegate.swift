//
//  MapViewDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class MapViewDelegate: NSObject {
   
}

extension MapViewDelegate: MKMapViewDelegate {
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    if let address = annotation as? Address {
      var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("Address")
      if annotationView == nil {
        annotationView = MKAnnotationView(annotation: address, reuseIdentifier: "Address")
        annotationView.enabled = true
        annotationView.image = UIImage(named: "pin")
      } else {
        annotationView.annotation = address
      }
      return annotationView
    }
    return nil
  }
}
