//
//  CurrencyTextFieldDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol CurrencyTextFieldDelegateDelegate {
  func currencyNewValue(value: Float)
}

class CurrencyTextFieldDelegate: NSObject {
  lazy var numberFormatter: NSNumberFormatter = {
    let numberFormatter = NSNumberFormatter()
    numberFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    numberFormatter.numberStyle = .CurrencyStyle
    numberFormatter.maximumFractionDigits = 2
    numberFormatter.minimumFractionDigits = 2
    numberFormatter.currencySymbol = "R$"
    return numberFormatter
  }()
  var delegate: CurrencyTextFieldDelegateDelegate?
}

extension CurrencyTextFieldDelegate: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let maxDigits = 5
    var stringMaybeChanged = string
    if count(stringMaybeChanged) > 1 {
      var stringPasted = stringMaybeChanged
      stringPasted = stringPasted.stringByReplacingOccurrencesOfString(numberFormatter.currencySymbol!, withString: "", options: NSStringCompareOptions.LiteralSearch, range: Range<String.Index>(start: stringPasted.startIndex, end: advance(stringPasted.startIndex, count(stringPasted))))
      stringPasted = stringPasted.stringByReplacingOccurrencesOfString(numberFormatter.groupingSeparator, withString: "", options: .LiteralSearch, range: Range<String.Index>(start: stringPasted.startIndex, end: advance(stringPasted.startIndex, count(stringPasted))))
      let numberPasted = NSDecimalNumber(string: stringPasted)
      stringMaybeChanged = numberFormatter.stringFromNumber(numberPasted)!
    }
    let selectedRange = textField.selectedTextRange
    let start = textField.beginningOfDocument
    let cursorOffset = textField.offsetFromPosition(start, toPosition: selectedRange!.start)
    var textFieldTextStr = textField.text
    let textFieldTextStrLength = count(textFieldTextStr)
    textFieldTextStr = (textFieldTextStr as NSString).stringByReplacingCharactersInRange(range, withString: stringMaybeChanged)
    textFieldTextStr = textFieldTextStr.stringByReplacingOccurrencesOfString(numberFormatter.currencySymbol!, withString: "")
    textFieldTextStr = textFieldTextStr.stringByReplacingOccurrencesOfString(numberFormatter.groupingSeparator, withString: "")
    textFieldTextStr = textFieldTextStr.stringByReplacingOccurrencesOfString(numberFormatter.decimalSeparator!, withString: "")
    if count(textFieldTextStr) <= maxDigits {
      let textFieldTextNum = NSDecimalNumber(string: textFieldTextStr)
      let divideByNum = NSDecimalNumber(int: 10).decimalNumberByRaisingToPower(numberFormatter.maximumFractionDigits)
      let textFieldTextNewNum = textFieldTextNum.decimalNumberByDividingBy(divideByNum)
      let textFieldTextNewStr = numberFormatter.stringFromNumber(textFieldTextNewNum)
      
      textField.text = textFieldTextNewStr
      
      delegate?.currencyNewValue(textFieldTextNewNum.floatValue)
      
      if cursorOffset != textFieldTextStrLength {
        let lengthDelta = count(textFieldTextStr) - textFieldTextStrLength
        let newCursorOffset = max(0, min(count(textFieldTextNewStr!), cursorOffset + lengthDelta))
        let newPosition = textField.positionFromPosition(textField.beginningOfDocument, offset: newCursorOffset)
        let newRange = textField.textRangeFromPosition(newPosition, toPosition: newPosition)
        textField.selectedTextRange = newRange
      }
    }
    return false
  }
}
