//
//  OpenKitchenMenuCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class OpenKitchenMenuCollectionDelegate: NSObject {
  @IBOutlet weak var openKitchenMenuViewController: OpenKitchenMenuViewController!
  
  var datasource: [Food] = []
}

extension OpenKitchenMenuCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count + 1
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if indexPath.row < datasource.count {
      let chefFoodCell = tableView.dequeueReusableCellWithIdentifier("ChefFoodCell", forIndexPath: indexPath) as! ChefFoodCell
      chefFoodCell.configureWithFood(datasource[indexPath.row], showsEditingControl: false, showsDetails: false)
      return chefFoodCell
    } else {
      let chefHomeNewFoodCell = tableView.dequeueReusableCellWithIdentifier("ChefHomeNewFoodCell", forIndexPath: indexPath) as! ChefHomeNewFoodCell
      chefHomeNewFoodCell.configureWithType(.Short)
      chefHomeNewFoodCell.contentView.backgroundColor = UIColor(red: 250.0 / 255.0, green: 248.0 / 255.0, blue: 236.0 / 255.0, alpha: 1)
      chefHomeNewFoodCell.callback = {
        self.openKitchenMenuViewController.addNewFood()
      }
      return chefHomeNewFoodCell
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    dispatch_async(dispatch_get_main_queue()) {
      if indexPath.row < self.datasource.count {
        self.openKitchenMenuViewController.presentOptionsForIndex(indexPath.row)
      }
    }
  }
  
  func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    openKitchenMenuViewController.didDeselectFood()
  }
}
