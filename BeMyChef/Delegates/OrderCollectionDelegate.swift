//
//  OrderCollectionDelegate.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum Cell {
  case Status, Info, Map, MapFull, User, Action, Rate, RateComment
}

class OrderCollectionDelegate: NSObject {
  @IBOutlet weak var chefOrderViewController: ChefOrderViewController?
  @IBOutlet weak var orderDetailViewController: OrderDetailViewController?
  
  lazy var mapViewDelegate: MapViewDelegate = {
    return MapViewDelegate()
  }()
  var order: Order!
  var datasource = [Cell]()
}

extension OrderCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    let cellType = datasource[indexPath.row]
    switch cellType {
    case .Status:
      return 210
    case .Info:
      return 148
    case .Map:
      return 200
    case .MapFull:
      return 268
    case .User:
      return 102
    case .Action:
      return 91
    case .Rate:
      return 100
    case .RateComment:
      return 90
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cellType = datasource[indexPath.row]
    switch cellType {
    case .Status:
      let statusCell = tableView.dequeueReusableCellWithIdentifier("OrderStatusCell", forIndexPath: indexPath) as! OrderStatusCell
      statusCell.configureWithOrder(order)
      return statusCell
    case .Info:
      let infoCell = tableView.dequeueReusableCellWithIdentifier("OrderInfoCell", forIndexPath: indexPath) as! OrderInfoCell
      infoCell.configureWithOrder(order)
      return infoCell
    case .Map:
      let mapCell = tableView.dequeueReusableCellWithIdentifier("OrderMapCell", forIndexPath: indexPath) as! OrderMapCell
      if let chefOrderViewController = self.chefOrderViewController {
        mapCell.configureWithUser(order.user)
      } else {
        mapCell.configureWithUser(order.food.kitchen?.chef?.user)
      }
      mapCell.mapView.delegate = mapViewDelegate
      return mapCell
    case .MapFull:
      let mapFullCell = tableView.dequeueReusableCellWithIdentifier("OrderMapFullCell", forIndexPath: indexPath) as! OrderMapFullCell
      if let chefOrderViewController = self.chefOrderViewController {
        mapFullCell.configureWithUser(order.user)
      } else {
        mapFullCell.configureWithUser(order.food.kitchen?.chef?.user)
      }
      mapFullCell.mapView.delegate = mapViewDelegate
      return mapFullCell
    case .User:
      let userCell = tableView.dequeueReusableCellWithIdentifier("OrderUserCell", forIndexPath: indexPath) as! OrderUserCell
      if let chefOrderViewController = self.chefOrderViewController {
        userCell.configureWithUser(order.user)
      } else {
        userCell.configureWithUser(order.food.kitchen?.chef?.user)
      }
      return userCell
    case .Action:
      let actionCell = tableView.dequeueReusableCellWithIdentifier("OrderActionCell", forIndexPath: indexPath) as! OrderActionCell
      if let chefOrderViewController = self.chefOrderViewController {
        if order.status == .Confirmation {
          actionCell.configureWithButtonType(("recusar pedido", {
            chefOrderViewController.declineOrder()
          }))
        } else {
          actionCell.configureWithButtonType(("cancelar pedido", {
            chefOrderViewController.cancelOrder()
          }))
        }
      } else if let orderDetailViewController = self.orderDetailViewController {
        actionCell.configureWithButtonType(("não recebi", {
          orderDetailViewController.notDelivered()
        }))
      }
      return actionCell
    case .Rate:
      let rateCell = tableView.dequeueReusableCellWithIdentifier("RateCell", forIndexPath: indexPath) as! RateCell
      rateCell.callback = {
        self.orderDetailViewController!.currentRating = $0
      }
      return rateCell
    case .RateComment:
      let rateCommentCell = tableView.dequeueReusableCellWithIdentifier("RateCommentCell", forIndexPath: indexPath) as! RateCommentCell
      rateCommentCell.callback = {
        self.orderDetailViewController!.currentComment = $0
      }
      return rateCommentCell
    default:
      return UITableViewCell()
    }
  }
}
