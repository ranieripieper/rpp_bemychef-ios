//
//  SingleButtonAlert.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SingleButtonAlert: Alert {
  @IBOutlet weak var label: UILabel!
  
  var callback: (() -> ())?
  
  // MARK: Button
  @IBAction func buttonTapped(sender: UIButton) {
    dismiss()
  }
  
  override func dismiss() {
    super.dismiss()
    callback?()
  }
}
