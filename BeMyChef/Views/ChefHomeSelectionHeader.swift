//
//  ChefHomeSelectionHeader.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

typealias ButtonType = (String, (() -> ())?)

class ChefHomeSelectionHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var leftButton: Button!
  @IBOutlet weak var rightButton: Button!
  var leftCallback: (() -> ())?
  var rightCallback: (() -> ())?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    leftButton.selected = true
  }
  
  func configureWithButtonTypes(left: ButtonType, right: ButtonType, leftSelected: Bool) {
    leftButton.setTitle(left.0, forState: .Normal)
    leftCallback = left.1
    rightButton.setTitle(right.0, forState: .Normal)
    rightCallback = right.1
    leftButton.selected = leftSelected
    rightButton.selected = !leftSelected
  }
  
  @IBAction func leftTapped(sender: UIButton) {
    leftCallback?()
  }
  
  @IBAction func rightTapped(sender: UIButton) {
    rightCallback?()
  }
}
