//
//  NewOrderView.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NewOrderView: Alert {
  @IBOutlet weak var foodImageView: UIImageView!
  @IBOutlet weak var foodNameLabel: UILabel!
  @IBOutlet weak var foodAmountLabel: UILabel!
  @IBOutlet weak var deliveryLabel: UILabel!
  @IBOutlet weak var deliveryImageView: UIImageView!
  
  var order: Order!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    foodImageView.setImageWithURL(NSURL(string: order.food.pictures.first as? String ?? ""))
    foodNameLabel.text = order.food.name
    foodAmountLabel.text = String(order.food.amountAvailable)
//    deliveryImageView.image = UIImage(named: (order.food.deliveryType == .Delivery ? "" : ""))
    deliveryLabel.text = "você entrega"
  }
  
  // MARK: Button
  @IBAction func buttonTapped(sender: UIButton) {
    if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate, let window = appDelegate.window, let navigationController = window.rootViewController as? UINavigationController {
      let chefOrderViewController = UIStoryboard(name: "Chef", bundle: nil).instantiateViewControllerWithIdentifier("ChefOrderViewController") as! ChefOrderViewController
      chefOrderViewController.order = order
      navigationController.pushViewController(chefOrderViewController, animated: true)
    }
  }
}
