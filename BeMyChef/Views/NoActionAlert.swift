//
//  NoActionAlert.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NoActionAlert: Alert {
  @IBOutlet weak var imageView: UIImageView!
  
  func setKitchenOpen() {
    imageView.image = UIImage(named: "img_alert_open")
  }
  
  func setKitchenClosed() {
    imageView.image = UIImage(named: "img_alert_closed")
  }
}
