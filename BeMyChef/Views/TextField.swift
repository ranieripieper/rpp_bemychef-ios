//
//  TextField.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/8/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TextField: UITextField {
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setPaddings()
  }
  
  func setPaddings() {
    let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: bounds.height))
    self.leftView = leftView
    self.leftViewMode = .Always
    
    let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: bounds.height))
    self.rightView = rightView
    self.rightViewMode = .Always
  }
}
