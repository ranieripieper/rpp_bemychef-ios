//
//  RatingView.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/1/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class RatingView: UIView {
  var dark = false
  
  let interPadding: CGFloat = 2
  
  var rating: Float = 0 {
    didSet {
      self.setNeedsDisplay()
    }
  }
  lazy var stars: (UIImage, UIImage) = {
    if self.dark {
      return (UIImage(named: "icn_chef_star_filled")!, UIImage(named: "icn_chef_star_unfilled")!)
    } else {
      return (UIImage(named: "icn_home_list_rate_unfilled")!, UIImage(named: "icn_home_list_rate_filled")!)
    }
  }()
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func drawRect(rect: CGRect) {
    let width = (bounds.width - 4 * interPadding) / 5
    let frame1 = CGRect(x: 0, y: 0, width: width, height: bounds.height)
    for i in 0...4 {
      let frame = CGRect(x: CGFloat(i) * (width + interPadding), y: 0, width: width, height: bounds.height)
      if rating > Float(i) + 0.5 {
        stars.1.drawInRect(frame)
      } else {
        stars.0.drawInRect(frame)
      }
    }
  }
}
