//
//  CustomAlert.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CustomAlert: Alert {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  
  enum PaymentStatus {
    case Processing, Approved, Failed
  }
  var status: PaymentStatus! {
    didSet {
      switch status! {
      case .Processing:
        animateProcessing()
      case .Approved:
        showApproved()
      case .Failed:
        showFailed()
      }
    }
  }
  var callback: (() -> ())?
  var timer: NSTimer?
  var loadingImage1: UIImageView?
  var loadingImage2: UIImageView?
  var loadingImage3: UIImageView?
  
  override func tapped(tap: UITapGestureRecognizer) {
    if status == nil || status != .Processing {
      super.tapped(tap)
      self.callback?()
    }
  }
  
  func configureForAcceptOrderDelivery() {
    imageView.image = UIImage(named: "img_alert_accepted")
    titleLabel.text = "Mãos à obra!"
    messageLabel.text = "Que bom que você aceitou o pedido! Lembre-se de que você deverá fazer a entrega do prato. Ela estará, no máximo, a 2km da sua cozinha.\n\nNão se esqueça de avisar quando o prato estiver pronto e de lacrar a sua embalagem BeMyChef!!"
  }
  
  func configureForAcceptOrderTakeaway() {
    imageView.image = UIImage(named: "img_alert_accepted")
    titleLabel.text = "Mãos à obra!"
    messageLabel.text = "Que bom que você aceitou o pedido! Lembre-se de que o comprador irá retirar o pedido na sua casa.\n\nEntão é só avisar quando o prato estiver pronto e não esquecer de lacrar a embalagem BeMyChef!!"
  }
  
  func configureForCancelOrder() {
    imageView.image = UIImage(named: "img_alert_denied")
    titleLabel.text = "Pedido cancelado"
    messageLabel.text = "Seu cliente será avisado que algo deu errado e o pedido foi cancelado.\n\nSeu prato continua disponível na sua cozinha."
  }
  
  func configureForOrderReady() {
    imageView.image = UIImage(named: "img_alert_ready")
    titleLabel.text = "Nhami!"
    messageLabel.text = "Seu comprador já foi notificado que o pedido está pronto!\n\nAgora é só pegar a bike e ir entregar, que ele mora logo ali ;)"
  }
  
  func configureForPayment() {
    imageView.image = UIImage(named: "img_alert_paymet_top")
    titleLabel.text = "Status do pagamento"
    messageLabel.text = "Seu pagamento está sendo processado."
  }
  
  func animateProcessing() {
    layoutIfNeeded()
    loadingImage2 = UIImageView(image: UIImage(named: "img_payment_loading"))
    loadingImage2!.frame = CGRect(x: (contentView!.bounds.width - loadingImage2!.frame.width) / 2, y: contentView!.bounds.height * 3 / 4, width: loadingImage2!.bounds.width, height: loadingImage2!.bounds.height)
    contentView!.addSubview(loadingImage2!)
    loadingImage1 = UIImageView(image: UIImage(named: "img_payment_loading"))
    loadingImage1!.frame = CGRect(x: loadingImage2!.frame.origin.x - loadingImage2!.bounds.width - 16, y: loadingImage2!.frame.origin.y, width: loadingImage1!.bounds.width, height: loadingImage1!.bounds.height)
    contentView!.addSubview(loadingImage1!)
    loadingImage3 = UIImageView(image: UIImage(named: "img_payment_loading"))
    loadingImage3!.frame = CGRect(x: loadingImage2!.frame.origin.x + loadingImage2!.bounds.width + 16, y: loadingImage2!.frame.origin.y, width: loadingImage3!.bounds.width, height: loadingImage3!.bounds.height)
    contentView!.addSubview(loadingImage3!)
    loadingImage1!.alpha = 1
    loadingImage2!.alpha = 0.5
    loadingImage3!.alpha = 0.75
    timer = NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: "next", userInfo: nil, repeats: true)
  }
  
  func next() {
    if loadingImage1 != nil {
      loadingImage1!.alpha = loadingImage1!.alpha - 0.25
      if loadingImage1!.alpha < 0.5 {
        loadingImage1!.alpha = 1
      }
    }
    if loadingImage2 != nil {
      loadingImage2!.alpha = loadingImage2!.alpha - 0.25
      if loadingImage2!.alpha < 0.5 {
        loadingImage2!.alpha = 1
      }
    }
    if loadingImage3 != nil {
      loadingImage3!.alpha = loadingImage3!.alpha - 0.25
      if loadingImage3!.alpha < 0.5 {
        loadingImage3!.alpha = 1
      }
    }
  }
  
  func stopAnimatingProcessing() {
    timer?.invalidate()
    timer = nil
    loadingImage1?.removeFromSuperview()
    loadingImage1 = nil
    loadingImage2?.removeFromSuperview()
    loadingImage2 = nil
    loadingImage3?.removeFromSuperview()
    loadingImage3 = nil
  }
  
  func showApproved() {
    stopAnimatingProcessing()
    let approvedImageView = UIImageView(image: UIImage(named: "img_payment_done"))
    approvedImageView.frame = CGRect(x: (contentView!.bounds.width - approvedImageView.bounds.width) / 2, y: (contentView!.bounds.height * 3 / 2 - approvedImageView.bounds.height) / 2, width: approvedImageView.bounds.width, height: approvedImageView.bounds.height)
    contentView!.addSubview(approvedImageView)
    messageLabel.text = "Pagamento aprovado!"
  }
  
  func showFailed() {
    stopAnimatingProcessing()
    let failedImageView = UIImageView(image: UIImage(named: "img_payment_wrong"))
    failedImageView.frame = CGRect(x: (contentView!.bounds.width - failedImageView.bounds.width) / 2, y: (contentView!.bounds.height * 3 / 2 - failedImageView.bounds.height) / 2 + 30, width: failedImageView.bounds.width, height: failedImageView.bounds.height)
    contentView!.addSubview(failedImageView)
    messageLabel.text = "Algo deu errado durante o processo. Por favor, tente novamente."
  }
}
