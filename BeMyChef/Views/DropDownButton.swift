//
//  DropDownButton.swift
//  BeMyChef
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class DropDownButton: UIButton {
  var arrayImageView: UIImageView
  override var selected: Bool {
    didSet {
      UIView.animateWithDuration(0.2) {
        if self.selected {
          self.arrayImageView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, CGFloat(M_PI * 0.99))
        } else {
          self.arrayImageView.transform = CGAffineTransformIdentity
        }
      }
    }
  }
  
  required init(coder aDecoder: NSCoder) {
    arrayImageView = UIImageView(image: UIImage(named: "icn_arrow_down"))
    arrayImageView.contentMode = .Center
    super.init(coder: aDecoder)
    arrayImageView.setTranslatesAutoresizingMaskIntoConstraints(false)
    addSubview(arrayImageView)
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[array(60)]-(0)-|", options: .AlignAllCenterX, metrics: nil, views: ["array": arrayImageView])
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(2)-[array]-(0)-|", options: .AlignAllCenterY, metrics: nil, views: ["array": arrayImageView])
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
    layoutIfNeeded()
  }
  
  override func drawRect(rect: CGRect) {
    let lineRect = CGRect(x: bounds.width - 60, y: 4, width: 2, height: bounds.height - 4 * 2)
    let context = UIGraphicsGetCurrentContext()
    CGContextMoveToPoint(context, lineRect.origin.x, lineRect.origin.y);
    CGContextAddLineToPoint(context, lineRect.origin.x, lineRect.origin.y + lineRect.height);
    CGContextSetStrokeColorWithColor(context, UIColor(red: 236 / 255, green: 111 / 255, blue: 102 / 255, alpha: 1).CGColor);
    CGContextStrokePath(context);
  }
}
