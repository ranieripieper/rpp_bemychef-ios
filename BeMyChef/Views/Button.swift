//
//  Button.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Button: UIButton {
  override var selected: Bool {
    didSet {
      if selected {
        backgroundColor = UIColor(red: 236.0 / 255.0, green: 111.0 / 255.0, blue: 102.0 / 255.0, alpha: 1)
      } else {
        backgroundColor = UIColor(red: 245.0 / 255.0, green: 245.0 / 255.0, blue: 245.0 / 255.0, alpha: 1)
      }
    }
  }
}
