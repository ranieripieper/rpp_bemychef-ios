//
//  ZipcodeView.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class ZipcodeView: UIView {
  @IBOutlet weak var tableView: UITableView!
  
  var zipcodeCollectionDelegate: ZipcodeCollectionDelegate!
  var results: [[(Address, String)]] = [] {
    didSet {
      zipcodeCollectionDelegate.datasource = results
      tableView.reloadData()
    }
  }
  var callback: (Address -> ())? {
    didSet {
      zipcodeCollectionDelegate.callback = callback
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    tableView.registerNib(UINib(nibName: "ZipcodeCell", bundle: nil), forCellReuseIdentifier: "ZipcodeCell")
    zipcodeCollectionDelegate = ZipcodeCollectionDelegate()
    tableView.dataSource = zipcodeCollectionDelegate
    tableView.delegate = zipcodeCollectionDelegate
  }
}
