//
//  Alert.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Alert: UIView {
  @IBOutlet weak var dimmingView: UIView!
  @IBOutlet weak var contentView: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let tap = UITapGestureRecognizer(target: self, action: "tapped:")
    addGestureRecognizer(tap)
  }
  
  func present() {
    if let appdelegate = UIApplication.sharedApplication().delegate as? AppDelegate, let window = appdelegate.window {
      window.addSubview(self)
      frame = window.bounds
    }
    contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: .BeginFromCurrentState, animations: {
      self.contentView.transform = CGAffineTransformIdentity
      self.dimmingView.alpha = 0.7
      }) { finished in
        
    }
  }
  
  func dismiss() {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 10.0, options: .BeginFromCurrentState, animations: {
      self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01)
      self.contentView.alpha = 0.2
      self.dimmingView.alpha = 0.0
      }) { finished in
        self.removeFromSuperview()
    }
  }
  
  func tapped(tap: UITapGestureRecognizer) {
    dismiss()
  }
}
