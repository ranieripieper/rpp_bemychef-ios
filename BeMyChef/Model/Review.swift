//
//  Review.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class Review: NSObject {
  var id: Int
  var user: User
  var food: Food
  var text: String
  var date: NSDate
  var imageURL: String?
  
  init(id: Int, user: User, food: Food, text: String, date: NSDate, imageURL: String?) {
    self.id = id
    self.user = user
    self.food = food
    self.text = text
    self.date = date
    self.imageURL = imageURL
    super.init()
  }
}

// MARK: API
extension Review {
  func update(photo: UIImage?, text: String, rating: Float, completion: Result<Review> -> ()) {
    NetworkManager.sharedInstance.updateComment(id, foodId: food.id, photo: photo, text: text, rating: rating) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}
