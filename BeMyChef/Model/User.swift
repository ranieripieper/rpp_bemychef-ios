//
//  User.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

enum Mode {
  case User, Chef
  
  init(int: Int) {
    if int == 1 {
      self = User
    } else {
      self = Chef
    }
  }
  
  func toInt() -> Int {
    switch self {
    case .User:
      return 1
    case .Chef:
      return 2
    }
  }
}

class User: NSObject {
  var id: Int
  var name: String
  var email: String
  var avatarURL: String
  var chef: Chef?
  var address: Address?
  
  init(_ API: APIResponse) {
    id = API["id"] as! Int
    name = API["name"] as! String
    email = API["email"] as? String ?? ""
    if let photoResponse = API["photo"] as? APIResponse, let url = photoResponse["file_url"] as? String {
      avatarURL = url
    } else {
      avatarURL = ""
    }
    if let chefResponse = API["chef"] as? APIResponse {
      chef = Chef(chefResponse)
    } else {
      chef = nil
    }
    super.init()
    chef?.user = self
  }

  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeIntegerForKey("id")
    name = aDecoder.decodeObjectForKey("name") as? String ?? ""
    email = aDecoder.decodeObjectForKey("email") as? String ?? ""
    avatarURL = aDecoder.decodeObjectForKey("avatarURL") as? String ?? ""
    chef = aDecoder.decodeObjectForKey("chef") as? Chef
    address = aDecoder.decodeObjectForKey("address") as? Address
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(id, forKey: "id")
    aCoder.encodeObject(name, forKey: "name")
    aCoder.encodeObject(email, forKey: "email")
    aCoder.encodeObject(avatarURL, forKey: "avatarURL")
    aCoder.encodeObject(chef, forKey: "chef")
    aCoder.encodeObject(address, forKey: "address")
  }
  
  func distanceToUser(user: User) -> Float {
    // goes all the distance logic
    let number = arc4random() % 22700
    let float = Float(number)
    return float
  }
  
  class func persistedUser() -> User? {
    if let persistedString = SSKeychain.passwordForService("co.bemychef", account: "PersistedUser"), let data = NSData(base64EncodedString: persistedString, options: .IgnoreUnknownCharacters), let user = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? User {
      return user
    }
    return nil
  }
  
  func persist() {
    let data = NSKeyedArchiver.archivedDataWithRootObject(self)
    let string = data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
    SSKeychain.setPassword(string, forService: "co.bemychef", account: "PersistedUser")
  }
  
  class func logout() {
    SSKeychain.deletePasswordForService("co.bemychef", account: "PersistedUser")
    Defaults.removeObjectForKey("CurrentMode")
    Defaults.synchronize()
  }

  class func currentMode() -> Mode {
    if let int = Defaults["CurrentMode"].int {
      return Mode(int: int)
    } else {
      return .User
    }
  }
  
  class func setMode(mode: Mode) {
    Defaults["CurrentMode"] = mode.toInt()
    Defaults.synchronize()
  }
  
  func isChef() -> Bool {
    return chef != nil
  }
}

// MARK: API Authentication
extension User {
  class func registerUser(email: String, password: String, name: String, avatar: UIImage?, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.registerUser(email, password: password, name: name, avatar: avatar) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
        let user = User(boxed.unbox["user"] as! APIResponse)
        user.persist()
        Defaults["CurrentMode"] = Mode.User.toInt()
        Defaults.synchronize()
        completion(Result(boxed.unbox))
      case .Failure(let error):
        println(error.localizedDescription)
        completion(Result(error))
      }
    }
  }
  
  class func registerChef(email: String, password: String, name: String, avatar: UIImage?, chefAbout: String, kitchenPictures: [UIImage], phone: String, cpf: String, zipcode: String, street: String, number: String, complement: String, bankName: String, bankNumber: String, bankAgency: String, bankAccount: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.registerChef(email, password: password, name: name, avatar: avatar, chefAbout: chefAbout, kitchenPictures: kitchenPictures, phone: phone, cpf: cpf, zipcode: zipcode, street: street, number: number, complement: complement, bankName: bankName, bankNumber: bankNumber, bankAgency: bankAgency, bankAccount: bankAccount) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
        let chef = User(boxed.unbox["user"] as! APIResponse)
        chef.persist()
        Defaults["CurrentMode"] = Mode.Chef.toInt()
        Defaults.synchronize()
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func updatePaymentInfo(cpf: String, bankName: String, bankNumber: String, bankAgency: String, bankAccount: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.updatePaymentInfo(cpf, bankName: bankName, bankNumber: bankNumber, bankAgency: bankAgency, bankAccount: bankAccount) { result in
      switch result {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  class func sendConfirmationToEmail(email: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.sendConfirmationToEmail(email) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  class func login(email: String, password: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.login(email, password: password) { result in
      switch result {
      case .Success(let boxed):
        if let userAPI = boxed.unbox["user"] as? APIResponse {
          let user = User(userAPI)
          user.persist()
        }
        completion(Result(true))
      case .Failure(let error):
        println(error.localizedDescription)
        completion(Result(error))
      }
    }
  }
  
  class func forgotPassword(email: String, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.forgotPassword(email) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func update(email: String?, password: String?, name: String?, zipcode: String?, avatar: UIImage?, chef: Bool?, chefAbout: String?, address: Address?, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.update(email, password: password, name: name, zipcode: zipcode, avatar: avatar, chef: chef, chefAbout: chefAbout, address: address) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}

// MARK: API Interactions
extension User {
  func deliveredAddresses(completion: Result<[Address]> -> ()) {
    NetworkManager.sharedInstance.deliveredAddresses() { result in
      switch result {
      case .Success(let boxed):
        var addresses = [Address]()
        if let addressesAPI = boxed.unbox["addresses"] as? [APIResponse] {
          for addressAPI in addressesAPI {
            let address = Address(addressAPI)
            addresses.append(address)
          }
        }
        completion(Result(addresses))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func chef(chefId: Int, completion: Result<Chef> -> ()) {
    NetworkManager.sharedInstance.chef(chefId) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func chefWithUserId(userId: Int, completion: Result<Chef> -> ()) {
    NetworkManager.sharedInstance.chefWithUserId(userId) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func userNotifications(page: Int, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.userNotifications(page) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        completion(result)
      }
    }
  }
  
  func orders(page: Int, completion: Result<[Order]> -> ()) {
    NetworkManager.sharedInstance.orders(page) {
      switch $0 {
      case .Success(let boxed):
        println(boxed.unbox)
        if let ordersAPI = boxed.unbox["ordereds"] as? [APIResponse] {
          completion(Result(ordersAPI.map() {
            Order($0)
            }))
        }
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}
