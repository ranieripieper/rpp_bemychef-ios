//
//  Food.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

enum DeliveryType {
  case Both
  case Takeaway
  case Delivery
  case ProDelivery
  
  init(string: String) {
    switch string.lowercaseString {
      case "both":
        self = Both
      case "delivery":
        self = Delivery
      case "takeaway":
        self = Takeaway
      case "proDelivery":
        self = ProDelivery
      default:
        self = Both
    }
  }
  
  init(rawValue: Int) {//1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy
    switch rawValue {
    case 1:
      self = .Delivery
    case 2:
      self = .Takeaway
    case 3:
      self = .ProDelivery
    default:
      self = .Both
    }
  }
  
  func string() -> String {
    switch self {
    case .Both:
      return "both"
    case .Delivery:
      return "delivery"
    case .Takeaway:
      return "takeaway"
    case .ProDelivery:
      return "motoboy"
    default:
      return ""
    }
  }
  
  func rawValue() -> Int {
    switch self {
    case .Delivery:
      return 1
    case .Takeaway:
      return 2
    case .ProDelivery:
      return 3
    case .Both:
      return 4
    }
  }
  
  func confirmImage() -> UIImage? {
    switch self {
      case .Delivery:
        return UIImage(named: "img_confirm_entrega")
      case .Takeaway:
        return UIImage(named: "img_confirm_retiro")
      default:
        return nil
    }
  }
}

class Food: NSObject {
  var id: Int
  var name: String
  var subtitle: String
  var pictures: [AnyObject]
  var price: Float
  var type: FoodType
  var preparingTime: Int
  var amountAvailable: Int
  var servesCount: Int
  var reviews: [Review]?
  var ingredients: [Ingredient]
  var kitchen: Kitchen!
  
  init(id: Int, name: String, subtitle: String, pictures: [UIImage]?, price: String, type: FoodType, preparingTime: String, amountAvailable: String, servesCount: String, reviews: [Review]?, ingredients: [Ingredient], kitchen: Kitchen?) {
    self.id = id
    self.name = name
    self.subtitle = subtitle
    self.pictures = pictures ?? []
    self.price = (price as NSString).floatValue
    self.type = type
    self.preparingTime = preparingTime.toInt() ?? 0
    self.amountAvailable = amountAvailable.toInt() ?? 0
    self.servesCount = servesCount.toInt() ?? 0
    self.reviews = reviews
    self.ingredients = ingredients
    self.kitchen = kitchen
    super.init()
  }
  
  init(_ API: APIResponse) {
    id = API["id"] as! Int
    name = API["name"] as! String
    subtitle = API["description"] as! String
    price = API["price"] as! Float
    type = FoodType(rawValue: API["dish_type"] as! Int)
    preparingTime = API["time_to_prepare"] as! Int
    amountAvailable = API["quantity_available"] as? Int ?? 0
    servesCount = API["serves_count"] as? Int ?? 0
    reviews = []
    if let picturesAPI = API["photos"] as? [APIResponse] {
      pictures = picturesAPI.map() {
        $0["file_url"] as! String
      }
    } else {
      pictures = []
    }
    if let ingredientsAPI = API["ingredients"] as? [APIResponse] {
      ingredients = (ingredientsAPI).map() {
        Ingredient($0)
      }
    } else {
      ingredients = []
    }
    if let kitchenAPI = API["kitchen"] as? APIResponse {
      kitchen = Kitchen(kitchenAPI)
    } else {
      kitchen = nil
    }
  }
}

// MARK: API
extension Food {
  class func findNearFood(page: Int, latitude: Float, longitude: Float, completion: Result<[Food]> -> ()) {
    NetworkManager.sharedInstance.findNearFood(page, latitude: latitude, longitude: longitude) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
        if let foodsDict = boxed.unbox["dishes"] as? [[String: AnyObject]] {
          var foods = [Food]()
          for foodDict in foodsDict {
            foods.append(Food(foodDict))
          }
          completion(Result(foods))
        }
      case .Failure(let error):
        println(error.localizedDescription)
        completion(Result(error))
      }
    }
  }
  
  func refresh(completion: Result<Food> -> ()) {
    NetworkManager.sharedInstance.food(id) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func comment(photo: UIImage?, text: String, rating: Float, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.commentAtFood(id, photo: photo, text: text, rating: rating) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func comments(page: Int, completion: Result<[Review]> -> ()) {
    NetworkManager.sharedInstance.comments(id, page: page) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func myCommentAtFood(completion: Result<Review> -> ()) {
    NetworkManager.sharedInstance.myCommentAtFood(id) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func reserve(amount: Int, deliveryType: DeliveryType, completion: Result<Reserve> -> ()) {
    NetworkManager.sharedInstance.reserve(id, amount: amount, deliveryType: deliveryType.rawValue(), value: price, deliveryPrice: (deliveryType == .Delivery ? 5 : 0)) {
      switch $0 {
      case .Success(let boxed):
        let reserve = Reserve(boxed.unbox)
        reserve.deliveryType = deliveryType
        completion(Result(reserve))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func order(quantity: Int, deliveryType: DeliveryType, paymentMethod: PaymentMethod, ps: String?, completion: Result<APIResponse> -> ()) {
//    NetworkManager.sharedInstance.order(id, quantity: quantity, deliveryType: deliveryType.rawValue(), ps: ps, cardHolderName: paymentMethod.cardNameHolder, cardNumber: paymentMethod.cardNumber, cardCVV: paymentMethod.cardCVV!, cardExpYear: paymentMethod.cardExpirationYear, cardExpMonth: paymentMethod.cardExpirationMonth, cardFlag: paymentMethod.cardFlag) { result in
//      switch result {
//      case .Success(let boxed):
//        println(boxed.unbox)
//      case .Failure(let error):
//        println(error.localizedDescription)
//      }
//    }
  }
  
  class func insertFood(name: String, description: String, amount: Int, price: Float, type: Int, prepTime: Float, kitchenId: Int, ingredientsId: [Int], pictures: [UIImage], completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.insertFood(name, description: description, amount: amount, price: price, type: type, prepTime: prepTime, kitchenId: kitchenId, ingredientsId: ingredientsId, pictures: pictures) { result in
      switch result {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func updateFood(quantity: Int, timeToPrepare: Int, price: String, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.updateFood(id, quantity: quantity, timeToPrepare: timeToPrepare, price: price) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func delete(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.deleteFood(id) {
      switch $0 {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}
