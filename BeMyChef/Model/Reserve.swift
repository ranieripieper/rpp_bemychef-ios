//
//  Reserve.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Reserve: NSObject {
  let id: Int
  let food: Food
  var deliveryType: DeliveryType?
  
  init(_ API: APIResponse) {
    id = (API["reserve"] as! APIResponse)["id"] as! Int
    food = Food(API["dish"] as! APIResponse)
  }
}

// MARK: API
extension Reserve {
  func confirmWithPaymentMethod(paymentMethod: PaymentMethod, completion: Result<Order> -> ()) {
    PagarMe.sharedInstance().encryptionKey = "ek_test_bfYwFGESFNxBeNk0kU7LDs6Yz2ujQB"
    let creditCard = PagarMeCreditCard(cardNumber: paymentMethod.cardNumber, cardHolderName: paymentMethod.cardNameHolder, cardExpirationMonth: String(paymentMethod.cardExpirationMonth), cardExpirationYear: String(paymentMethod.cardExpirationYear), cardCvv: String(paymentMethod.cardCVV!))
    creditCard.generateHash { error, hash in
      if let error = error {
        completion(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
      } else {
        NetworkManager.sharedInstance.confirmReserve(self.id, cardHash: hash) {
          switch $0 {
          case .Success(let boxed):
            let order = Order(boxed.unbox)
            order.deliveryType = self.deliveryType
            completion(Result(order))
          case .Failure(let error):
            completion(Result(error))
          }
        }
      }
    }
  }
}
