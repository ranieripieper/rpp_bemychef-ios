//
//  Note.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Note {
  let id: Int
  let text: String
  
  init(id: Int, text: String) {
    self.id = id
    self.text = text
  }
}

extension Note {
  func userNotification(notificationId: Int, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.userNotification(id) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func chefNotification(completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.chefNotification(id) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}
