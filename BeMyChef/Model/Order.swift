//
//  Order.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

enum Status {
  case Undefinied, Confirmation, Production, Timeout, NotAccepted, Cancelled, CancelledUser, Ready, Delivered
  
  init(rawValue: Int) {
    switch rawValue {
    case 0:
      self = .Undefinied
    case 1:
      self = .Confirmation
    case 2:
      self = .Production
    case 3:
      self = .Timeout
    case 4:
      self = .NotAccepted
    case 5:
      self = .Cancelled
    case 6:
      self = .CancelledUser
    case 7:
      self = .Ready
    case 8:
      self = .Delivered
    default:
      self = .Undefinied
    }
  }
  
  func toString() -> String {
    switch self {
    case .Confirmation:
      return "Esperando confirmação"
    case .Production:
      return "Pedido em produção"
    case .Cancelled:
      return "Prato cancelado"
    case .Ready:
      return "Prato pronto!"
    case .Timeout, .NotAccepted:
      return "Prato recusado"
    case .Delivered:
      return "Prato entregue"
    default:
      return ""
    }
  }
}

class Order {
  let id: Int
  let userId: Int
  let food: Food
  let amount: Int
  var status: Status = .Undefinied
  var deliveryType: DeliveryType?
  let user: User?
  
  init(id: Int, userId: Int, food: Food, amount: Int) {
    self.id = id
    self.userId = userId
    self.food = food
    self.amount = amount
    self.user = nil
  }
  
  init(_ API: APIResponse) {
    if let orderAPI = API["ordered"] as? APIResponse {
      id = orderAPI["id"] as! Int
      userId = orderAPI["user_id"] as! Int
      amount = orderAPI["quantity"] as! Int
      status = Status(rawValue: orderAPI["last_ordered_status_id"] as! Int)
      deliveryType = DeliveryType(rawValue: orderAPI["delivery_pick_up"] as! Int)
    } else {
      id = API["id"] as? Int ?? 0
      userId = API["user_id"] as? Int ?? 0
      amount = API["quantity"] as? Int ?? 0
      status = Status(rawValue: API["last_ordered_status_id"] as! Int)
      deliveryType = DeliveryType(rawValue: API["delivery_pick_up"] as! Int)
    }
    if let foodAPI = API["dish"] as? APIResponse {
      food = Food(foodAPI)
    } else {
      food = Food([:])
    }
    if let userAPI = API["user"] as? APIResponse {
      user = User(userAPI)
      if let addressAPI = API["address"] as? APIResponse {
        user?.address = Address(addressAPI)
      }
    } else {
      user = nil
    }
  }
}

// MARK: API
extension Order {
  func info(completion: Result<Order> -> ()) {
    NetworkManager.sharedInstance.orderInfo(id) { result in
      switch result {
      case .Success(let boxed):
        if let orderedAPI = boxed.unbox["ordered"] as? APIResponse {
          completion(Result(Order(orderedAPI)))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func accept(accept: Bool, completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.acceptOrder(id, accept: accept) { result in
      switch result {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func cancel(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.cancelOrder(id) { result in
      switch result {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func foodReady(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.foodReady(id) { result in
      switch result {
      case .Success(let boxed):
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}
