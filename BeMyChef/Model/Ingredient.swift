//
//  Ingredient.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Ingredient: NSObject {
  let id: Int
  let name: String
  
  init(id: Int, name: String) {
    self.id = id
    self.name = name
  }
  
  init(_ API: APIResponse) {
    id = API["id"] as! Int
    name = API["name"] as! String
  }
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeIntegerForKey("id")
    name = aDecoder.decodeObjectForKey("name") as! String
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(id, forKey: "id")
    aCoder.encodeObject(name, forKey: "name")
  }
  
  class func all() -> [Ingredient] {
    var ingredients = [Ingredient]()
    if let array = Defaults["Ingredients"].array {
      for data in array as! [NSData] {
        ingredients.append(NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Ingredient)
      }
    }
    return ingredients
  }
}

// MARK: API
extension Ingredient {
  class func allIngredients() {
    NetworkManager.sharedInstance.allIngredients() { result in
      switch result {
      case .Success(let boxed):
        var ingredients = [Ingredient]()
        if let ingredientsAPI = boxed.unbox["ingredients"] as? [APIResponse] {
          for ingredientAPI in ingredientsAPI {
            ingredients.append(Ingredient(ingredientAPI))
          }
        }
        Defaults["Ingredients"] = ingredients.map() {
          NSKeyedArchiver.archivedDataWithRootObject($0)
        }
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  class func allIngredients(page: Int, completion: Result<[Ingredient]> -> ()) {
    NetworkManager.sharedInstance.allIngredients(page) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}
