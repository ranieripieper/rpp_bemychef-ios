//
//  Address.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import MapKit

class Address: NSObject {
  let id: Int
  let street: String
  let number: String
  let neighborhood: String?
  let zipcode: String
  let city: String?
  let state: String?
  let country: String?
  let complement: String?
  var latitude: Float
  var longitude: Float
  
  init(zipcode: String, street: String, number: String, complement: String?) {
    id = 0
    self.street = street
    self.number = number
    self.zipcode = zipcode
    self.complement = complement
    latitude = LocationManager.latitude
    longitude = LocationManager.longitude
    neighborhood = nil
    city = nil
    state = nil
    country = nil
  }
  
  convenience init(latitude: Float, longitude: Float) {
    self.init(zipcode: "", street: "", number: "", complement: "")
    self.latitude = latitude
    self.longitude = longitude
  }
  
  init(_ API: APIResponse) {
    id = API["id"] as? Int ?? 0
    street = API["street"] as? String ?? ""
    number = API["number"] as? String ?? ""
    neighborhood = API["neighborhood"] as? String
    zipcode = API["zip_code"] as? String ?? ""
    city = API["city"] as? String
    state = API["state"] as? String
    country = API["country"] as? String
    complement = API["complement"] as? String
    latitude = API["latitude"] as? Float ?? 0
    longitude = API["longitude"] as? Float ?? 0
  }
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeIntegerForKey("id")
    street = aDecoder.decodeObjectForKey("street") as! String
    number = aDecoder.decodeObjectForKey("number") as! String
    neighborhood = aDecoder.decodeObjectForKey("neighborhood") as? String
    zipcode = aDecoder.decodeObjectForKey("zipcode") as! String
    city = aDecoder.decodeObjectForKey("city") as? String
    state = aDecoder.decodeObjectForKey("state") as? String
    country = aDecoder.decodeObjectForKey("country") as? String
    complement = aDecoder.decodeObjectForKey("complement") as? String
    latitude = aDecoder.decodeFloatForKey("latitude")
    longitude = aDecoder.decodeFloatForKey("longitude")
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(id, forKey: "id")
    aCoder.encodeObject(street, forKey: "street")
    aCoder.encodeObject(number, forKey: "number")
    aCoder.encodeObject(neighborhood, forKey: "neighborhood")
    aCoder.encodeObject(zipcode, forKey: "zipcode")
    aCoder.encodeObject(city, forKey: "city")
    aCoder.encodeObject(state, forKey: "state")
    aCoder.encodeObject(country, forKey: "country")
    aCoder.encodeObject(complement, forKey: "complement")
    aCoder.encodeFloat(latitude, forKey: "latitude")
    aCoder.encodeFloat(longitude, forKey: "longitude")
  }
  
  func toString() -> String {
    var string = street + ", \(number)"
    if neighborhood != nil && count(neighborhood!) > 0 {
      string += ", " + neighborhood!
    }
    string += ", " + zipcode
    if city != nil && count(city!) > 0 {
      string += ", " + city!
    }
    if state != nil && count(state!) > 0 {
      string += ", " + state!
    }
    if country != nil && count(country!) > 0 {
      string += ", " + country!
    }
    return string
  }
  
  func APIDict() -> [String: AnyObject] {
    var params = [String: AnyObject]()
    params["street"] = street
    params["number"] = number
    if neighborhood != nil {
      params["neighborhood"] = neighborhood!
    }
    params["zipcode"] = zipcode
    if city != nil {
      params["city"] = city!
    }
    if state != nil {
      params["state"] = state!
    }
    if country != nil {
      params["country"] = country!
    }
    return params
  }
  
  init(geocode API: [[String: AnyObject]]) {
    id = 0
    complement = ""
    latitude = 0
    longitude = 0
    zipcode = API.filter() {
      if let types = $0["types"] as? [String] {
        return find(types, "postal_code") != nil
      }
      return false
    }.first?["long_name"] as? String ?? ""
    street = API.filter() {
      if let types = $0["types"] as? [String] {
        return find(types, "route") != nil
      }
      return false
      }.first?["long_name"] as? String ?? ""
    number = "0"
    neighborhood = API.filter() {
      if let types = $0["types"] as? [String] {
        return find(types, "neighborhood") != nil
      }
      return false
      }.first?["long_name"] as? String ?? ""
    city = API.filter() {
      if let types = $0["types"] as? [String] {
        return find(types, "locality") != nil
      }
      return false
      }.first?["long_name"] as? String ?? ""
    state = API.filter() {
      if let types = $0["types"] as? [String] {
        return find(types, "administrative_area_level_1") != nil
      }
      return false
      }.first?["long_name"] as? String ?? ""
    country = API.filter() {
      if let types = $0["types"] as? [String] {
        return find(types, "country") != nil
      }
      return false
      }.first?["long_name"] as? String ?? ""
  }
  
  class func persistedAddress() -> Address? {
    if let persistedString = SSKeychain.passwordForService("co.bemychef", account: "PersistedAddress"), let data = NSData(base64EncodedString: persistedString, options: .IgnoreUnknownCharacters), let address = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Address {
      return address
    }
    return nil
  }
  
  func persist() {
    let data = NSKeyedArchiver.archivedDataWithRootObject(self)
    let string = data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
    SSKeychain.setPassword(string, forService: "co.bemychef", account: "PersistedAddress")
  }
}

extension Address: MKAnnotation {
  var title: String { return "" }
  var coordinate: CLLocationCoordinate2D { return CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)) }
}
