//
//  Banks.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Bank {
  var code: String
  var name: String
  
  init(code: String, name: String) {
    self.code = code
    self.name = name
  }
  
  static func allBanks() -> [Bank] {
    let path = NSBundle.mainBundle().pathForResource("bank", ofType: "csv")
    let banksString = String(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
    let banksArray = banksString!.componentsSeparatedByString("\n")
    
    var banks = [Bank]()
    for bankString in banksArray {
      let components = bankString.componentsSeparatedByString(",")
      banks.append(Bank(code: components.first!, name: components.last!))
    }
    return banks
  }
}
