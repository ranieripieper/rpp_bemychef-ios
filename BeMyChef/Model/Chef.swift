//
//  Chef.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class Chef: NSObject {
  var chefId: Int
  var rating: Float
  var rates: Int
  var cancelledCount: Int
  var notAcceptedCount: Int
  var user: User!
  var kitchens: [Kitchen]
  var openOrders = [Order]()
  var orderHistory = [Order]()
  
  init(_ API: APIResponse) {
    chefId = API["id"] as! Int
    rating = API["avg_rating"] as! Float
    rates = API["nr_rating"] as! Int
    cancelledCount = API["qt_cancel_ordered"] as! Int
    notAcceptedCount = API["qt_not_accept_ordered"] as! Int
    if let userResponse = API["user"] as? APIResponse {
      user = User(userResponse)
    }
    kitchens = []
    if let kitchensAPI = API["kitchens"] as? [APIResponse] {
      for kitchenAPI in kitchensAPI {
        kitchens.append(Kitchen(kitchenAPI))
      }
    }
  }

  required init(coder aDecoder: NSCoder) {
    chefId = aDecoder.decodeIntegerForKey("chefId")
    rating = aDecoder.decodeFloatForKey("rating")
    rates = aDecoder.decodeIntegerForKey("rates")
    cancelledCount = aDecoder.decodeIntegerForKey("cancelledCount")
    notAcceptedCount = aDecoder.decodeIntegerForKey("notAcceptedCount")
    kitchens = aDecoder.decodeObjectForKey("kitchens") as? [Kitchen] ?? []
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(chefId, forKey: "chefId")
    aCoder.encodeFloat(rating, forKey: "rating")
    aCoder.encodeInteger(rates, forKey: "rates")
    aCoder.encodeInteger(cancelledCount, forKey: "cancelledCount")
    aCoder.encodeInteger(notAcceptedCount, forKey: "notAcceptedCount")
    aCoder.encodeObject(kitchens, forKey: "kitchens")
  }
}

// MARK: API
extension Chef {
  func kitchensForChef(chefId: Int, completion: Result<[Kitchen]> -> ()) {
    NetworkManager.sharedInstance.kitchensForChef(chefId) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func availableKitchensForChef(chefId: Int, completion: Result<[Kitchen]> -> ()) {
    NetworkManager.sharedInstance.availableKitchensForChef(chefId) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func paymentsSummary(completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.chefPaymentSummary() { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func myKitchen(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.myKitchen() { result in
      switch result {
      case .Success(let boxed):
        if let kitchenAPI = boxed.unbox["kitchen"] as? APIResponse {
          self.kitchens = [Kitchen(kitchenAPI)]
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func myKitchens(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.myKitchens() { result in
      switch result {
      case .Success(let boxed):
        if let kitchensAPI = boxed.unbox["kitchens"] as? [APIResponse] {
          var kitchens = [Kitchen]()
          for kitchenAPI in kitchensAPI {
            kitchens.append(Kitchen(kitchenAPI))
          }
          self.kitchens = kitchens
        }
        completion(Result(true))
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
  
  func chefNotifications(page: Int, completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.chefNotifications(page) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func myOrders(page: Int, completion: Result<[Order]> -> ()) {
    NetworkManager.sharedInstance.myOrders(page) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
        if let orderedsAPI = boxed.unbox["ordereds"] as? [APIResponse] {
          completion(Result(orderedsAPI.map() {
            Order($0)
            }))
        }
      case .Failure(let error):
        completion(Result(error))
      }
    }
  }
}
