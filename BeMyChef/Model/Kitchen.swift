//
//  Kitchen.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import MapKit

class Kitchen: NSObject {
  let id: Int
  let chef: Chef?
  let address: Address?
  var foods: [Food]
  var open = false
  let verified: Bool
  let deliveryType: DeliveryType
  
  override init() {
    id = 0
    address = Address(zipcode: "", street: "", number: "", complement: "")
    open = false
    verified = false
    chef = User.persistedUser()!.chef!
    foods = []
    deliveryType = .Both
  }
  
  init(_ API: APIResponse) {
    id = API["id"] as! Int
    open = !(API["close"] as! Bool)
    verified = API["verified"] as! Bool
    if let addressAPI = API["address"] as? APIResponse {
      address = Address(addressAPI)
    } else {
      address = nil
    }
    if let chefAPI = API["chef"] as? APIResponse {
      chef = Chef(chefAPI)
    } else {
      chef = nil
    }
    if let foodsAPI = API["dishes"] as? [APIResponse] {
      var foods = [Food]()
      for foodAPI in foodsAPI {
        foods.append(Food(foodAPI))
      }
      self.foods = foods
    } else {
      self.foods = []
    }
    let delivery = API["delivery"] as? Bool ?? false
    let takeaway = API["pick_up"] as? Bool ?? false
    if delivery && takeaway {
      deliveryType = .Both
    } else if delivery {
      deliveryType = .Delivery
    } else {
      deliveryType = .Takeaway
    }
  }
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeIntegerForKey("id")
    address = aDecoder.decodeObjectForKey("address") as? Address
    open = aDecoder.decodeBoolForKey("open")
    verified = aDecoder.decodeBoolForKey("verified")
    chef = aDecoder.decodeObjectForKey("chef") as? Chef
    foods = aDecoder.decodeObjectForKey("foods") as? [Food] ?? []
    deliveryType = DeliveryType(rawValue: aDecoder.decodeIntegerForKey("deliveryType"))
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeInteger(id, forKey: "id")
    aCoder.encodeObject(address, forKey: "address")
    aCoder.encodeBool(open, forKey: "open")
    aCoder.encodeBool(verified, forKey: "verified")
    aCoder.encodeObject(chef, forKey: "chef")
    aCoder.encodeObject(foods, forKey: "foods")
    aCoder.encodeInteger(deliveryType.rawValue(), forKey: "deliveryType")
  }
  
  func location() -> CLLocation {
    return CLLocation(latitude: CLLocationDegrees(address?.latitude ?? 0), longitude: CLLocationDegrees(address?.longitude ?? 0))
  }
  
  func distanceToLocation(location: CLLocation) -> Float {
    return Float(location.distanceFromLocation(self.location()))
  }
}

// MARK: API
extension Kitchen {
  class func kitchen(kitchenId: Int, completion: Result<Kitchen> -> ()) {
    NetworkManager.sharedInstance.kitchen(kitchenId) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
  
  func open(foods: [Food], deliveryType: DeliveryType, completion: Result<[Food]> -> ()) {
    NetworkManager.sharedInstance.openKitchen(id, foods: foods.map() {
      ["id": $0.id, "price": $0.price, "quantity": $0.amountAvailable, "time_to_prepare": $0.preparingTime]
      }, delivery: deliveryType != .Takeaway, takeaway: deliveryType != .Delivery) { result in
      switch result {
      case .Success(let boxed):
        if let kitchenAPI = boxed.unbox["kitchen"] as? APIResponse, let foodsAPI = kitchenAPI["dishes"] as? [APIResponse] {
          let foods = foodsAPI.map() {
            Food($0)
          }
          completion(Result(foods))
        }
      case .Failure(let error):
        println(error.localizedDescription)
        completion(Result(error))
      }
    }
  }
  
  func close(completion: Result<Bool> -> ()) {
    NetworkManager.sharedInstance.closeKitchen(id) { result in
      switch result {
      case .Success(let boxed):
        self.open = false
        completion(Result(true))
      case .Failure(let error):
        println(error.localizedDescription)
        completion(Result(error))
      }
    }
  }
}
