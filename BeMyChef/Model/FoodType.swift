//
//  FoodType.swift
//  BeMyChef
//
//  Created by Gilson Gil on 12/19/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

enum FoodType {
  case Regular
  case OvoLacto
  case LactoVeg
  case Vegetarian
  case Vegan
  
  init(rawValue: Int) {
    switch rawValue {
    case 0:
      self = .Regular
    case 1:
      self = .OvoLacto
    case 2:
      self = .LactoVeg
    case 3:
      self = Vegetarian
    case 4:
      self = Vegan
    default:
      self = Regular
    }
  }
  
  static func imageNameForType(foodType: FoodType) -> String {
    switch foodType {
    case .Regular:
      return ""
    case .OvoLacto:
      return "icn_badge_lacto_ovo_lacto"
    case .LactoVeg:
      return "icn_badge_lacto_veg"
    case .Vegetarian:
      return "icn_badge_veg"
    case .Vegan:
      return "icn_badge_vegano"
    }
  }
}

// MARK: API
extension FoodType {
  static func withIngredients(ingredientsIds: [Ingredient], completion: Result<APIResponse> -> ()) {
    NetworkManager.sharedInstance.foodTypeWithIngredients(ingredientsIds.map() { $0.id }) { result in
      switch result {
      case .Success(let boxed):
        println(boxed.unbox)
      case .Failure(let error):
        println(error.localizedDescription)
      }
    }
  }
}
