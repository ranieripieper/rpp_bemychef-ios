//
//  PaymentMethod.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class PaymentMethod {
  let cardNameHolder: String
  let cardNumber: String
  var cardCVV: Int?
  let cardExpirationMonth: Int
  let cardExpirationYear: Int
  let cardFlag: String
  
  init(cardNameHolder: String, cardNumber: String, cardExpirationMonth: Int, cardExpirationYear: Int, cardFlag: String) {
    self.cardNameHolder = cardNameHolder
    self.cardNumber = cardNumber
    self.cardExpirationMonth = cardExpirationMonth
    self.cardExpirationYear = cardExpirationYear
    self.cardFlag = cardFlag
  }
}
